<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
		<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

			<div id="product">
				<div class="main">
					<div class="container">
						<div class="row">
							<div class="col-md-9">

								<div class="breadcrumb clearfix">
									<ul>
										<li itemtype="http://shema.org/Breadcrumb" itemscope="" class="home"><a
												title="Đến trang chủ" href="http://localhost:8099/advpro/view/home"
												itemprop="url"><span itemprop="title">Trang chủ</span></a></li>
										<li itemtype="http://shema.org/Breadcrumb" itemscope=""
											class="category17 icon-li">
											<div class="link-site-more">
												<a title="" href="http://localhost:8099/advpro/view/showProductWithCate"
													itemprop="url"> <span itemprop="title">Sản phẩm</span>
												</a>
											</div>
										</li>
										<li class="productname icon-li"><strong>${product.name}</strong></li>
									</ul>
								</div>

								<div class="product-detail clearfix relative ng-scope" ng-controller="productController"
									ng-init="initController(51003)">

									<!--Begin-->
									<div class="product-block clearfix">
										<div class="row">
											<div class="col-md-6 col-sm-6 col-xs-12 product-image clearfix">
												<div class="sp-loading" style="display: none;">
													<img src="/Images/sp-loading.gif" alt=""><br>LOADING
													IMAGES
												</div>
												<div class="sp-wrap sp-touch" style="display: inline-block;">
													<!-- ngRepeat: item in ProductImages -->
													<!-- end ngRepeat: item in ProductImages -->
													<div class="sp-large">
														<a href="http://runecom02.runtime.vn//Uploads/shop2198/images/product/p50_large.jpg"
															ng-repeat="item in ProductImages"
															class="ng-scope .sp-current-big"><img id="mainA"
																style="width: 350px; height: 430px;"
																src="http://localhost:8085/da/public/getImage/${product.imageUrl}"></a>
													</div>
												</div>
											</div>
											<div class="col-md-6 col-sm-6 col-xs-12 clearfix">
												<h2 class="ng-binding">${product.name}</h2>
												<!-- ngIf: IsTrackingInventory==false||AllowPurchaseWhenSoldOut==true || (IsTrackingInventory&&AllowPurchaseWhenSoldOut==false&&Quantity>0) -->
												<div class="price ng-scope"
													ng-if="IsTrackingInventory==false||AllowPurchaseWhenSoldOut==true || (IsTrackingInventory&amp;&amp;AllowPurchaseWhenSoldOut==false&amp;&amp;Quantity>0)">
													<!-- ngIf: IsPromotion==true -->
													<div ng-if="IsPromotion==true" class="ng-scope">
														<span class="price-new ng-binding">${product.priceFm}đ</span>
														<span class="price-old ng-binding">${product.priceMM}₫</span>
													</div>
													<!-- end ngIf: IsPromotion==true -->
													<!-- ngIf: IsPromotion==false -->
													<span class="product-code ng-binding">Mã SP:
														${product.code} </span>
												</div>
												<!-- end ngIf: IsTrackingInventory==false||AllowPurchaseWhenSoldOut==true || (IsTrackingInventory&&AllowPurchaseWhenSoldOut==false&&Quantity>0) -->
												<!-- ngIf: IsTrackingInventory==true&&AllowPurchaseWhenSoldOut==false&&Quantity<=0 -->

												<div class="social">
													<!-- AddThis Button BEGIN -->
													<div class="addthis_toolbox addthis_default_style">
														<a class="addthis_button_facebook_like at300b"
															fb:like:layout="button_count">
															<div class="fb-like fb_iframe_widget"
																data-layout="button_count" data-show_faces="false"
																data-share="true" data-action="like" data-width="90"
																data-height="25" data-font="arial"
																data-href="http://runecom02.runtime.vn/san-pham/dam-body-lap-the-tay-dai.html"
																data-send="false" style="height: 25px;"
																fb-xfbml-state="rendered"
																fb-iframe-plugin-query="action=like&amp;app_id=227481454296289&amp;container_width=0&amp;font=arial&amp;height=25&amp;href=http%3A%2F%2Frunecom02.runtime.vn%2Fsan-pham%2Fdam-body-lap-the-tay-dai.html&amp;layout=button_count&amp;locale=en_US&amp;sdk=joey&amp;send=false&amp;share=true&amp;show_faces=false&amp;width=90">
																<span
																	style="vertical-align: bottom; width: 145px; height: 28px;"><iframe
																		name="f336482864fda44" width="90px"
																		height="25px"
																		data-testid="fb:like Facebook Social Plugin"
																		title="fb:like Facebook Social Plugin"
																		frameborder="0" allowtransparency="true"
																		allowfullscreen="true" scrolling="no"
																		allow="encrypted-media"
																		src="https://www.facebook.com/v2.6/plugins/like.php?action=like&amp;app_id=227481454296289&amp;channel=https%3A%2F%2Fstaticxx.facebook.com%2Fx%2Fconnect%2Fxd_arbiter%2F%3Fversion%3D46%23cb%3Dfdbd78157151cc%26domain%3Drunecom02.runtime.vn%26origin%3Dhttp%253A%252F%252Frunecom02.runtime.vn%252Ffeff5aa395fd9%26relation%3Dparent.parent&amp;container_width=0&amp;font=arial&amp;height=25&amp;href=http%3A%2F%2Frunecom02.runtime.vn%2Fsan-pham%2Fdam-body-lap-the-tay-dai.html&amp;layout=button_count&amp;locale=en_US&amp;sdk=joey&amp;send=false&amp;share=true&amp;show_faces=false&amp;width=90"
																		style="border: none; visibility: visible; width: 145px; height: 28px;"
																		class=""></iframe></span>
															</div>
														</a> <a class="addthis_button_google_plusone at300b"
															g:plusone:size="medium">
															<div class="google_plusone_iframe_widget"
																style="width: 90px; height: 25px;">
																<span>
																	<div id="___plusone_0"
																		style="position: absolute; width: 450px; left: -10000px;">
																		<iframe ng-non-bindable="" frameborder="0"
																			hspace="0" marginheight="0" marginwidth="0"
																			scrolling="no"
																			style="position: absolute; top: -10000px; width: 450px; margin: 0px; border-style: none"
																			tabindex="0" vspace="0" width="100%"
																			id="I0_1606593712148"
																			name="I0_1606593712148"
																			src="https://apis.google.com/u/0/se/0/_/+1/fastbutton?usegapi=1&amp;size=medium&amp;hl=en-US&amp;origin=http%3A%2F%2Frunecom02.runtime.vn&amp;url=http%3A%2F%2Frunecom02.runtime.vn%2Fsan-pham%2Fdam-body-lap-the-tay-dai.html&amp;gsrc=3p&amp;ic=1&amp;jsh=m%3B%2F_%2Fscs%2Fapps-static%2F_%2Fjs%2Fk%3Doz.gapi.vi.RtB5B1e5jPU.O%2Fam%3DwQE%2Fd%3D1%2Fct%3Dzgms%2Frs%3DAGLTcCMtyT7gJt8TJN2EW_l75ORYrYqkdw%2Fm%3D__features__#_methods=onPlusOne%2C_ready%2C_close%2C_open%2C_resizeMe%2C_renderstart%2Concircled%2Cdrefresh%2Cerefresh&amp;id=I0_1606593712148&amp;_gfid=I0_1606593712148&amp;parent=http%3A%2F%2Frunecom02.runtime.vn&amp;pfname=&amp;rpctoken=28310873"
																			data-gapiattached="true"></iframe>
																	</div>
																	<g:plusone size="medium" lang="null"
																		href="http://runecom02.runtime.vn/san-pham/dam-body-lap-the-tay-dai.html"
																		callback="_at_plusonecallback"
																		data-gapiscan="true" data-onload="true"
																		data-gapistub="true"></g:plusone>
																</span>
															</div>
														</a>
														<div class="atclear"></div>
													</div>

													<div class="option ng-scope" ng-repeat="item in ProductOptions">
														<label class="ng-binding">Màu sắc</label>
														<div class="dropdown-option ">
															<!-- ngIf: $index==0 -->
															<ul ng-if="$index==0" class="option1 ng-scope">
																<c:forEach var="s" items="${product.productColors}">


																 
																		<li ng-repeat="it in item.ProductOptionValues"
																			class="ng-scope"><a onclick="clickAbc(this)"
																				class="colorsss" value="${s.name}"
																				uuid="${s.id}"><strong style=""
																					class="ng-binding">
																					${s.name}</strong></a>
																		</li>
																 
																</c:forEach>
															</ul>
														</div>
														<div class="clearfix"></div>
													</div>



												</div>
												<!-- ngRepeat: item in ProductOptions -->

												<div class="quantity clearfix">
													<label>Số lượng</label>
													<div class="quantity-input">
														<form:form modelAttribute="cart" method="POST">
															<form:input type="number" value="0"
																class="text ng-pristine ng-untouched ng-valid"
																path="quantity" ng-init="InputQuantity=1" />
															<form:input type="hidden" id="color" value=""
																class="text ng-pristine ng-untouched ng-valid"
																path="color" ng-init="InputQuantity=1" />

															<form:input type="hidden" id="colorUUID"
																value="c7a60edd-a031-4c03-b069-6f567dad5d41"
																class="text ng-pristine ng-untouched ng-valid"
																path="colorUUID" ng-init="InputQuantity=1" />
													</div>
												</div>
												<!-- ngIf: IsTrackingInventory==false||AllowPurchaseWhenSoldOut==true || (IsTrackingInventory&&AllowPurchaseWhenSoldOut==false&&Quantity>0) -->
												<div class="action-cart ng-scope"
													ng-if="IsTrackingInventory==false||AllowPurchaseWhenSoldOut==true || (IsTrackingInventory&amp;&amp;AllowPurchaseWhenSoldOut==false&amp;&amp;Quantity>0)">

													<button onclick="funciotn()" class="btn btn-primary" value="true"
														name="insert">Thêm giỏ hàng</button>


													</form:form>



													<script
														src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
												</div>
												<!-- end ngIf: IsTrackingInventory==false||AllowPurchaseWhenSoldOut==true || (IsTrackingInventory&&AllowPurchaseWhenSoldOut==false&&Quantity>0) -->
												<!-- ngIf: IsTrackingInventory==true&&AllowPurchaseWhenSoldOut==false&&Quantity<=0 -->
												<div class="call">

												</div>
											</div>
											<div class="col-md-12 col-sm-12 col-xs-12 clearfix">
												<c:forEach var="pr" items="${product.images}">
													<img onmouseover="bigImg(this)" onmouseout="normalImg(this)"
														style="max-width: 116px; max-height: 72px; margin: 15px; display: inline;"
														class="img-responsive mySlides" alt="03" src="${pr.url}">


												</c:forEach>




												<script>
													function bigImg(x) {
														x.style.height = "150px";
														x.style.width = "64px";
														x.style = "max-width: 150px;max-height: 72px;margin: 10px;display: inline;border: 2px solid #ee4d2d;"
														var urla = x.src
														document.getElementById("mainA").src = urla;
													}

													function normalImg(x) {
														x.style.height = "116px";
														x.style.width = "64px";
														x.style = "max-width: 116px;max-height: 72px;margin: 10px;display: inline"
													}
													let sty = "border: 2px solid rgb(238, 77, 45);"
													let sty1 = "border:1px solid #d4d4d4"

													function clickAbc(x) {
														let colors = "";

														var abc = document.getElementsByClassName("colorsss")
														Array.from(abc).forEach(el => {
															if (el.style.cssText == sty) {
																el.style = sty1
															}
														})
														x.style = sty

														document.getElementById("color").value = x.getAttribute("value")
														document.getElementById("colorUUID").value = x.getAttribute("uuid")
													}
												</script>



											</div>
										</div>
									</div>
									<div class="product-tabs">
										<ul class="nav nav-tabs">
											<!-- ngRepeat: item in ProductTabs -->
											<li role="presentation" ng-class="{'active':$index==0}"
												ng-repeat="item in ProductTabs" class="ng-scope active"><a
													data-toggle="tab" href="#tab1" class="ng-binding">Chi tiết
													sản phẩm</a></li>
											<!-- end ngRepeat: item in ProductTabs -->
										</ul>
										<div class="tab-content">
											<!-- ngRepeat: item in ProductTabs -->
											<div class="tab-pane fade in ng-scope active"
												ng-class="{'active':$index==0}" id="tab1"
												ng-repeat="item in ProductTabs">
												<div ng-bind-html="item.Content|unsafe" class="ng-binding">
													<div
														style="margin: 0px; padding: 5px 0px; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: rgb(221, 221, 221); font-size: 13px; font-family: arial, sans-serif; line-height: 22.1px;">
														<div
															style="margin: 0px 0px 5px; padding: 0px; text-transform: uppercase; font-size: 1.1em; font-weight: bold; clear: both;">
															ĐIỂM NỔI BẬT</div>

														<div style="margin: 0px; padding: 0px;">
															<ul>${product.description}
															</ul>
														</div>
													</div>

													<div
														style="margin: 0px; padding: 0px; font-family: arial, sans-serif; font-size: 13px; line-height: 22.1px; clear: both;">
														&nbsp;</div>


												</div>
												<!-- end ngRepeat: item in ProductTabs -->
											</div>
										</div>
										<!--End-->

									</div>
								</div>
								<div class="col-md-12">
									<div class="fb-comments"
										data-href="https://developers.facebook.com/docs/plugins/comments#configurator"
										data-width="" data-numposts="5"></div>
									<div id="fb-root"></div>
									<script async defer crossorigin="anonymous"
										src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v9.0&appId=2919367368159378&autoLogAppEvents=1"
										nonce="5bLbnP2B"></script>


								</div>
							</div>

							<div class="col-md-3">

								<div class="menu-product">
									<h3>
										<span> Sản phẩm </span>
									</h3>
									<ul class="level_0">
										<c:forEach var="s" items="${listCategory}">

											<li class="has-child level0">
												<c:if test="${s.childrent.size()>0}">
													<a class="dropdown-toggle has-category parent"
														href="http://localhost:8099/advpro/view/showProductWithCate/${s.id}">
														<span>${s.name} </span>
													</a>
												</c:if>
												<c:if test="${s.childrent.size() == 0 && s.parent.id == null }">
													<a class=""
														href="http://localhost:8099/advpro/view/showProductWithCate/${s.id}">
														<span>${s.name}</span>
													</a>
												</c:if>
												<c:if test="${s.childrent.size()>0}">
													<ul class="level1 drop-menu">
														<c:forEach var="s" items="${s.childrent}">
															<li class="level1"><a class=""
																	href="http://localhost:8099/advpro/view/showProductWithCate/${s.id}"><span>${s.name}</span></a>
															</li>
														</c:forEach>
													</ul>
												</c:if>
											</li>
										</c:forEach>
									</ul>
								</div>

								<div id="left_column">
									<div class="block left-module">
										<p class="title_block">Sản phẩm Hot</p>
										<div class="block_content">
											<ul class="products-block best-sell">


												<c:forEach var="s" items="${listProductNew}">

													<li class="clearfix">
														<div class="products-block-left">
															<a
																href="http://localhost:8099/advpro/view/showpro/${s.id}"><img
																	class="img-responsive"
																	alt="Đầm body cá tình với nhiều màu sắc hiện đại, trẻ trung"
																	src="http://localhost:8085/da/public/getImage/${s.imageUrl}"></a>
														</div>
														<div class="products-block-right">
															<p class="product-name">
																<a
																	href="http://localhost:8099/advpro/view/showpro/${s.id}">${s.name}</a>
															</p>
															<p class="product-price">
																<span class="">${s.priceFm}₫</span> <span
																	class="price old-price">${s.priceMM}₫</span>
															</p>
															<p class="product-star">
																<i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
																	class="fa fa-star"></i> <i class="fa fa-star"></i>
																<i class="fa fa-star-half-o"></i>
															</p>
														</div>
													</li>
												</c:forEach>
											</ul>
										</div>
									</div>
								</div>











							</div>
						</div>
					</div>
				</div>











				<script>
					function aaa() {
						document.getElementById('modalMyCart').style.display = "none";
					}

				</script>

				<c:if test="${ss != null}">
					<div class="modal fade in" id="modalMyCart" tabindex="-1" role="dialog"
						aria-labelledby="modalMyCartLabel" aria-hidden="true"
						style="display: block; padding-right: 13px;">
						<div class="modal-dialog  modal-lg">
							<div class="modal-content">

								<div class="modal-body">
									<h4 class="modal-title ng-binding" id="modalMyCartLabel">${mes}</h4>
								</div>
								<div class="modal-footer">
									<div class="row margin-top-10">
										<div class="col-lg-6 text-right">
											<div class="buttons btn-modal-cart">
												<a class="btn btn-default" onclick="aaa()"> Tiếp tục mua
													hàng </a>
											</div>
										</div>
									</div>
								</div>
							</div>

						</div>
					</div>
				</c:if>
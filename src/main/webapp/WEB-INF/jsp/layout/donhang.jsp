<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
		<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>


			<div class="content-page">
				<div class="container">

					<div class="row">

						<div class="col-md-12">

							<div class="breadcrumb clearfix">
								<ul>
									<li itemtype="http://shema.org/Breadcrumb" itemscope="" class="home">
										<a title="Đến trang chủ" href="/" itemprop="url"><span itemprop="title">Trang
												chủ</span></a>
									</li>
									<li class="icon-li"><strong>Đơn hàng của tôi</strong> </li>
								</ul>
							</div>
							<script type="text/javascript">
								$(".link-site-more").hover(function () { $(this).find(".s-c-n").show(); }, function () { $(this).find(".s-c-n").hide(); });
							</script>

							<div class="myorder-content clearfix">
								<h1 class="page-heading"><span>Đơn hàng của tôi</span></h1>
								<div class="myorder-block">
									<div class="table-responsive clearfix myorder-info">
										<table class="table table-mycart">
											<thead>
												<tr>
													<th>Mã đơn hàng</th>
													<th>Tên khách hàng</th>
													<th>Tổng tiền</th>
													<th>Vận chuyển</th>
													<th>Chi tiết</th>
												</tr>
											</thead>
											<tbody>
												<c:forEach var="pr" items="${cart}">
													<tr>

														<td> ${pr.name}</td>
														<td> ${pr.customer.name}</td>
														<td> ${pr.totalPriceDm} đ</td>
														<td>${pr.status == 1?"Chưu chuyển":"Đang chuyển hàng"}</td>
														<td><a
															href="http://localhost:8099/advpro/view/donhang/detail/${pr.id}">Chi tiết đơn hàng</a></td>
													</tr>
												</c:forEach>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
		<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
			<div class="cart-content ng-scope" ng-controller="orderController">
				<h1 class="page-heading">
					<span>Giỏ hàng của tôi</span>
				</h1>
				<div class="steps clearfix">
					<ul class="clearfix">
						<li
							class="cart active col-md-2 col-xs-12 col-sm-4 col-md-offset-3 col-sm-offset-0 col-xs-offset-0">
							<span> </span><span>Giỏ hàng của
								tôi</span><span class="step-number"><a>1</a></span>
						</li>
						<li class="payment col-md-2 col-xs-12 col-sm-4"><span> </span><span>Thanh toán</span><span
								class="step-number"><a>2</a></span></li>
						<li class="finish col-md-2 col-xs-12 col-sm-4"><span> </span><span>Hoàn tất</span><span
								class="step-number"><a>3</a></span></li>
					</ul>
				</div>
				<div class="cart-block-info">
					<div class="cart-info table-responsive">
						<table class="table product-list">
							<thead>
								<tr>
									<th></th>
									<th>Tên sản phẩm</th>
									<th>Màu sắc</th>
									<th>Giá</th>
									<th>Số lượng</th>
									<th>Thành tiền</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								<!-- ngRepeat: item in OrderDetails -->
								<c:forEach var="pr" items="${cart}">
									<tr ng-repeat="item in OrderDetails" class="ng-scope">
										<td class="image"><a
												href="http://localhost:8099/advpro/view/showpro/${pr.product.id}"><img
													class="img-responsive" alt="product"
													src="http://localhost:8085/da/public/getImage/${pr.product.imageUrl}"></a>
										</td>
										<td class="des"><a
												href="http://localhost:8099/advpro/view/showpro/${pr.product.id}"
												class="ng-binding">${pr.product.name}</a> <span
												class="ng-binding"></span></td>

										<td> ${pr.color}
										</td>
										<td class="price ng-binding"> ${pr.product.priceFm} ₫</td> 
										<td class="quantity">
											<a href="http://localhost:8099/advpro/view/cart/plus/${pr.product.id}/${pr.colorUUID}"
												class="qty qty-minus">&#8593;</a> ${pr.quantity}
											<a href="http://localhost:8099/advpro/view/cart/minus/${pr.product.id}/${pr.colorUUID}"
												class="qty qty-plus">&#8595;</a>

										</td>
										<td class="amount ng-binding"> ${pr.total} đ</td>
										<td class=""><a href="http://localhost:8099/advpro/view/cart/remove/${pr.product.id}/${pr.colorUUID}" href="javascript:void(0)">Xoá sản phẩm
											</a></td>
									</tr>
								</c:forEach>
								<!-- end ngRepeat: item in OrderDetails -->
							</tbody>
						</table>
					</div>
					<div class="clearfix text-right">
						<span><b>Tổng thanh toán:</b></span> <span class="pay-price ng-binding"> ${total}đ </span>
					</div>
					<form:form modelAttribute="cart" method="POST">
						<div class="text-right" style="margin-top: 50px">

							<button class="button-default" id="checkout" value="insert" name="insert">Đặt hàng</button>
						</div>
					</form:form>
				</div>
			</div>

			<style>
				a.qty {
					width: 1em;
					line-height: 1em;
					border-radius: 2em;
					font-size: 20px;
					font-weight: bold;
					text-align: center;
					background: #43ace3;
					color: #fff;
				}
			</style>


			<script>
				function aaa() {
					document.getElementById('modalMyCart').style.display = "none";
				}

			</script>

			<c:if test="${ss != null}">
				<div class="modal fade in" id="modalMyCart" tabindex="-1" role="dialog"
					aria-labelledby="modalMyCartLabel" aria-hidden="true" style="display: block; padding-right: 13px;">
					<div class="modal-dialog  modal-lg">
						<div class="modal-content">

							<div class="modal-body">
								<h4 class="modal-title ng-binding" id="modalMyCartLabel">${mes}</h4>
							</div>
							<div class="modal-footer">
								<div class="row margin-top-10">
									<div class="col-lg-6 text-right">
										<div class="buttons btn-modal-cart">
											<a class="btn btn-default" onclick="aaa()"> Tiếp tục mua
												hàng </a>
										</div>
									</div>
								</div>
							</div>
						</div>

					</div>
				</div>
			</c:if>
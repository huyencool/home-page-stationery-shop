<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:forEach var="pr" items="${listProduct}">
	<li class="col-md-3 col-sm-6 col-xs-12">
		<div class="product-container product-resize fixheight"
			style="height: 298px;">
			<div class="left-block image-resize" style="height: 221px;">
				<a href="http://localhost:8099/advpro/view/showpro/${pr.id}"><img
					class="img-responsive" alt="product" src="http://localhost:8085/da/public/getImage/${pr.imageUrl}"></a>
				<div class="quick-view">
					<a title="Add to my wishlist" class="heart" href="#"></a> <a
						title="Xem chi tiết" class="compare"
						href="http://localhost:8099/advpro/view/showpro/${pr.id}"></a>
					<a href="javascript:void(0);"
						class="qv-e-button btn-quickview-1 search" title="Xem nhanh"
						data-handle="/san-pham/dam-body-ca-tinh-voi-nhieu-mau-sac-hien-dai-tre-trung.html"></a>
				</div>
				<div class="add-to-cart">
					<a class="add-to-car"
					href="http://localhost:8099/advpro/view/showpro/${pr.id}">Thêm
						vào giỏ</a>
				</div>
			</div>

			<div class="right-block">
				<h5 class="product-name">
					<a
					href="http://localhost:8099/advpro/view/showpro/${pr.id}">${pr.name}</a>
				</h5>
				<div class="content_price">
					<span class="price product-price">${pr.priceFm} đ</span><span
					class="price old-price">${s.priceMM}₫</span>
				</div>
			</div>
		</div>
	</li>

</c:forEach>
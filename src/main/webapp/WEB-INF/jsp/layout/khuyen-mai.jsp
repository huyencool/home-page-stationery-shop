<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
        <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
            <div class="adv">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <!--Begin-->
                            <div class="product-content ng-scope" ng-controller="productController"
                                ng-init="initProductPromotionSlideController('ProductPromotionSlides')">
                                <h2 class="page-heading">
                                    <span class="page-heading-title"> Sản phẩm khuyến mãi từ ngày <span >${eventList[0].startDate} đến ${eventList[0].endDate}</span> </span> 
                                </h2>
                                <div class="latest-deals-product">
                                    <ul class="product-list owl-carousel owl-theme owl-loaded" data-dots="false"
                                        data-loop="true" data-nav="true" data-margin="10" data-autoplaytimeout="1000"
                                        data-autoplayhoverpause="true"
                                        data-responsive="{&quot;0&quot;:{&quot;items&quot;:1},&quot;600&quot;:{&quot;items&quot;:3},&quot;1000&quot;:{&quot;items&quot;:5}}">
                                    
                                        <div class="owl-stage-outer">
                                            <div class="owl-stage"
                                                style="transform: translate3d(-1180px, 0px, 0px); transition: all 0s ease 0s; width: 5192px;">
                                                <div class="owl-item cloned" style="width: 226px; margin-right: 10px;">
                                                    <li ng-repeat="item in ProductPromotionSlides" class="ng-scope">
                                                        <div class="left-block">
                                                            <a href="/san-pham/noi-com-dien-panasonic-sr-ga721wra.html"><img
                                                                    class="img-responsive"
                                                                    ng-src="http://runecom02.runtime.vn/Uploads/shop2198/images/product/p62_large.jpg"
                                                                    alt="Nồi Cơm Điện PANASONIC SR-GA721WRA"
                                                                    title="Nồi Cơm Điện PANASONIC SR-GA721WRA"
                                                                    src="http://runecom02.runtime.vn/Uploads/shop2198/images/product/p62_large.jpg"></a>
                                                            <div class="quick-view">
                                                                <a title="Add to my wishlist" class="heart"
                                                                    href="#"></a>
                                                                <a title="Xem chi tiết" class="compare"
                                                                    href="/san-pham/noi-com-dien-panasonic-sr-ga721wra.html"></a>
                                                                <a href="javascript:void(0);"
                                                                    class="qv-e-button btn-quickview-1 search"
                                                                    title="Xem nhanh"
                                                                    data-handle="/san-pham/noi-com-dien-panasonic-sr-ga721wra.html"></a>
                                                            </div>
                                                            <div class="add-to-cart">
                                                                <a class="add-to-car" href="javascript:void(0);"
                                                                    ng-click="AddToCard(item)">Thêm vào giỏ</a>
                                                            </div>
                                                            <div class="price-percent-reduction2 ng-binding">
                                                                Sale <br>-19<strong>%</strong>
                                                            </div>
                                                        </div>
                                                        <div class="right-block">
                                                            <h5 class="product-name">
                                                                <a href="/san-pham/noi-com-dien-panasonic-sr-ga721wra.html"
                                                                    class="ng-binding">Nồi Cơm Điện PANASONIC
                                                                    SR-GA721WRA</a>
                                                            </h5>
                                                            <!-- ngIf: ConfigProduct.ShowPrice==true -->
                                                            <div class="content_price ng-scope"
                                                                ng-if="ConfigProduct.ShowPrice==true">
                                                                <!-- ngIf: item.IsPromotion==true&&item.Price>0 -->
                                                                <span class="price product-price ng-binding ng-scope"
                                                                    ng-if="item.IsPromotion==true&amp;&amp;item.Price>0">1,250,000&nbsp;₫</span>
                                                                <!-- end ngIf: item.IsPromotion==true&&item.Price>0 -->
                                                                <!-- ngIf: item.IsPromotion==true&&item.Price>0 -->
                                                                <span class="price old-price ng-binding ng-scope"
                                                                    ng-if="item.IsPromotion==true&amp;&amp;item.Price>0">1,545,000&nbsp;₫</span>
                                                                <!-- end ngIf: item.IsPromotion==true&&item.Price>0 -->
                                                                <!-- ngIf: item.IsPromotion==false&&item.Price>0 -->
                                                                <!-- ngIf: item.Price<=0 -->
                                                            </div>
                                                            <!-- end ngIf: ConfigProduct.ShowPrice==true -->
                                                            <!-- ngIf: ConfigProduct.ShowPrice==false -->
                                                        </div>
                                                    </li>
                                                </div>
                                                <div class="owl-item cloned" style="width: 226px; margin-right: 10px;">
                                                    <li ng-repeat="item in ProductPromotionSlides" class="ng-scope">
                                                        <div class="left-block">
                                                            <a href="/san-pham/ghe-kieu-cho-nhan-vien-van-phong.html"><img
                                                                    class="img-responsive"
                                                                    ng-src="http://runecom02.runtime.vn/Uploads/shop2198/images/product/p77_large.jpg"
                                                                    alt="Ghê kiểu cho nhân viên văn phòng"
                                                                    title="Ghê kiểu cho nhân viên văn phòng"
                                                                    src="http://runecom02.runtime.vn/Uploads/shop2198/images/product/p77_large.jpg"></a>
                                                            <div class="quick-view">
                                                                <a title="Add to my wishlist" class="heart"
                                                                    href="#"></a>
                                                                <a title="Xem chi tiết" class="compare"
                                                                    href="/san-pham/ghe-kieu-cho-nhan-vien-van-phong.html"></a>
                                                                <a href="javascript:void(0);"
                                                                    class="qv-e-button btn-quickview-1 search"
                                                                    title="Xem nhanh"
                                                                    data-handle="/san-pham/ghe-kieu-cho-nhan-vien-van-phong.html"></a>
                                                            </div>
                                                            <div class="add-to-cart">
                                                                <a class="add-to-car" href="javascript:void(0);"
                                                                    ng-click="AddToCard(item)">Thêm vào giỏ</a>
                                                            </div>
                                                            <div class="price-percent-reduction2 ng-binding">
                                                                Sale <br>-8<strong>%</strong>
                                                            </div>
                                                        </div>
                                                        <div class="right-block">
                                                            <h5 class="product-name">
                                                                <a href="/san-pham/ghe-kieu-cho-nhan-vien-van-phong.html"
                                                                    class="ng-binding">Ghê kiểu cho nhân viên văn
                                                                    phòng</a>
                                                            </h5>
                                                            <!-- ngIf: ConfigProduct.ShowPrice==true -->
                                                            <div class="content_price ng-scope"
                                                                ng-if="ConfigProduct.ShowPrice==true">
                                                                <!-- ngIf: item.IsPromotion==true&&item.Price>0 -->
                                                                <span class="price product-price ng-binding ng-scope"
                                                                    ng-if="item.IsPromotion==true&amp;&amp;item.Price>0">1,190,000&nbsp;₫</span>
                                                                <!-- end ngIf: item.IsPromotion==true&&item.Price>0 -->
                                                                <!-- ngIf: item.IsPromotion==true&&item.Price>0 -->
                                                                <span class="price old-price ng-binding ng-scope"
                                                                    ng-if="item.IsPromotion==true&amp;&amp;item.Price>0">1,300,000&nbsp;₫</span>
                                                                <!-- end ngIf: item.IsPromotion==true&&item.Price>0 -->
                                                                <!-- ngIf: item.IsPromotion==false&&item.Price>0 -->
                                                                <!-- ngIf: item.Price<=0 -->
                                                            </div>
                                                            <!-- end ngIf: ConfigProduct.ShowPrice==true -->
                                                            <!-- ngIf: ConfigProduct.ShowPrice==false -->
                                                        </div>
                                                    </li>
                                                </div>
                                                <div class="owl-item cloned" style="width: 226px; margin-right: 10px;">
                                                    <li ng-repeat="item in ProductPromotionSlides" class="ng-scope">
                                                        <div class="left-block">
                                                            <a href="/san-pham/ghe-nhua-chan-cao-tien-nghi.html"><img
                                                                    class="img-responsive"
                                                                    ng-src="http://runecom02.runtime.vn/Uploads/shop2198/images/product/p78_large.jpg"
                                                                    alt="Ghế nhựa chân cao, tiện nghi"
                                                                    title="Ghế nhựa chân cao, tiện nghi"
                                                                    src="http://runecom02.runtime.vn/Uploads/shop2198/images/product/p78_large.jpg"></a>
                                                            <div class="quick-view">
                                                                <a title="Add to my wishlist" class="heart"
                                                                    href="#"></a>
                                                                <a title="Xem chi tiết" class="compare"
                                                                    href="/san-pham/ghe-nhua-chan-cao-tien-nghi.html"></a>
                                                                <a href="javascript:void(0);"
                                                                    class="qv-e-button btn-quickview-1 search"
                                                                    title="Xem nhanh"
                                                                    data-handle="/san-pham/ghe-nhua-chan-cao-tien-nghi.html"></a>
                                                            </div>
                                                            <div class="add-to-cart">
                                                                <a class="add-to-car" href="javascript:void(0);"
                                                                    ng-click="AddToCard(item)">Thêm vào giỏ</a>
                                                            </div>
                                                            <div class="price-percent-reduction2 ng-binding">
                                                                Sale <br>-16<strong>%</strong>
                                                            </div>
                                                        </div>
                                                        <div class="right-block">
                                                            <h5 class="product-name">
                                                                <a href="/san-pham/ghe-nhua-chan-cao-tien-nghi.html"
                                                                    class="ng-binding">Ghế nhựa chân cao, tiện nghi</a>
                                                            </h5>
                                                            <!-- ngIf: ConfigProduct.ShowPrice==true -->
                                                            <div class="content_price ng-scope"
                                                                ng-if="ConfigProduct.ShowPrice==true">
                                                                <!-- ngIf: item.IsPromotion==true&&item.Price>0 -->
                                                                <span class="price product-price ng-binding ng-scope"
                                                                    ng-if="item.IsPromotion==true&amp;&amp;item.Price>0">210,000&nbsp;₫</span>
                                                                <!-- end ngIf: item.IsPromotion==true&&item.Price>0 -->
                                                                <!-- ngIf: item.IsPromotion==true&&item.Price>0 -->
                                                                <span class="price old-price ng-binding ng-scope"
                                                                    ng-if="item.IsPromotion==true&amp;&amp;item.Price>0">250,000&nbsp;₫</span>
                                                                <!-- end ngIf: item.IsPromotion==true&&item.Price>0 -->
                                                                <!-- ngIf: item.IsPromotion==false&&item.Price>0 -->
                                                                <!-- ngIf: item.Price<=0 -->
                                                            </div>
                                                            <!-- end ngIf: ConfigProduct.ShowPrice==true -->
                                                            <!-- ngIf: ConfigProduct.ShowPrice==false -->
                                                        </div>
                                                    </li>
                                                </div>
                                                <div class="owl-item cloned" style="width: 226px; margin-right: 10px;">
                                                    <li ng-repeat="item in ProductPromotionSlides" class="ng-scope">
                                                        <div class="left-block">
                                                            <a href="/san-pham/ghe-cao-cap-gt3300.html"><img
                                                                    class="img-responsive"
                                                                    ng-src="http://runecom02.runtime.vn/Uploads/shop2198/images/product/p79_large.jpg"
                                                                    alt="Ghê cao cấp GT3300" title="Ghê cao cấp GT3300"
                                                                    src="http://runecom02.runtime.vn/Uploads/shop2198/images/product/p79_large.jpg"></a>
                                                            <div class="quick-view">
                                                                <a title="Add to my wishlist" class="heart"
                                                                    href="#"></a>
                                                                <a title="Xem chi tiết" class="compare"
                                                                    href="/san-pham/ghe-cao-cap-gt3300.html"></a> <a
                                                                    href="javascript:void(0);"
                                                                    class="qv-e-button btn-quickview-1 search"
                                                                    title="Xem nhanh"
                                                                    data-handle="/san-pham/ghe-cao-cap-gt3300.html"></a>
                                                            </div>
                                                            <div class="add-to-cart">
                                                                <a class="add-to-car" href="javascript:void(0);"
                                                                    ng-click="AddToCard(item)">Thêm vào giỏ</a>
                                                            </div>
                                                            <div class="price-percent-reduction2 ng-binding">
                                                                Sale <br>-9<strong>%</strong>
                                                            </div>
                                                        </div>
                                                        <div class="right-block">
                                                            <h5 class="product-name">
                                                                <a href="/san-pham/ghe-cao-cap-gt3300.html"
                                                                    class="ng-binding">Ghê cao cấp GT3300</a>
                                                            </h5>
                                                            <!-- ngIf: ConfigProduct.ShowPrice==true -->
                                                            <div class="content_price ng-scope"
                                                                ng-if="ConfigProduct.ShowPrice==true">
                                                                <!-- ngIf: item.IsPromotion==true&&item.Price>0 -->
                                                                <span class="price product-price ng-binding ng-scope"
                                                                    ng-if="item.IsPromotion==true&amp;&amp;item.Price>0">1,450,000&nbsp;₫</span>
                                                                <!-- end ngIf: item.IsPromotion==true&&item.Price>0 -->
                                                                <!-- ngIf: item.IsPromotion==true&&item.Price>0 -->
                                                                <span class="price old-price ng-binding ng-scope"
                                                                    ng-if="item.IsPromotion==true&amp;&amp;item.Price>0">1,600,000&nbsp;₫</span>
                                                                <!-- end ngIf: item.IsPromotion==true&&item.Price>0 -->
                                                                <!-- ngIf: item.IsPromotion==false&&item.Price>0 -->
                                                                <!-- ngIf: item.Price<=0 -->
                                                            </div>
                                                            <!-- end ngIf: ConfigProduct.ShowPrice==true -->
                                                            <!-- ngIf: ConfigProduct.ShowPrice==false -->
                                                        </div>
                                                    </li>
                                                </div>
                                                <div class="owl-item cloned" style="width: 226px; margin-right: 10px;">
                                                    <li ng-repeat="item in ProductPromotionSlides" class="ng-scope">
                                                        <div class="left-block">
                                                            <a href="/san-pham/ghe-cao-cap-tg9930.html"><img
                                                                    class="img-responsive"
                                                                    ng-src="http://runecom02.runtime.vn/Uploads/shop2198/images/product/p80_large.jpg"
                                                                    alt="Ghế cao cấp TG9930" title="Ghế cao cấp TG9930"
                                                                    src="http://runecom02.runtime.vn/Uploads/shop2198/images/product/p80_large.jpg"></a>
                                                            <div class="quick-view">
                                                                <a title="Add to my wishlist" class="heart"
                                                                    href="#"></a>
                                                                <a title="Xem chi tiết" class="compare"
                                                                    href="/san-pham/ghe-cao-cap-tg9930.html"></a> <a
                                                                    href="javascript:void(0);"
                                                                    class="qv-e-button btn-quickview-1 search"
                                                                    title="Xem nhanh"
                                                                    data-handle="/san-pham/ghe-cao-cap-tg9930.html"></a>
                                                            </div>
                                                            <div class="add-to-cart">
                                                                <a class="add-to-car" href="javascript:void(0);"
                                                                    ng-click="AddToCard(item)">Thêm vào giỏ</a>
                                                            </div>
                                                            <div class="price-percent-reduction2 ng-binding">
                                                                Sale <br>-12<strong>%</strong>
                                                            </div>
                                                        </div>
                                                        <div class="right-block">
                                                            <h5 class="product-name">
                                                                <a href="/san-pham/ghe-cao-cap-tg9930.html"
                                                                    class="ng-binding">Ghế cao cấp TG9930</a>
                                                            </h5>
                                                            <!-- ngIf: ConfigProduct.ShowPrice==true -->
                                                            <div class="content_price ng-scope"
                                                                ng-if="ConfigProduct.ShowPrice==true">
                                                                <!-- ngIf: item.IsPromotion==true&&item.Price>0 -->
                                                                <span class="price product-price ng-binding ng-scope"
                                                                    ng-if="item.IsPromotion==true&amp;&amp;item.Price>0">1,190,000&nbsp;₫</span>
                                                                <!-- end ngIf: item.IsPromotion==true&&item.Price>0 -->
                                                                <!-- ngIf: item.IsPromotion==true&&item.Price>0 -->
                                                                <span class="price old-price ng-binding ng-scope"
                                                                    ng-if="item.IsPromotion==true&amp;&amp;item.Price>0">1,350,000&nbsp;₫</span>
                                                                <!-- end ngIf: item.IsPromotion==true&&item.Price>0 -->
                                                                <!-- ngIf: item.IsPromotion==false&&item.Price>0 -->
                                                                <!-- ngIf: item.Price<=0 -->
                                                            </div>
                                                            <!-- end ngIf: ConfigProduct.ShowPrice==true -->
                                                            <!-- ngIf: ConfigProduct.ShowPrice==false -->
                                                        </div>
                                                    </li>
                                                </div>














                                                <c:forEach var="pr" begin="0" end="4"      items="${eventList[0].productEvent}"> 

                                                <div class="owl-item active" style="width: 226px; margin-right: 10px;">
                                                    <li ng-repeat="item in ProductPromotionSlides" style="min-height:285;" class="ng-scope">
                                                        <div class="left-block">
                                                            <a
                                                               href="http://localhost:8099/advpro/view/showpro/${pr.product.id}/${pr.discountPercent}">
                                                                <img class="img-responsive" 
                                                                    alt="Đầm body cá tình với nhiều màu sắc hiện đại, trẻ trung"
                                                                    title="Đầm body cá tình với nhiều màu sắc hiện đại, trẻ trung"
                                                                    src="http://localhost:8085/da/public/getImage/${pr.product.imageUrl}" style="
																max-width: 250px;
																max-height: 200px;
															">
                                                            </a>
                                                            <div class="quick-view">
                                                                <a title="Add to my wishlist" class="heart"
                                                                    href="#"></a>
                                                                <a title="Xem chi tiết" class="compare"
                                                                   href="http://localhost:8099/advpro/view/showpro/${pr.product.id}/${pr.discountPercent}"></a>
                                                                <a href="javascript:void(0);"
                                                                    class="qv-e-button btn-quickview-1 search"
                                                                    title="Xem nhanh"
                                                                    data-handle="/san-pham/dam-body-ca-tinh-voi-nhieu-mau-sac-hien-dai-tre-trung.html"></a>
                                                            </div>
                                                            <div class="add-to-cart">
                                                                <a class="add-to-car"  href="http://localhost:8099/advpro/view/showpro/${pr.product.id}/${pr.discountPercent}""
                                                                    >Thêm vào giỏ</a>
                                                                    
                                                            </div>
                                                        </div>
                                                        <div class="right-block">
                                                            <h5 class="product-name">
                                                                <a href="http://localhost:8099/advpro/view/showpro/${pr.product.id}/${pr.discountPercent}"
                                                                    class="ng-binding">${pr.product.name}</a>
                                                            </h5>
                                                            <!-- ngIf: ConfigProduct.ShowPrice==true -->
                                                            <div class="content_price ng-scope"
                                                                ng-if="ConfigProduct.ShowPrice==true">
                                                                <!-- ngIf: item.IsPromotion==true&&item.Price>0 -->
                                                                <span class="price product-price ng-binding ng-scope"
                                                                    ng-if="item.IsPromotion==true&amp;&amp;item.Price>0">${pr.priceFm}&nbsp;₫</span><br> 
                                                                    <span class="price product-price ng-binding ng-scope"
                                                                    ng-if="item.IsPromotion==true&amp;&amp;item.Price>0">Sale Off&nbsp;${pr.percentSale}%</span><br> 
                                                                    <!-- end ngIf: item.IsPromotion==true&&item.Price>0 -->
                                                                <!-- ngIf: item.IsPromotion==true&&item.Price>0 -->
                                                                                                         <!-- ngIf: item.IsPromotion==false&&item.Price>0 -->
                                                                <!-- ngIf: item.Price<=0 -->
                                                            </div>
                                                            <!-- end ngIf: ConfigProduct.ShowPrice==true -->
                                                            <!-- ngIf: ConfigProduct.ShowPrice==false -->
                                                        </div>
                                                    </li>
                                                </div>
                                              
                                                </c:forEach>















                                                <div class="owl-item" style="width: 226px; margin-right: 10px;">
                                                    <li ng-repeat="item in ProductPromotionSlides" class="ng-scope">
                                                        <div class="left-block">
                                                            <a
                                                                href="/san-pham/vot-tennis-wilson-pro-staff-97-ls-wrt7250102.html"><img
                                                                    class="img-responsive"
                                                                    ng-src="http://runecom02.runtime.vn/Uploads/shop2198/images/product/p58_large.jpg"
                                                                    alt="Vợt tennis Wilson Pro Staff 97 LS WRT7250102"
                                                                    title="Vợt tennis Wilson Pro Staff 97 LS WRT7250102"
                                                                    src="http://runecom02.runtime.vn/Uploads/shop2198/images/product/p58_large.jpg"></a>
                                                            <div class="quick-view">
                                                                <a title="Add to my wishlist" class="heart"
                                                                    href="#"></a>
                                                                <a title="Xem chi tiết" class="compare"
                                                                    href="/san-pham/vot-tennis-wilson-pro-staff-97-ls-wrt7250102.html"></a>
                                                                <a href="javascript:void(0);"
                                                                    class="qv-e-button btn-quickview-1 search"
                                                                    title="Xem nhanh"
                                                                    data-handle="/san-pham/vot-tennis-wilson-pro-staff-97-ls-wrt7250102.html"></a>
                                                            </div>
                                                            <div class="add-to-cart">
                                                                <a class="add-to-car" href="javascript:void(0);"
                                                                    ng-click="AddToCard(item)">Thêm vào giỏ</a>
                                                            </div>
                                                            <div class="price-percent-reduction2 ng-binding">
                                                                Sale <br>-35<strong>%</strong>
                                                            </div>
                                                        </div>
                                                        <div class="right-block">
                                                            <h5 class="product-name">
                                                                <a href="/san-pham/vot-tennis-wilson-pro-staff-97-ls-wrt7250102.html"
                                                                    class="ng-binding">Vợt tennis Wilson Pro Staff 97 LS
                                                                    WRT7250102</a>
                                                            </h5>
                                                            <!-- ngIf: ConfigProduct.ShowPrice==true -->
                                                            <div class="content_price ng-scope"
                                                                ng-if="ConfigProduct.ShowPrice==true">
                                                                <!-- ngIf: item.IsPromotion==true&&item.Price>0 -->
                                                                <span class="price product-price ng-binding ng-scope"
                                                                    ng-if="item.IsPromotion==true&amp;&amp;item.Price>0">4,150,000&nbsp;₫</span>
                                                                <!-- end ngIf: item.IsPromotion==true&&item.Price>0 -->
                                                                <!-- ngIf: item.IsPromotion==true&&item.Price>0 -->
                                                                <span class="price old-price ng-binding ng-scope"
                                                                    ng-if="item.IsPromotion==true&amp;&amp;item.Price>0">6,400,000&nbsp;₫</span>
                                                                <!-- end ngIf: item.IsPromotion==true&&item.Price>0 -->
                                                                <!-- ngIf: item.IsPromotion==false&&item.Price>0 -->
                                                                <!-- ngIf: item.Price<=0 -->
                                                            </div>
                                                            <!-- end ngIf: ConfigProduct.ShowPrice==true -->
                                                            <!-- ngIf: ConfigProduct.ShowPrice==false -->
                                                        </div>
                                                    </li>
                                                </div>
                                                <div class="owl-item" style="width: 226px; margin-right: 10px;">
                                                    <li ng-repeat="item in ProductPromotionSlides" class="ng-scope">
                                                        <div class="left-block">
                                                            <a href="/san-pham/noi-com-dien-tu-philips-hd3130.html"><img
                                                                    class="img-responsive"
                                                                    ng-src="http://runecom02.runtime.vn/Uploads/shop2198/images/product/p61_large.jpg"
                                                                    alt="Nồi Cơm Điện Tử PHILIPS HD3130"
                                                                    title="Nồi Cơm Điện Tử PHILIPS HD3130"
                                                                    src="http://runecom02.runtime.vn/Uploads/shop2198/images/product/p61_large.jpg"></a>
                                                            <div class="quick-view">
                                                                <a title="Add to my wishlist" class="heart"
                                                                    href="#"></a>
                                                                <a title="Xem chi tiết" class="compare"
                                                                    href="/san-pham/noi-com-dien-tu-philips-hd3130.html"></a>
                                                                <a href="javascript:void(0);"
                                                                    class="qv-e-button btn-quickview-1 search"
                                                                    title="Xem nhanh"
                                                                    data-handle="/san-pham/noi-com-dien-tu-philips-hd3130.html"></a>
                                                            </div>
                                                            <div class="add-to-cart">
                                                                <a class="add-to-car" href="javascript:void(0);"
                                                                    ng-click="AddToCard(item)">Thêm vào giỏ</a>
                                                            </div>
                                                            <div class="price-percent-reduction2 ng-binding">
                                                                Sale <br>-10<strong>%</strong>
                                                            </div>
                                                        </div>
                                                        <div class="right-block">
                                                            <h5 class="product-name">
                                                                <a href="/san-pham/noi-com-dien-tu-philips-hd3130.html"
                                                                    class="ng-binding">Nồi Cơm Điện Tử PHILIPS
                                                                    HD3130</a>
                                                            </h5>
                                                            <!-- ngIf: ConfigProduct.ShowPrice==true -->
                                                            <div class="content_price ng-scope"
                                                                ng-if="ConfigProduct.ShowPrice==true">
                                                                <!-- ngIf: item.IsPromotion==true&&item.Price>0 -->
                                                                <span class="price product-price ng-binding ng-scope"
                                                                    ng-if="item.IsPromotion==true&amp;&amp;item.Price>0">1,705,000&nbsp;₫</span>
                                                                <!-- end ngIf: item.IsPromotion==true&&item.Price>0 -->
                                                                <!-- ngIf: item.IsPromotion==true&&item.Price>0 -->
                                                                <span class="price old-price ng-binding ng-scope"
                                                                    ng-if="item.IsPromotion==true&amp;&amp;item.Price>0">1,900,000&nbsp;₫</span>
                                                                <!-- end ngIf: item.IsPromotion==true&&item.Price>0 -->
                                                                <!-- ngIf: item.IsPromotion==false&&item.Price>0 -->
                                                                <!-- ngIf: item.Price<=0 -->
                                                            </div>
                                                            <!-- end ngIf: ConfigProduct.ShowPrice==true -->
                                                            <!-- ngIf: ConfigProduct.ShowPrice==false -->
                                                        </div>
                                                    </li>
                                                </div>
                                                <div class="owl-item" style="width: 226px; margin-right: 10px;">
                                                    <li ng-repeat="item in ProductPromotionSlides" class="ng-scope">
                                                        <div class="left-block">
                                                            <a href="/san-pham/noi-com-dien-panasonic-sr-ga721wra.html"><img
                                                                    class="img-responsive"
                                                                    ng-src="http://runecom02.runtime.vn/Uploads/shop2198/images/product/p62_large.jpg"
                                                                    alt="Nồi Cơm Điện PANASONIC SR-GA721WRA"
                                                                    title="Nồi Cơm Điện PANASONIC SR-GA721WRA"
                                                                    src="http://runecom02.runtime.vn/Uploads/shop2198/images/product/p62_large.jpg"></a>
                                                            <div class="quick-view">
                                                                <a title="Add to my wishlist" class="heart"
                                                                    href="#"></a>
                                                                <a title="Xem chi tiết" class="compare"
                                                                    href="/san-pham/noi-com-dien-panasonic-sr-ga721wra.html"></a>
                                                                <a href="javascript:void(0);"
                                                                    class="qv-e-button btn-quickview-1 search"
                                                                    title="Xem nhanh"
                                                                    data-handle="/san-pham/noi-com-dien-panasonic-sr-ga721wra.html"></a>
                                                            </div>
                                                            <div class="add-to-cart">
                                                                <a class="add-to-car" href="javascript:void(0);"
                                                                    ng-click="AddToCard(item)">Thêm vào giỏ</a>
                                                            </div>
                                                            <div class="price-percent-reduction2 ng-binding">
                                                                Sale <br>-19<strong>%</strong>
                                                            </div>
                                                        </div>
                                                        <div class="right-block">
                                                            <h5 class="product-name">
                                                                <a href="/san-pham/noi-com-dien-panasonic-sr-ga721wra.html"
                                                                    class="ng-binding">Nồi Cơm Điện PANASONIC
                                                                    SR-GA721WRA</a>
                                                            </h5>
                                                            <!-- ngIf: ConfigProduct.ShowPrice==true -->
                                                            <div class="content_price ng-scope"
                                                                ng-if="ConfigProduct.ShowPrice==true">
                                                                <!-- ngIf: item.IsPromotion==true&&item.Price>0 -->
                                                                <span class="price product-price ng-binding ng-scope"
                                                                    ng-if="item.IsPromotion==true&amp;&amp;item.Price>0">1,250,000&nbsp;₫</span>
                                                                <!-- end ngIf: item.IsPromotion==true&&item.Price>0 -->
                                                                <!-- ngIf: item.IsPromotion==true&&item.Price>0 -->
                                                                <span class="price old-price ng-binding ng-scope"
                                                                    ng-if="item.IsPromotion==true&amp;&amp;item.Price>0">1,545,000&nbsp;₫</span>
                                                                <!-- end ngIf: item.IsPromotion==true&&item.Price>0 -->
                                                                <!-- ngIf: item.IsPromotion==false&&item.Price>0 -->
                                                                <!-- ngIf: item.Price<=0 -->
                                                            </div>
                                                            <!-- end ngIf: ConfigProduct.ShowPrice==true -->
                                                            <!-- ngIf: ConfigProduct.ShowPrice==false -->
                                                        </div>
                                                    </li>
                                                </div>
                                                <div class="owl-item" style="width: 226px; margin-right: 10px;">
                                                    <li ng-repeat="item in ProductPromotionSlides" class="ng-scope">
                                                        <div class="left-block">
                                                            <a href="/san-pham/ghe-kieu-cho-nhan-vien-van-phong.html"><img
                                                                    class="img-responsive"
                                                                    ng-src="http://runecom02.runtime.vn/Uploads/shop2198/images/product/p77_large.jpg"
                                                                    alt="Ghê kiểu cho nhân viên văn phòng"
                                                                    title="Ghê kiểu cho nhân viên văn phòng"
                                                                    src="http://runecom02.runtime.vn/Uploads/shop2198/images/product/p77_large.jpg"></a>
                                                            <div class="quick-view">
                                                                <a title="Add to my wishlist" class="heart"
                                                                    href="#"></a>
                                                                <a title="Xem chi tiết" class="compare"
                                                                    href="/san-pham/ghe-kieu-cho-nhan-vien-van-phong.html"></a>
                                                                <a href="javascript:void(0);"
                                                                    class="qv-e-button btn-quickview-1 search"
                                                                    title="Xem nhanh"
                                                                    data-handle="/san-pham/ghe-kieu-cho-nhan-vien-van-phong.html"></a>
                                                            </div>
                                                            <div class="add-to-cart">
                                                                <a class="add-to-car" href="javascript:void(0);"
                                                                    ng-click="AddToCard(item)">Thêm vào giỏ</a>
                                                            </div>
                                                            <div class="price-percent-reduction2 ng-binding">
                                                                Sale <br>-8<strong>%</strong>
                                                            </div>
                                                        </div>
                                                        <div class="right-block">
                                                            <h5 class="product-name">
                                                                <a href="/san-pham/ghe-kieu-cho-nhan-vien-van-phong.html"
                                                                    class="ng-binding">Ghê kiểu cho nhân viên văn
                                                                    phòng</a>
                                                            </h5>
                                                            <!-- ngIf: ConfigProduct.ShowPrice==true -->
                                                            <div class="content_price ng-scope"
                                                                ng-if="ConfigProduct.ShowPrice==true">
                                                                <!-- ngIf: item.IsPromotion==true&&item.Price>0 -->
                                                                <span class="price product-price ng-binding ng-scope"
                                                                    ng-if="item.IsPromotion==true&amp;&amp;item.Price>0">1,190,000&nbsp;₫</span>
                                                                <!-- end ngIf: item.IsPromotion==true&&item.Price>0 -->
                                                                <!-- ngIf: item.IsPromotion==true&&item.Price>0 -->
                                                                <span class="price old-price ng-binding ng-scope"
                                                                    ng-if="item.IsPromotion==true&amp;&amp;item.Price>0">1,300,000&nbsp;₫</span>
                                                                <!-- end ngIf: item.IsPromotion==true&&item.Price>0 -->
                                                                <!-- ngIf: item.IsPromotion==false&&item.Price>0 -->
                                                                <!-- ngIf: item.Price<=0 -->
                                                            </div>
                                                            <!-- end ngIf: ConfigProduct.ShowPrice==true -->
                                                            <!-- ngIf: ConfigProduct.ShowPrice==false -->
                                                        </div>
                                                    </li>
                                                </div>
                                                <div class="owl-item" style="width: 226px; margin-right: 10px;">
                                                    <li ng-repeat="item in ProductPromotionSlides" class="ng-scope">
                                                        <div class="left-block">
                                                            <a href="/san-pham/ghe-nhua-chan-cao-tien-nghi.html"><img
                                                                    class="img-responsive"
                                                                    ng-src="http://runecom02.runtime.vn/Uploads/shop2198/images/product/p78_large.jpg"
                                                                    alt="Ghế nhựa chân cao, tiện nghi"
                                                                    title="Ghế nhựa chân cao, tiện nghi"
                                                                    src="http://runecom02.runtime.vn/Uploads/shop2198/images/product/p78_large.jpg"></a>
                                                            <div class="quick-view">
                                                                <a title="Add to my wishlist" class="heart"
                                                                    href="#"></a>
                                                                <a title="Xem chi tiết" class="compare"
                                                                    href="/san-pham/ghe-nhua-chan-cao-tien-nghi.html"></a>
                                                                <a href="javascript:void(0);"
                                                                    class="qv-e-button btn-quickview-1 search"
                                                                    title="Xem nhanh"
                                                                    data-handle="/san-pham/ghe-nhua-chan-cao-tien-nghi.html"></a>
                                                            </div>
                                                            <div class="add-to-cart">
                                                                <a class="add-to-car" href="javascript:void(0);"
                                                                    ng-click="AddToCard(item)">Thêm vào giỏ</a>
                                                            </div>
                                                            <div class="price-percent-reduction2 ng-binding">
                                                                Sale <br>-16<strong>%</strong>
                                                            </div>
                                                        </div>
                                                        <div class="right-block">
                                                            <h5 class="product-name">
                                                                <a href="/san-pham/ghe-nhua-chan-cao-tien-nghi.html"
                                                                    class="ng-binding">Ghế nhựa chân cao, tiện nghi</a>
                                                            </h5>
                                                            <!-- ngIf: ConfigProduct.ShowPrice==true -->
                                                            <div class="content_price ng-scope"
                                                                ng-if="ConfigProduct.ShowPrice==true">
                                                                <!-- ngIf: item.IsPromotion==true&&item.Price>0 -->
                                                                <span class="price product-price ng-binding ng-scope"
                                                                    ng-if="item.IsPromotion==true&amp;&amp;item.Price>0">210,000&nbsp;₫</span>
                                                                <!-- end ngIf: item.IsPromotion==true&&item.Price>0 -->
                                                                <!-- ngIf: item.IsPromotion==true&&item.Price>0 -->
                                                                <span class="price old-price ng-binding ng-scope"
                                                                    ng-if="item.IsPromotion==true&amp;&amp;item.Price>0">250,000&nbsp;₫</span>
                                                                <!-- end ngIf: item.IsPromotion==true&&item.Price>0 -->
                                                                <!-- ngIf: item.IsPromotion==false&&item.Price>0 -->
                                                                <!-- ngIf: item.Price<=0 -->
                                                            </div>
                                                            <!-- end ngIf: ConfigProduct.ShowPrice==true -->
                                                            <!-- ngIf: ConfigProduct.ShowPrice==false -->
                                                        </div>
                                                    </li>
                                                </div>
                                                <div class="owl-item" style="width: 226px; margin-right: 10px;">
                                                    <li ng-repeat="item in ProductPromotionSlides" class="ng-scope">
                                                        <div class="left-block">
                                                            <a href="/san-pham/ghe-cao-cap-gt3300.html"><img
                                                                    class="img-responsive"
                                                                    ng-src="http://runecom02.runtime.vn/Uploads/shop2198/images/product/p79_large.jpg"
                                                                    alt="Ghê cao cấp GT3300" title="Ghê cao cấp GT3300"
                                                                    src="http://runecom02.runtime.vn/Uploads/shop2198/images/product/p79_large.jpg"></a>
                                                            <div class="quick-view">
                                                                <a title="Add to my wishlist" class="heart"
                                                                    href="#"></a>
                                                                <a title="Xem chi tiết" class="compare"
                                                                    href="/san-pham/ghe-cao-cap-gt3300.html"></a> <a
                                                                    href="javascript:void(0);"
                                                                    class="qv-e-button btn-quickview-1 search"
                                                                    title="Xem nhanh"
                                                                    data-handle="/san-pham/ghe-cao-cap-gt3300.html"></a>
                                                            </div>
                                                            <div class="add-to-cart">
                                                                <a class="add-to-car" href="javascript:void(0);"
                                                                    ng-click="AddToCard(item)">Thêm vào giỏ</a>
                                                            </div>
                                                            <div class="price-percent-reduction2 ng-binding">
                                                                Sale <br>-9<strong>%</strong>
                                                            </div>
                                                        </div>
                                                        <div class="right-block">
                                                            <h5 class="product-name">
                                                                <a href="/san-pham/ghe-cao-cap-gt3300.html"
                                                                    class="ng-binding">Ghê cao cấp GT3300</a>
                                                            </h5>
                                                            <!-- ngIf: ConfigProduct.ShowPrice==true -->
                                                            <div class="content_price ng-scope"
                                                                ng-if="ConfigProduct.ShowPrice==true">
                                                                <!-- ngIf: item.IsPromotion==true&&item.Price>0 -->
                                                                <span class="price product-price ng-binding ng-scope"
                                                                    ng-if="item.IsPromotion==true&amp;&amp;item.Price>0">1,450,000&nbsp;₫</span>
                                                                <!-- end ngIf: item.IsPromotion==true&&item.Price>0 -->
                                                                <!-- ngIf: item.IsPromotion==true&&item.Price>0 -->
                                                                <span class="price old-price ng-binding ng-scope"
                                                                    ng-if="item.IsPromotion==true&amp;&amp;item.Price>0">1,600,000&nbsp;₫</span>
                                                                <!-- end ngIf: item.IsPromotion==true&&item.Price>0 -->
                                                                <!-- ngIf: item.IsPromotion==false&&item.Price>0 -->
                                                                <!-- ngIf: item.Price<=0 -->
                                                            </div>
                                                            <!-- end ngIf: ConfigProduct.ShowPrice==true -->
                                                            <!-- ngIf: ConfigProduct.ShowPrice==false -->
                                                        </div>
                                                    </li>
                                                </div>
                                                <div class="owl-item" style="width: 226px; margin-right: 10px;">
                                                    <li ng-repeat="item in ProductPromotionSlides" class="ng-scope">
                                                        <div class="left-block">
                                                            <a href="/san-pham/ghe-cao-cap-tg9930.html"><img
                                                                    class="img-responsive"
                                                                    ng-src="http://runecom02.runtime.vn/Uploads/shop2198/images/product/p80_large.jpg"
                                                                    alt="Ghế cao cấp TG9930" title="Ghế cao cấp TG9930"
                                                                    src="http://runecom02.runtime.vn/Uploads/shop2198/images/product/p80_large.jpg"></a>
                                                            <div class="quick-view">
                                                                <a title="Add to my wishlist" class="heart"
                                                                    href="#"></a>
                                                                <a title="Xem chi tiết" class="compare"
                                                                    href="/san-pham/ghe-cao-cap-tg9930.html"></a> <a
                                                                    href="javascript:void(0);"
                                                                    class="qv-e-button btn-quickview-1 search"
                                                                    title="Xem nhanh"
                                                                    data-handle="/san-pham/ghe-cao-cap-tg9930.html"></a>
                                                            </div>
                                                            <div class="add-to-cart">
                                                                <a class="add-to-car" href="javascript:void(0);"
                                                                    ng-click="AddToCard(item)">Thêm vào giỏ</a>
                                                            </div>
                                                            <div class="price-percent-reduction2 ng-binding">
                                                                Sale <br>-12<strong>%</strong>
                                                            </div>
                                                        </div>
                                                        <div class="right-block">
                                                            <h5 class="product-name">
                                                                <a href="/san-pham/ghe-cao-cap-tg9930.html"
                                                                    class="ng-binding">Ghế cao cấp TG9930</a>
                                                            </h5>
                                                            <!-- ngIf: ConfigProduct.ShowPrice==true -->
                                                            <div class="content_price ng-scope"
                                                                ng-if="ConfigProduct.ShowPrice==true">
                                                                <!-- ngIf: item.IsPromotion==true&&item.Price>0 -->
                                                                <span class="price product-price ng-binding ng-scope"
                                                                    ng-if="item.IsPromotion==true&amp;&amp;item.Price>0">1,190,000&nbsp;₫</span>
                                                                <!-- end ngIf: item.IsPromotion==true&&item.Price>0 -->
                                                                <!-- ngIf: item.IsPromotion==true&&item.Price>0 -->
                                                                <span class="price old-price ng-binding ng-scope"
                                                                    ng-if="item.IsPromotion==true&amp;&amp;item.Price>0">1,350,000&nbsp;₫</span>
                                                                <!-- end ngIf: item.IsPromotion==true&&item.Price>0 -->
                                                                <!-- ngIf: item.IsPromotion==false&&item.Price>0 -->
                                                                <!-- ngIf: item.Price<=0 -->
                                                            </div>
                                                            <!-- end ngIf: ConfigProduct.ShowPrice==true -->
                                                            <!-- ngIf: ConfigProduct.ShowPrice==false -->
                                                        </div>
                                                    </li>
                                                </div>
                                                <div class="owl-item cloned" style="width: 226px; margin-right: 10px;">
                                                    <li ng-repeat="item in ProductPromotionSlides" class="ng-scope">
                                                        <div class="left-block">
                                                            <a
                                                               href="http://localhost:8099/advpro/view/showpro/${pr.id}"><img
                                                                    class="img-responsive"
                                                                    ng-src="http://runecom02.runtime.vn/Uploads/shop2198/images/product/p35_large.jpg"
                                                                    alt="Đầm body cá tình với nhiều màu sắc hiện đại, trẻ trung"
                                                                    title="Đầm body cá tình với nhiều màu sắc hiện đại, trẻ trung"
                                                                    src="http://runecom02.runtime.vn/Uploads/shop2198/images/product/p35_large.jpg"></a>
                                                            <div class="quick-view">
                                                                <a title="Add to my wishlist" class="heart"
                                                                    href="#"></a>
                                                                <a title="Xem chi tiết" class="compare"
                                                                   href="http://localhost:8099/advpro/view/showpro/${pr.id}"></a>
                                                                <a href="javascript:void(0);"
                                                                    class="qv-e-button btn-quickview-1 search"
                                                                    title="Xem nhanh"
                                                                    data-handle="/san-pham/dam-body-ca-tinh-voi-nhieu-mau-sac-hien-dai-tre-trung.html"></a>
                                                            </div>
                                                            <div class="add-to-cart">
                                                                <a class="add-to-car" href="javascript:void(0);"
                                                                    ng-click="AddToCard(item)">Thêm vào giỏ</a>
                                                            </div>
                                                            <div class="price-percent-reduction2 ng-binding">
                                                                Sale <br>-35<strong>%</strong>
                                                            </div>
                                                        </div>
                                                        <div class="right-block">
                                                            <h5 class="product-name">
                                                                <ahref="http://localhost:8099/advpro/view/showpro/${pr.id}"
                                                                    class="ng-binding">Đầm body cá tình với nhiều màu
                                                                    sắc hiện đại, trẻ trung</a>
                                                            </h5>
                                                            <!-- ngIf: ConfigProduct.ShowPrice==true -->
                                                            <div class="content_price ng-scope"
                                                                ng-if="ConfigProduct.ShowPrice==true">
                                                                <!-- ngIf: item.IsPromotion==true&&item.Price>0 -->
                                                                <span class="price product-price ng-binding ng-scope"
                                                                    ng-if="item.IsPromotion==true&amp;&amp;item.Price>0">400,000&nbsp;₫</span>
                                                                <!-- end ngIf: item.IsPromotion==true&&item.Price>0 -->
                                                                <!-- ngIf: item.IsPromotion==true&&item.Price>0 -->
                                                                <span class="price old-price ng-binding ng-scope"
                                                                    ng-if="item.IsPromotion==true&amp;&amp;item.Price>0">620,000&nbsp;₫</span>
                                                                <!-- end ngIf: item.IsPromotion==true&&item.Price>0 -->
                                                                <!-- ngIf: item.IsPromotion==false&&item.Price>0 -->
                                                                <!-- ngIf: item.Price<=0 -->
                                                            </div>
                                                            <!-- end ngIf: ConfigProduct.ShowPrice==true -->
                                                            <!-- ngIf: ConfigProduct.ShowPrice==false -->
                                                        </div>
                                                    </li>
                                                </div>
                                                <div class="owl-item cloned" style="width: 226px; margin-right: 10px;">
                                                    <li ng-repeat="item in ProductPromotionSlides" class="ng-scope">
                                                        <div class="left-block">
                                                            <a href="/san-pham/dam-maxi-du-tiec-hoa-hong-nh028.html"><img
                                                                    class="img-responsive"
                                                                    ng-src="http://runecom02.runtime.vn/Uploads/shop2198/images/product/p49_large.jpg"
                                                                    alt="Đầm maxi dự tiệc hoa hồng - NH028"
                                                                    title="Đầm maxi dự tiệc hoa hồng - NH028"
                                                                    src="http://runecom02.runtime.vn/Uploads/shop2198/images/product/p49_large.jpg"></a>
                                                            <div class="quick-view">
                                                                <a title="Add to my wishlist" class="heart"
                                                                    href="#"></a>
                                                                <a title="Xem chi tiết" class="compare"
                                                                    href="/san-pham/dam-maxi-du-tiec-hoa-hong-nh028.html"></a>
                                                                <a href="javascript:void(0);"
                                                                    class="qv-e-button btn-quickview-1 search"
                                                                    title="Xem nhanh"
                                                                    data-handle="/san-pham/dam-maxi-du-tiec-hoa-hong-nh028.html"></a>
                                                            </div>
                                                            <div class="add-to-cart">
                                                                <a class="add-to-car" href="javascript:void(0);"
                                                                    ng-click="AddToCard(item)">Thêm vào giỏ</a>
                                                            </div>
                                                            <div class="price-percent-reduction2 ng-binding">
                                                                Sale <br>-21<strong>%</strong>
                                                            </div>
                                                        </div>
                                                        <div class="right-block">
                                                            <h5 class="product-name">
                                                                <a href="/san-pham/dam-maxi-du-tiec-hoa-hong-nh028.html"
                                                                    class="ng-binding">Đầm maxi dự tiệc hoa hồng -
                                                                    NH028</a>
                                                            </h5>
                                                            <!-- ngIf: ConfigProduct.ShowPrice==true -->
                                                            <div class="content_price ng-scope"
                                                                ng-if="ConfigProduct.ShowPrice==true">
                                                                <!-- ngIf: item.IsPromotion==true&&item.Price>0 -->
                                                                <span class="price product-price ng-binding ng-scope"
                                                                    ng-if="item.IsPromotion==true&amp;&amp;item.Price>0">190,000&nbsp;₫</span>
                                                                <!-- end ngIf: item.IsPromotion==true&&item.Price>0 -->
                                                                <!-- ngIf: item.IsPromotion==true&&item.Price>0 -->
                                                                <span class="price old-price ng-binding ng-scope"
                                                                    ng-if="item.IsPromotion==true&amp;&amp;item.Price>0">240,000&nbsp;₫</span>
                                                                <!-- end ngIf: item.IsPromotion==true&&item.Price>0 -->
                                                                <!-- ngIf: item.IsPromotion==false&&item.Price>0 -->
                                                                <!-- ngIf: item.Price<=0 -->
                                                            </div>
                                                            <!-- end ngIf: ConfigProduct.ShowPrice==true -->
                                                            <!-- ngIf: ConfigProduct.ShowPrice==false -->
                                                        </div>
                                                    </li>
                                                </div>
                                                <div class="owl-item cloned" style="width: 226px; margin-right: 10px;">
                                                    <li ng-repeat="item in ProductPromotionSlides" class="ng-scope">
                                                        <div class="left-block">
                                                            <a href="/san-pham/dam-body-lap-the-tay-dai.html"><img
                                                                    class="img-responsive"
                                                                    ng-src="http://runecom02.runtime.vn/Uploads/shop2198/images/product/p50_large.jpg"
                                                                    alt="Đầm body lập thể tay dài"
                                                                    title="Đầm body lập thể tay dài"
                                                                    src="http://runecom02.runtime.vn/Uploads/shop2198/images/product/p50_large.jpg"></a>
                                                            <div class="quick-view">
                                                                <a title="Add to my wishlist" class="heart"
                                                                    href="#"></a>
                                                                <a title="Xem chi tiết" class="compare"
                                                                    href="/san-pham/dam-body-lap-the-tay-dai.html"></a>
                                                                <a href="javascript:void(0);"
                                                                    class="qv-e-button btn-quickview-1 search"
                                                                    title="Xem nhanh"
                                                                    data-handle="/san-pham/dam-body-lap-the-tay-dai.html"></a>
                                                            </div>
                                                            <div class="add-to-cart">
                                                                <a class="add-to-car" href="javascript:void(0);"
                                                                    ng-click="AddToCard(item)">Thêm vào giỏ</a>
                                                            </div>
                                                            <div class="price-percent-reduction2 ng-binding">
                                                                Sale <br>-45<strong>%</strong>
                                                            </div>
                                                        </div>
                                                        <div class="right-block">
                                                            <h5 class="product-name">
                                                                <a href="/san-pham/dam-body-lap-the-tay-dai.html"
                                                                    class="ng-binding">Đầm body lập thể tay dài</a>
                                                            </h5>
                                                            <!-- ngIf: ConfigProduct.ShowPrice==true -->
                                                            <div class="content_price ng-scope"
                                                                ng-if="ConfigProduct.ShowPrice==true">
                                                                <!-- ngIf: item.IsPromotion==true&&item.Price>0 -->
                                                                <span class="price product-price ng-binding ng-scope"
                                                                    ng-if="item.IsPromotion==true&amp;&amp;item.Price>0">310,000&nbsp;₫</span>
                                                                <!-- end ngIf: item.IsPromotion==true&&item.Price>0 -->
                                                                <!-- ngIf: item.IsPromotion==true&&item.Price>0 -->
                                                                <span class="price old-price ng-binding ng-scope"
                                                                    ng-if="item.IsPromotion==true&amp;&amp;item.Price>0">560,000&nbsp;₫</span>
                                                                <!-- end ngIf: item.IsPromotion==true&&item.Price>0 -->
                                                                <!-- ngIf: item.IsPromotion==false&&item.Price>0 -->
                                                                <!-- ngIf: item.Price<=0 -->
                                                            </div>
                                                            <!-- end ngIf: ConfigProduct.ShowPrice==true -->
                                                            <!-- ngIf: ConfigProduct.ShowPrice==false -->
                                                        </div>
                                                    </li>
                                                </div>
                                                <div class="owl-item cloned" style="width: 226px; margin-right: 10px;">
                                                    <li ng-repeat="item in ProductPromotionSlides" class="ng-scope">
                                                        <div class="left-block">
                                                            <a href="/san-pham/dam-mac-nha-tay-lo-nitimo-2001.html"><img
                                                                    class="img-responsive"
                                                                    ng-src="http://runecom02.runtime.vn/Uploads/shop2198/images/product/p51_large.jpg"
                                                                    alt="Đầm mặc nhà tay lỡ NITIMO 2001"
                                                                    title="Đầm mặc nhà tay lỡ NITIMO 2001"
                                                                    src="http://runecom02.runtime.vn/Uploads/shop2198/images/product/p51_large.jpg"></a>
                                                            <div class="quick-view">
                                                                <a title="Add to my wishlist" class="heart"
                                                                    href="#"></a>
                                                                <a title="Xem chi tiết" class="compare"
                                                                    href="/san-pham/dam-mac-nha-tay-lo-nitimo-2001.html"></a>
                                                                <a href="javascript:void(0);"
                                                                    class="qv-e-button btn-quickview-1 search"
                                                                    title="Xem nhanh"
                                                                    data-handle="/san-pham/dam-mac-nha-tay-lo-nitimo-2001.html"></a>
                                                            </div>
                                                            <div class="add-to-cart">
                                                                <a class="add-to-car" href="javascript:void(0);"
                                                                    ng-click="AddToCard(item)">Thêm vào giỏ</a>
                                                            </div>
                                                            <div class="price-percent-reduction2 ng-binding">
                                                                Sale <br>-32<strong>%</strong>
                                                            </div>
                                                        </div>
                                                        <div class="right-block">
                                                            <h5 class="product-name">
                                                                <a href="/san-pham/dam-mac-nha-tay-lo-nitimo-2001.html"
                                                                    class="ng-binding">Đầm mặc nhà tay lỡ NITIMO
                                                                    2001</a>
                                                            </h5>
                                                            <!-- ngIf: ConfigProduct.ShowPrice==true -->
                                                            <div class="content_price ng-scope"
                                                                ng-if="ConfigProduct.ShowPrice==true">
                                                                <!-- ngIf: item.IsPromotion==true&&item.Price>0 -->
                                                                <span class="price product-price ng-binding ng-scope"
                                                                    ng-if="item.IsPromotion==true&amp;&amp;item.Price>0">190,000&nbsp;₫</span>
                                                                <!-- end ngIf: item.IsPromotion==true&&item.Price>0 -->
                                                                <!-- ngIf: item.IsPromotion==true&&item.Price>0 -->
                                                                <span class="price old-price ng-binding ng-scope"
                                                                    ng-if="item.IsPromotion==true&amp;&amp;item.Price>0">280,000&nbsp;₫</span>
                                                                <!-- end ngIf: item.IsPromotion==true&&item.Price>0 -->
                                                                <!-- ngIf: item.IsPromotion==false&&item.Price>0 -->
                                                                <!-- ngIf: item.Price<=0 -->
                                                            </div>
                                                            <!-- end ngIf: ConfigProduct.ShowPrice==true -->
                                                            <!-- ngIf: ConfigProduct.ShowPrice==false -->
                                                        </div>
                                                    </li>
                                                </div>
                                                <div class="owl-item cloned" style="width: 226px; margin-right: 10px;">
                                                    <li ng-repeat="item in ProductPromotionSlides" class="ng-scope">
                                                        <div class="left-block">
                                                            <a href="/san-pham/giay-tennis-t19-xanh.html"><img
                                                                    class="img-responsive"
                                                                    ng-src="http://runecom02.runtime.vn/Uploads/shop2198/images/product/p55_large.jpg"
                                                                    alt="Giày Tennis T19 xanh"
                                                                    title="Giày Tennis T19 xanh"
                                                                    src="http://runecom02.runtime.vn/Uploads/shop2198/images/product/p55_large.jpg"></a>
                                                            <div class="quick-view">
                                                                <a title="Add to my wishlist" class="heart"
                                                                    href="#"></a>
                                                                <a title="Xem chi tiết" class="compare"
                                                                    href="/san-pham/giay-tennis-t19-xanh.html"></a> <a
                                                                    href="javascript:void(0);"
                                                                    class="qv-e-button btn-quickview-1 search"
                                                                    title="Xem nhanh"
                                                                    data-handle="/san-pham/giay-tennis-t19-xanh.html"></a>
                                                            </div>
                                                            <div class="add-to-cart">
                                                                <a class="add-to-car" href="javascript:void(0);"
                                                                    ng-click="AddToCard(item)">Thêm vào giỏ</a>
                                                            </div>
                                                            <div class="price-percent-reduction2 ng-binding">
                                                                Sale <br>-12<strong>%</strong>
                                                            </div>
                                                        </div>
                                                        <div class="right-block">
                                                            <h5 class="product-name">
                                                                <a href="/san-pham/giay-tennis-t19-xanh.html"
                                                                    class="ng-binding">Giày Tennis T19 xanh</a>
                                                            </h5>
                                                            <!-- ngIf: ConfigProduct.ShowPrice==true -->
                                                            <div class="content_price ng-scope"
                                                                ng-if="ConfigProduct.ShowPrice==true">
                                                                <!-- ngIf: item.IsPromotion==true&&item.Price>0 -->
                                                                <span class="price product-price ng-binding ng-scope"
                                                                    ng-if="item.IsPromotion==true&amp;&amp;item.Price>0">790,000&nbsp;₫</span>
                                                                <!-- end ngIf: item.IsPromotion==true&&item.Price>0 -->
                                                                <!-- ngIf: item.IsPromotion==true&&item.Price>0 -->
                                                                <span class="price old-price ng-binding ng-scope"
                                                                    ng-if="item.IsPromotion==true&amp;&amp;item.Price>0">900,000&nbsp;₫</span>
                                                                <!-- end ngIf: item.IsPromotion==true&&item.Price>0 -->
                                                                <!-- ngIf: item.IsPromotion==false&&item.Price>0 -->
                                                                <!-- ngIf: item.Price<=0 -->
                                                            </div>
                                                            <!-- end ngIf: ConfigProduct.ShowPrice==true -->
                                                            <!-- ngIf: ConfigProduct.ShowPrice==false -->
                                                        </div>
                                                    </li>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="owl-controls">
                                            <div class="owl-nav">
                                                <div class="owl-prev" style="">
                                                    <i class="fa fa-angle-left"></i>
                                                </div>
                                                <div class="owl-next" style="">
                                                    <i class="fa fa-angle-right"></i>
                                                </div>
                                            </div>
                                            <div class="owl-dots" style="display: none;"></div>
                                        </div>
                                    </ul>
                                </div>
                            </div>
                            <!--End-->
                        </div>
                    </div>
                </div>
            </div>
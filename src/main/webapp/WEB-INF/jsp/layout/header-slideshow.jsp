<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
		<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
			<div class="slideshow">
				<div class="container">
					<div class="row">
						<div class="col-md-3"></div>
						<div class="col-md-9 ">


							<div id="home-slider">
								<div class="header-top-right">
									<div class="homeslider">
										<div class="bx-wrapper" style="max-width: 100%;">






											<div class="bx-viewport"
												style="width: 100%; overflow: hidden; position: relative; height: 450px;">

												<c:forEach var="pr" items="${slide}">

													<img class="img-responsive mySlides" alt="03" src="${pr.url}">

												</c:forEach>
											</div>
											<div class="bx-controls bx-has-pager bx-has-controls-direction">
												<div class="bx-controls-direction">
													<a class="bx-prev" onclick="plusDivs(-1)"><i
															class="fa fa-angle-left"></i></a><a class="bx-next"
														id="nextt" onclick="plusDivs(1)"><i
															class="fa fa-angle-right"></i></a>
												</div>
											</div>



											<script>
												var slideIndex = 1;
												showDivs(slideIndex);

												function plusDivs(n) {
													showDivs(slideIndex += n);
												}

												function showDivs(n) {
													var i;
													var x = document.getElementsByClassName("mySlides");
													if (n > x.length) { slideIndex = 1 }
													if (n < 1) { slideIndex = x.length }
													for (i = 0; i < x.length; i++) {
														x[i].style.display = "none";
													}
													x[slideIndex - 1].style.display = "block";
												}

												window.onload = timedText;

												function timedText() { 
													 setInterval(function () {   
														this.plusDivs(1)
													}, 2000);
												}
											</script>




										</div>
									</div>
								</div>
							</div>
							<!--Begin-->
							<!--End-->
						</div>
					</div>
				</div>
			</div>
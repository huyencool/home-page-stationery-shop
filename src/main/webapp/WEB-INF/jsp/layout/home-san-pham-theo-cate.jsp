<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
		<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
			<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>



				<div class="content-page">
					<div class="container">


						<c:forEach var="s" items="${listCategoryAndProduct}">
							<div class="category-featured featured1">
								<nav class="navbar nav-menu show-brand">
									<div class="container">
										<!-- Brand and toggle get grouped for better mobile display -->
										<div class="navbar-brand">
											<a href="http://localhost:8099/advpro/view/showProductWithCate/${s.id}">
												<p style="font-size: 15px;"><img
													src="http://runecom02.runtime.vn/Uploads/shop2198/images/icon/s5.png">${s.name}</p></a>
										</div>
										<!-- Collect the nav links, forms, and other content for toggling -->
										<div class="collapse navbar-collapse">
											<ul class="nav navbar-nav"> 
												<c:forEach var="c" items="${s.childrent}">

													<li><a class="sub" data-toggle="tab"
															href="http://localhost:8099/advpro/view/showProductWithCate/${c.id}"
															data-code="40422">${c.name}</a></li>
												</c:forEach>
											</ul>
										</div>
										<!-- /.navbar-collapse -->
									</div>
									<!-- /.container-fluid -->

								</nav>
								<div class="product-featured clearfix">
									<div class="row"> 
										<div class="col-sm-12 col-left-tab">
											<div class="product-featured-tab-content">
												<div class="tab-container">
													<div class="tab-panel active" id="tab_1" style="display: flex;">

														<div>
															<ul class="product-list row">
																<c:forEach var="pr" begin="0" end="7"
																	items="${s.products}"> 
																	<li class="col-sm-3">
																		<div class="right-block">
																			<h5 class="product-name">
																				<a
																					href="http://localhost:8099/advpro/view/showpro/${pr.id}">${pr.name}</a>

																			</h5>
																			<div class="content_price">
																				<span class="price product-price">
																					${pr.priceFm}₫
																				</span>
																				<span class="price old-price">
																					${pr.priceMM}₫</span>
																			</div>
																		</div>
																		<div class="left-block">
																			<a
																				href="http://localhost:8099/advpro/view/showpro/${pr.id}"><img
																					class="img-responsive" alt="product"
																					src="http://localhost:8085/da/public/getImage/${pr.imageUrl}" style="
																max-width: 250px;
																max-height: 150px;
															"></a>
																			<div class="quick-view">
																				<a title="Add to my wishlist"
																					class="heart"
																					href="http://localhost:8099/advpro/view/showpro/${pr.id}"></a>
																				<a title="Xem chi tiết" class="compare"
																					href="http://localhost:8099/advpro/view/showpro/${pr.id}"></a>
																				<a href="javascript:void(0);"
																					class="qv-e-button btn-quickview-1 search"
																					title="Xem nhanh"
																					data-handle="/san-pham/dam-body-ca-tinh-voi-nhieu-mau-sac-hien-dai-tre-trung.html"></a>
																			</div>
																			<div class="add-to-cart">
																				<a class="add-to-car"
																					href="http://localhost:8099/advpro/view/showpro/${pr.id}">Thêm
																					vào giỏ</a>
																			</div>
																		</div>
																	</li>
																</c:forEach>


															</ul>
														</div>
													</div>
													<div class="tab-panel" id="tab_1_0">
														<div class="box-left">
															<div class="banner-img">
																<a href="http://localhost:8099/advpro/view/showProductWithCate"><img
																		src="http://runecom02.runtime.vn/Uploads/shop2198/images/banner/banner-product1.jpg"
																		alt="Thời trang"></a>
															</div>
														</div>
														<div class="box-right">
															<div class="loading"
																style="display: none; text-align: center; padding: 20px;">
																<img
																	src="http://runecom02.runtime.vn/assets/100002/img/ajax-loader.gif">
															</div>
															<ul class="product-list row"></ul>
														</div>
													</div>
													<div class="tab-panel" id="tab_1_1">
														<div class="box-left">
															<div class="banner-img">
																<a href="http://localhost:8099/advpro/view/showProductWithCate"><img
																		src="http://runecom02.runtime.vn/Uploads/shop2198/images/banner/banner-product1.jpg"
																		alt="Thời trang"></a>
															</div>
														</div>
														<div class="box-right">
															<div class="loading"
																style="display: none; text-align: center; padding: 20px;">
																<img
																	src="http://runecom02.runtime.vn/assets/100002/img/ajax-loader.gif">
															</div>
															<ul class="product-list row"></ul>
														</div>
													</div>
													<div class="tab-panel" id="tab_1_2">
														<div class="box-left">
															<div class="banner-img">
																<a href="http://localhost:8099/advpro/view/showProductWithCate"><img
																		src="http://runecom02.runtime.vn/Uploads/shop2198/images/banner/banner-product1.jpg"
																		alt="Thời trang"></a>
															</div>
														</div>
														<div class="box-right">
															<div class="loading"
																style="display: none; text-align: center; padding: 20px;">
																<img
																	src="http://runecom02.runtime.vn/assets/100002/img/ajax-loader.gif">
															</div>
															<ul class="product-list row"></ul>
														</div>
													</div>
													<div class="tab-panel" id="tab_1_3">
														<div class="box-left">
															<div class="banner-img">
																<a href="http://localhost:8099/advpro/view/showProductWithCate"><img
																		src="http://runecom02.runtime.vn/Uploads/shop2198/images/banner/banner-product1.jpg"
																		alt="Thời trang"></a>
															</div>
														</div>
														<div class="box-right">
															<div class="loading"
																style="display: none; text-align: center; padding: 20px;">
																<img
																	src="http://runecom02.runtime.vn/assets/100002/img/ajax-loader.gif">
															</div>
															<ul class="product-list row"></ul>
														</div>
													</div>
													<div class="tab-panel" id="tab_1_4">
														<div class="box-left">
															<div class="banner-img">
																<a href="http://localhost:8099/advpro/view/showProductWithCate"><img
																		src="http://runecom02.runtime.vn/Uploads/shop2198/images/banner/banner-product1.jpg"
																		alt="Thời trang"></a>
															</div>
														</div>
														<div class="box-right">
															<div class="loading"
																style="display: none; text-align: center; padding: 20px;">
																<img
																	src="http://runecom02.runtime.vn/assets/100002/img/ajax-loader.gif">
															</div>
															<ul class="product-list row"></ul>
														</div>
													</div>
													<div class="tab-panel" id="tab_1_5">
														<div class="box-left">
															<div class="banner-img">
																<a href="http://localhost:8099/advpro/view/showProductWithCate"><img
																		src="http://runecom02.runtime.vn/Uploads/shop2198/images/banner/banner-product1.jpg"
																		alt="Thời trang"></a>
															</div>
														</div>
														<div class="box-right">
															<div class="loading"
																style="display: none; text-align: center; padding: 20px;">
																<img
																	src="http://runecom02.runtime.vn/assets/100002/img/ajax-loader.gif">
															</div>
															<ul class="product-list row"></ul>
														</div>
													</div>
													<div class="tab-panel" id="tab_1_6">
														<div class="box-left">
															<div class="banner-img">
																<a href="http://localhost:8099/advpro/view/showProductWithCate"><img
																		src="http://runecom02.runtime.vn/Uploads/shop2198/images/banner/banner-product1.jpg"
																		alt="Thời trang"></a>
															</div>
														</div>
														<div class="box-right">
															<div class="loading"
																style="display: none; text-align: center; padding: 20px;">
																<img
																	src="http://runecom02.runtime.vn/assets/100002/img/ajax-loader.gif">
															</div>
															<ul class="product-list row"></ul>
														</div>
													</div>
													<div class="tab-panel" id="tab_1_7">
														<div class="box-left">
															<div class="banner-img">
																<a href="http://localhost:8099/advpro/view/showProductWithCate"><img
																		src="http://runecom02.runtime.vn/Uploads/shop2198/images/banner/banner-product1.jpg"
																		alt="Thời trang"></a>
															</div>
														</div>
														<div class="box-right">
															<div class="loading"
																style="display: none; text-align: center; padding: 20px;">
																<img
																	src="http://runecom02.runtime.vn/assets/100002/img/ajax-loader.gif">
															</div>
															<ul class="product-list row"></ul>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

						</c:forEach>
					</div>
				</div>

				</div>
				</div>
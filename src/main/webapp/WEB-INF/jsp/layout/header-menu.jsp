<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
		<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>


			<div id="nav-top-menu" class="nav-top-menu">
				<div class="container">
					<div class="row">
						<div class="col-sm-3" id="box-vertical-megamenus">
							<div class="box-vertical-megamenus menu-quick-select">
								<h4 class="title click-menu">
									<span class="title-menu">Danh mục sản phẩm</span> <span
										class="btn-open-mobile pull-right home-page"><i class="fa fa-bars"></i></span>
								</h4>
								<div class="vertical-menu-content is-home">
									<ul class="vertical-menu-list">


										<c:forEach var="s" items="${listCategory}">

											<li class="has-child level0">

												<c:if test="${s.childrent.size()>0}">
													<a class="dropdown-toggle has-category parent"
													href="http://localhost:8099/advpro/view/showProductWithCate/${s.id}"> <span>${s.name} </span>
													</a>
												</c:if>
												<c:if test="${s.childrent.size() == 0 && s.parent.id == null }">
													<a class="" href="http://localhost:8099/advpro/view/showProductWithCate/${s.id}">
														<span>${s.name}</span>
													</a>
												</c:if>

												<c:if test="${s.childrent.size()>0}">
													<ul class="level1 drop-menu">
														<c:forEach var="s" items="${s.childrent}"> 
															<li class="level1"><a class=""
																href="http://localhost:8099/advpro/view/showProductWithCate/${s.id}"	  ><span>${s.name}</span></a>
															</li> 
														</c:forEach>
													</ul>
												</c:if>
											</li>
										</c:forEach>




									</ul>
								</div>
							</div>
						</div>

						<jsp:include page="../layout/header-menu-notcatelogy.jsp" flush="true" />
					</div>
					<!-- userinfo on top-->
					<div id="form-search-opntop"></div>
					<!-- userinfo on top-->

					<!-- CART ICON ON MMENU -->
					<div id="shopping-cart-box-ontop" style="display: none;">
						<a href="http://localhost:8099/advpro/view/cart"></a> <span class="icon-cart-ontop"></span>
						<span class="cart-items-count">0</span>
						<span class="text">Giỏ hàng</span>
						<div class="shopping-cart-box-ontop-content"></div>
					</div>
				</div>
			</div>
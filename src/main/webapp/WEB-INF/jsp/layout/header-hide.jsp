<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<div id="header" class="header">

	<section class="top-link clearfix">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<ul class="nav navbar-nav topmenu-contact pull-left">
						 
					</ul>
					<ul
						class="nav navbar-nav navbar-right topmenu  hidden-xs hidden-sm">



						<c:choose>
							<c:when test="${userAll != null}">
								<li class="order-check"><a
									href="http://localhost:8099/advpro/view/donhang"><i
										class="fa fa-pencil-square-o"></i> Kiểm tra đơn hàng</a></li>
								<li class="order-cart"><i class="fa fa-shopping-cart"></i>
									Xin chào ${userAll.name}</a></li>
								<li class="order-check"><a
									href="http://localhost:8099/advpro/view/logout"><i
										class="fa fa-pencil-square-o"></i> Đăng xuất</a></li>
							</c:when>
							<c:otherwise>
								<li class="order-cart"><a
									href="http://localhost:8099/advpro/view/cart"><i
										class="fa fa-shopping-cart"></i> Giỏ hàng</a></li>
								<li class="account-login"><a
									href="http://localhost:8099/advpro/view/login"><i
										class="fa fa-sign-in"></i> Đăng nhập </a></li>
								<li class="account-register"><a
									href="http://localhost:8099/advpro/view/register"><i
										class="fa fa-key"></i> Đăng ký </a></li>
							</c:otherwise>
						</c:choose>



					</ul>
					<div class="show-mobile hidden-lg hidden-md">
						<div class="quick-user">
							<div class="quickaccess-toggle">
								<i class="fa fa-user"></i>
							</div>
							<div class="inner-toggle">
								<ul class="login links">

									<c:if test="userAll != null">
										<li><a href="http://localhost:8099/advpro/view/register"><i
												class="fa fa-sign-in"></i> Đăng ký</a></li>
										<li><a href="http://localhost:8099/advpro/view/login"><i
												class="fa fa-key"></i> Đăng nhập</a></li>
									</c:if>
								</ul>
							</div>
						</div>
						<div class="quick-access">
							<div class="quickaccess-toggle">
								<i class="fa fa-list"></i>
							</div>
							<div class="inner-toggle">
								<ul class="links">
									<li><a id="mobile-wishlist-total"
										href="http://localhost:8099/advpro/view/donhang" class="wishlist"><i
											class="fa fa-pencil-square-o"></i> Kiểm tra đơn hàng</a></li>
									<li><a href="http://localhost:8099/advpro/view/home"
										class="shoppingcart"><i class="fa fa-shopping-cart"></i>
											Giỏ hàng</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- MAIN HEADER -->
	<div class="container main-header">
		<div class="row">
			<div class="col-xs-12 col-sm-3 logo">
				<a href="http://localhost:8099/advpro/view/home" class="logo"
					title="CÔNG TY TNHH PHÁT TRIỂN CÔNG NGHỆ RUNTIME"> <img
					src="http://localhost:8099/advpro/img/logo.png""
					alt="CÔNG TY TNHH PHÁT TRIỂN CÔNG NGHỆ RUNTIME"
					title="CÔNG TY TNHH PHÁT TRIỂN CÔNG NGHỆ RUNTIME">
				</a>
				<h1 style="display: none;">CÔNG TY TNHH PHÁT TRIỂN CÔNG NGHỆ
					RUNTIME</h1>
			</div>
			<div class="col-xs-7 col-sm-7 header-search-box">
				<div class="search-box"></div>

			</div>
			<div class="col-xs-5 col-sm-2 group-button-header new-login">
				<div class="btn-cart" id="cart-block">
					<a title="My cart" href="http://localhost:8099/advpro/view/cart">Giỏ
						hàng</a> <span class="text-show">Giỏ hàng</span> <span
						class="notify notify-right">${totalQ}</span>
				</div>
			</div>
		</div>
	</div>
	<!-- END MANIN HEADER -->

	<!--Template--
    --End-->

	<jsp:include page="../layout/header-menu.jsp" flush="true" />


	<!--Template--
    --End-->
</div>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
		<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
			<div class="cart-content ng-scope" ng-controller="orderController">
				<h1 class="page-heading">
					<span>Giỏ hàng của tôi</span>
				</h1>

				<div class="payment-content ng-scope" ng-controller="orderController"
					ng-init="initCheckoutController()">
					<h1 class="page-heading">
						<span>Thanh toán</span>
					</h1>
					<div class="steps clearfix">
						<ul class="clearfix">
							<li
								class="cart active col-md-2 col-xs-12 col-sm-4 col-md-offset-3 col-sm-offset-0 col-xs-offset-0">
								<span> </span><span>Giỏ
									hàng của tôi</span><span class="step-number"><a>1</a></span>
							</li>
							<li class="payment active col-md-2 col-xs-12 col-sm-4"><span> </span><span>Thanh
									toán</span><span class="step-number"><a>2</a></span></li>
							<li class="finish col-md-2 col-xs-12 col-sm-4"><span> </span><span>Hoàn tất</span><span
									class="step-number"><a>3</a></span></li>
						</ul>
					</div>
					<div class="payment-block clearfix ng-pristine ng-invalid ng-invalid-required ng-valid-email"
						id="checkout-container" ng-submit="checkout()">


						<div class="col-md-4 col-sm-12 col-xs-12 payment-step step2">
							<h4>1. Địa chỉ thanh toán và giao hàng</h4>
							<div class="step-preview clearfix">
								<h2 class="title">Thông tin thanh toán</h2>
								<!-- ngIf: CustomerId>0 -->
								<!-- ngIf: CustomerId<=0 -->
								<div class="form-block ng-scope" ng-if="CustomerId<=0">
									<form:form modelAttribute="user" method="POST">
										<div class="user-login">
											<a href="http://localhost:8099/advpro/view/register">Đăng ký
												tài khoản mua hàng</a><a
												href="http://localhost:8099/advpro/view/login">Đăng nhập</a>
										</div>
										<div class="form-group">
											<form:input type="text"
												class="form-control ng-pristine ng-untouched ng-invalid ng-invalid-required"
												placeholder="Họ và tên" path="name" />
										</div>
										<div class="form-group">
											<form:input path="phone"
												class="form-control ng-pristine ng-untouched ng-invalid ng-invalid-required"
												placeholder="Số điện thoại" type="text" />
										</div>
										<div class="form-group">
											<form:input path="email" placeholder="Email" part="email"
												class="form-control ng-pristine ng-untouched ng-valid-email ng-invalid ng-invalid-required" />
										</div>
										<div class="form-group">
											<form:input type="text" placeholder="Địa chỉ"
												class="form-control ng-pristine ng-untouched ng-valid" path="address" />
										</div>
										<textarea class="form-control ng-pristine ng-untouched ng-valid" rows="4"
											placeholder="Ghi chú đơn hàng" ng-model="$parent.Description"></textarea>

										<button class="button-default" id="checkout" value="insert" name="insert">Đặt
											hàng</button>
										${mes}
								</div>
								<!-- end ngIf: CustomerId<=0 -->

							</div>
						</div>

						<div class="col-md-8 col-sm-8 col-xs-8 payment-step step1">
							<h4>2. Thông tin đơn hàng</h4>
							<div class="step-preview">
								<div class="cart-info">
									<c:forEach var="pr" items="${cart}">
										<div class="cart-items">
											<!-- ngRepeat: item in OrderDetails -->
											<div class="cart-item clearfix ng-scope" ng-repeat="item in OrderDetails">
												<span class="image pull-left" style="margin-right: 10px;">
													<a
														href="http://localhost:8099/advpro/view/showpro/${pr.product.id}">
														<img src="http://localhost:8085/da/public/getImage/${pr.product.imageUrl}"
															class="img-responsive">
													</a>
												</span>
												<div class="product-info pull-left">
													<span class="product-name"> <a
															href="/san-pham/dam-body-lap-the-tay-dai.html"
															class="ng-binding">${pr.product.name}</a>
													</span>
													<br> <br> <span class="ng-binding">Màu ${pr.color}</span>
													</br><br> 
													<span class="ng-binding">Số lượng ${pr.quantity}</span>
													<!-- ngIf: item.IsVariant==true -->
												</div>
												<span class="price ng-binding">${pr.total} đ</span>
											</div>
											<!-- end ngRepeat: item in OrderDetails -->
										</div>
									</br></br>
									</c:forEach>
									<div class="total-price">
										Thành tiền <label class="ng-binding"> ${total} ₫</label>
									</div>
									<div class="use-coupon hidden">
										<div class="form-group">
											<input placeholder="Nhập mã giảm giá" class="coupon-code form-control"> <a
												class="btn btn-primary">Sử dụng</a>
										</div>
									</div>
									<div class="total-payment">
										Thanh toán <span class="ng-binding">${total} ₫</span>
									</div>

								</div>


							</div>
						</div>
						</form:form>
					</div>

				</div>

				<style>
					a.qty {
						width: 1em;
						line-height: 1em;
						border-radius: 2em;
						font-size: 20px;
						font-weight: bold;
						text-align: center;
						background: #43ace3;
						color: #fff;
					}
				</style>
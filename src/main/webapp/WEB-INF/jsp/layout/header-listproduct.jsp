<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
		<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
			<div id="header" class="header">

				<section class="top-link clearfix">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<ul class="nav navbar-nav topmenu-contact pull-left">
								 
								</ul>
								<ul class="nav navbar-nav navbar-right topmenu  hidden-xs hidden-sm">
									<li class="order-check"><a href="http://localhost:8099/advpro/view/donhang"><i
												class="fa fa-pencil-square-o"></i> Kiểm tra đơn hàng</a></li>
									<li class="order-cart"><a href="http://localhost:8099/advpro/view/cart"><i
												class="fa fa-shopping-cart"></i> Giỏ hàng</a></li>
									<li class="account-login"><a href="http://localhost:8099/advpro/view/login"><i
												class="fa fa-sign-in"></i>
											Đăng nhập </a></li>
									<li class="account-register"><a href="http://localhost:8099/advpro/view/register"><i
												class="fa fa-key"></i> Đăng
											ký </a></li>
								</ul>
								<div class="show-mobile hidden-lg hidden-md">
									<div class="quick-user">
										<div class="quickaccess-toggle">
											<i class="fa fa-user"></i>
										</div>
										<div class="inner-toggle">
											<ul class="login links">
												<li>
													<a href="http://localhost:8099/advpro/view/register"><i
															class="fa fa-sign-in"></i> Đăng ký</a>
												</li>
												<li>
													<a href="http://localhost:8099/advpro/view/login"><i
															class="fa fa-key"></i> Đăng nhập</a>
												</li>
											</ul>
										</div>
									</div>
									<div class="quick-access">
										<div class="quickaccess-toggle">
											<i class="fa fa-list"></i>
										</div>
										<div class="inner-toggle">
											<ul class="links">
												<li><a id="mobile-wishlist-total" href="http://localhost:8099/advpro/view/donhang"
														class="wishlist"><i class="fa fa-pencil-square-o"></i> Kiểm tra
														đơn hàng</a></li>
												<li><a href="http://localhost:8099/advpro/view/cart"
														class="shoppingcart"><i class="fa fa-shopping-cart"></i> Giỏ
														hàng</a></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>

				<!-- MAIN HEADER -->



				<div class="container main-header">
					<div class="row">
						<div class="col-xs-12 col-sm-3 logo">
							<a href="http://localhost:8099/advpro/view/home" class="logo"
								title="CÔNG TY TNHH PHÁT TRIỂN CÔNG NGHỆ RUNTIME">
								<img src="http://runecom02.runtime.vn//Uploads/shop2198/images/logo2.png"
									alt="CÔNG TY TNHH PHÁT TRIỂN CÔNG NGHỆ RUNTIME"
									title="CÔNG TY TNHH PHÁT TRIỂN CÔNG NGHỆ RUNTIME">
							</a>
							<h1 style="display: none;">
								CÔNG TY TNHH PHÁT TRIỂN CÔNG NGHỆ RUNTIME
							</h1>
						</div>
						<div class="col-xs-7 col-sm-7 header-search-box">
							<div class="search-box">

							</div>
							<form action="http://localhost:8099/advpro/view/searchProduct" method="POST"
								class="search form-inline ng-pristine ng-valid">
								<div class="form-group input-serach">
									<input type="text" name="keyword" class="search_box" id="txtsearch"
										placeholder='Nhập từ khóa tìm kiếm...'>
									<input type="hidden" name="pageIndex" class="search_box" id="txtsearch" value='1'
										hidden>
									<input type="hidden" name="pageSize" class="search_box" id="txtsearch" value='10'
										hidden>
								</div>
								<button type="submit" name="insert" id="btnsearch" class="pull-right btn-search"> <span
										class="hidden-400">Tìm kiếm</span>
									<span class="show-400"><i class="fa fa-search"
											aria-hidden="true"></i></span></button>
							</form>
						</div>
						<div class="col-xs-5 col-sm-2 group-button-header new-login">
							<div class="btn-cart" id="cart-block">
								<a title="My cart" href="http://localhost:8099/advpro/view/cart">Giỏ hàng</a>
								<span class="text-show">Giỏ hàng</span>
								<span class="notify notify-right">${totalQ}</span>
							</div>
						</div>
					</div>
				</div>
				<!-- END MANIN HEADER -->

				<!--Template--
--End-->

				<div id="nav-top-menu" class="nav-top-menu">
					<div class="container">
						<div class="row">
							<div class="col-sm-3" id="box-vertical-megamenus">
								<div class="box-vertical-megamenus menu-quick-select">
									<h4 class="title click-menu">
										<span class="title-menu">Danh mục sản phẩm</span>
										<span class="btn-open-mobile pull-right home-page"><i
												class="fa fa-bars"></i></span>
									</h4>

								</div>
							</div>
							<div id="main-menu-new" class="col-sm-12 col-md-9 main-menu">
								<nav class="navbar navbar-default">
									<div class="container-fluid">
										<div class="navbar-header">
											<button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
												data-target="#new-menu" aria-expanded="false" aria-controls="navbar">
												<i class="fa fa-bars"></i>
											</button>
											<a class="navbar-brand" href="#">MENU</a>
										</div>
										<div id="new-menu" class="navbar-collapse collapse">
											<ul class="menu t-menu nav">
												<li class="level0"><a class="" href="http://localhost:8099/advpro/view/home"><span>Trang
													chủ</span></a></li>
										<li class="level0"><a class=""
												href="http://localhost:8099/advpro/view/showProductWithCate"><span>Sản
													phẩm</span></a></li>
										<li class="level0"><a class="" href="http://localhost:8099/advpro/view/news"><span>Giới
													thiệu</span></a></li>
										<li class="level0"><a class=""
												href="http://localhost:8099/advpro/view/shownews"><span>Tin
													tức</span></a></li>
										<li class="level0"><a class=""
												href="http://localhost:8099/advpro/view/contact"><span>Liên hệ</span></a></li>
											</ul>
										</div>
										<!--/.nav-collapse -->
									</div>
								</nav>
							</div>
						</div>
						<!-- userinfo on top-->
						<div id="form-search-opntop">
						</div>
						<!-- userinfo on top-->

						<!-- CART ICON ON MMENU -->
						<div id="shopping-cart-box-ontop" style="display: none;">
							<a href="http://localhost:8099/advpro/view/cart"></a>
							<span class="icon-cart-ontop"></span>
							<span class="cart-items-count">0</span>
							<span class="text">Giỏ hàng</span>
							<div class="shopping-cart-box-ontop-content">
							</div>
						</div>
					</div>
				</div>
			</div>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
        <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
            <div id="main-menu-new" class="col-sm-12 col-md-9 main-menu">
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#new-menu" aria-expanded="false" aria-controls="navbar">
                                <i class="fa fa-bars"></i>
                            </button>
                            <a class="navbar-brand" href="#">MENU</a>
                        </div>
                        <div id="new-menu" class="navbar-collapse collapse">
                            <ul class="menu t-menu nav">
                                <li class="level0"><a class="" href="http://localhost:8099/advpro/view/home"><span>Trang
                                            chủ</span></a></li>
                                <li class="level0"><a class=""
                                        href="http://localhost:8099/advpro/view/showProductWithCate"><span>Sản
                                            phẩm</span></a></li>
                                <li class="level0"><a class="" href="http://localhost:8099/advpro/view/news"><span>Giới
                                            thiệu</span></a></li>
                                <li class="level0"><a class=""
                                        href="http://localhost:8099/advpro/view/shownews"><span>Tin
                                            tức</span></a></li>
                                <li class="level0"><a class=""
                                        href="http://localhost:8099/advpro/view/contact"><span>Liên hệ</span></a></li>
                            </ul>
                        </div>
                        <!--/.nav-collapse -->
                    </div>
                </nav>
            </div>
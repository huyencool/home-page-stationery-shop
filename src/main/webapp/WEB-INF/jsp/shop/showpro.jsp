<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%><html style=""
	class="supports-js supports-no-touch supports-csstransforms supports-no-csstransforms3d supports-fontface">

<head>
	<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
		<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
		<meta charset="UTF-8">
		<title>Stationery shop</title>
		<meta name="description">
		<meta name="keywords">
		<link href="img/logo2.png" rel="shortcut icon" type="image/x-icon">

		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta property="fb:app_id" content="227481454296289">

		<meta content="vi_VN" property="og:locale">
		<meta content="website" property="og:type">
		<meta content="CÔNG TY TNHH PHÁT TRIỂN CÔNG NGHỆ RUNTIME" property="og:title">
		<meta property="og:description">

		<meta content="kute-shop" property="og:site_name">
		<!--------------CSS----------->
		<link rel="stylesheet"
			href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<link href="${pageContext.servletContext.contextPath}/assets/css/bootstrap.min.css" rel="stylesheet">
		<link href="${pageContext.servletContext.contextPath}/assets/css/reset.css?v=42" rel="stylesheet"
			type="text/css" media="all">
		<link href="${pageContext.servletContext.contextPath}/assets/css/flexslider.css?v=42" rel="stylesheet"
			type="text/css" media="all">
		<link href="${pageContext.servletContext.contextPath}/assets/css/animate.css?v=42" rel="stylesheet"
			type="text/css" media="all">
		<link href="${pageContext.servletContext.contextPath}/assets/css/jquery.bxslider.css?v=42" rel="stylesheet"
			type="text/css" media="all">
		<link href="${pageContext.servletContext.contextPath}/assets/js/jquery.fancybox.css?v=42" rel="stylesheet"
			type="text/css" media="all">
		<link href="${pageContext.servletContext.contextPath}/assets/css/jquery-ui.css?v=42" rel="stylesheet"
			type="text/css" media="all">
		<link href="${pageContext.servletContext.contextPath}/assets/css/owl.carousel.css?v=42" rel="stylesheet"
			type="text/css" media="all">
		<link href="${pageContext.servletContext.contextPath}/assets/css/select2.min.css?v=42" rel="stylesheet"
			type="text/css" media="all">

		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="${pageContext.servletContext.contextPath}/assets/css/style.css" rel="stylesheet" type="text/css">
		<link href="${pageContext.servletContext.contextPath}/assets/css/responsive.css" rel="stylesheet"
			type="text/css">
</head>

<body ng-app="appMain" style="" class="home option2 ng-scope">
	<div id="fb-root" class=" fb_reset">
		<div style="position: absolute; top: -10000px; width: 0px; height: 0px;">
			<div></div>
		</div>
	</div>
	<div class="wrapper page-home">


		<jsp:include page="../layout/header-listproduct.jsp" flush="true" />
		<jsp:include page="../layout/detail-product.jsp" flush="true" />
		<div class="main">
			<div class="container">

				<div class="footer">

					<footer id="footer">
						<div class="container">
							<!-- introduce-box -->
							<div id="introduce-box" class="row">
								<div class="col-md-3">
									<div id="address-box">
										<a href="/"><img
												src="http://runecom02.runtime.vn//Uploads/shop2198/images/logo2.png"
												alt="logo"></a>
										<div id="address-list">
											<div class="tit-name">Địa chỉ:</div>
											<div class="tit-contain">301 Nguyễn Văn Giáp - phường Cầu diễn-Hà nội</div>
											<div class="tit-name">Điện thoại:</div>
											<div class="tit-contain">0349655474</div>
											<div class="tit-name">Email:</div>
											<div class="tit-contain">run02@runtime.vn</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="row">
										<div class="col-sm-4">
											<div class="introduce-title">Về chúng tôi</div>
											<ul class="introduce-list">
												<li class="item">
													<a href="/http://localhost:8099/advpro/view/news-thieu.html">
														Giới thiệu
													</a>
												</li>
												<li class="item">
													<a href="/content/giao-hang-doi-tra.html">
														Giao hàng - Đổi trả
													</a>
												</li>
												<li class="item">
													<a href="/content/chinh-sach-bao-mat.html">
														Chính sách bảo mật
													</a>
												</li>
												<li class="item">
													<a href="/lien-he.html">
														Liên hệ
													</a>
												</li>
											</ul>
										</div>
										<div class="col-sm-4">
											<div class="introduce-title">Trợ giúp</div>
											<ul class="introduce-list">
												<li class="item">
													<a href="/content/huong-dan-mua-hang.html">
														Hướng dẫn mua hàng
													</a>
												</li>
												<li class="item">
													<a href="/content/huong-dan-thanh-toan.html">
														Hướng dẫn thanh toán
													</a>
												</li>
												<li class="item">
													<a href="/content/tai-khoan-giao-dich.html">
														Tài khoản giao dịch
													</a>
												</li>
											</ul>
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div id="contact-box" ng-controller="moduleController" ng-init="initController()"
										class="ng-scope">
										<div class="introduce-title">Đăng ký nhận tin</div>
										<form ng-submit="registerNewsletter()"
											class="contact-form ng-pristine ng-valid-email ng-invalid ng-invalid-required">
											<div class="input-group" id="mail-box">
												<input ng-model="newsletter.Email" type="email"
													placeholder="Đăng ký email" required="required"
													class="ng-pristine ng-untouched ng-valid-email ng-invalid ng-invalid-required">
												<span class="input-group-btn">
													<button type="submit" class="btn btn-default">Gửi</button>
												</span>
											</div><!-- /input-group -->
										</form>
										<div class="introduce-title">Liên kết</div>
										<div class="social-link">
											<a><i class="fa fa-facebook"></i></a>
											<a><i class="fa fa-youtube"></i></a>
											<a><i class="fa fa-twitter"></i></a>
											<a><i class="fa fa-google-plus"></i></a>
										</div>
									</div>
								</div>
							</div><!-- /#introduce-box -->
							<!-- #trademark-box -->
							<div id="trademark-box" class="row">
								<div class="col-sm-12">
									<ul id="trademark-list">
										<li id="payment-methods">Phương thức thanh toán</li>
										<li><a href="javascript:;"><img
													src="http://runecom02.runtime.vn/assets/100002/img/trademark_1.jpg?v=42"
													alt="Phương thức thanh toán 1"></a></li>
										<li><a href="javascript:;"><img
													src="http://runecom02.runtime.vn/assets/100002/img/trademark_2.jpg?v=42"
													alt="Phương thức thanh toán 2"></a></li>
										<li><a href="javascript:;"><img
													src="http://runecom02.runtime.vn/assets/100002/img/trademark_3.jpg?v=42"
													alt="Phương thức thanh toán 3"></a></li>
										<li><a href="javascript:;"><img
													src="http://runecom02.runtime.vn/assets/100002/img/trademark_4.jpg?v=42"
													alt="Phương thức thanh toán 4"></a></li>
										<li><a href="javascript:;"><img
													src="http://runecom02.runtime.vn/assets/100002/img/trademark_5.jpg?v=42"
													alt="Phương thức thanh toán 5"></a></li>
										<li><a href="javascript:;"><img
													src="http://runecom02.runtime.vn/assets/100002/img/trademark_6.jpg?v=42"
													alt="Phương thức thanh toán 6"></a></li>
										<li><a href="javascript:;"><img
													src="http://runecom02.runtime.vn/assets/100002/img/trademark_7.jpg?v=42"
													alt="Phương thức thanh toán 7"></a></li>
										<li><a href="javascript:;"><img
													src="http://runecom02.runtime.vn/assets/100002/img/trademark_8.jpg?v=42"
													alt="Phương thức thanh toán 8"></a></li>
										<li><a href="javascript:;"><img
													src="http://runecom02.runtime.vn/assets/100002/img/trademark_9.jpg?v=42"
													alt="Phương thức thanh toán 9"></a></li>
										<li><a href="javascript:;"><img
													src="http://runecom02.runtime.vn/assets/100002/img/trademark_10.jpg?v=42"
													alt="Phương thức thanh toán 10"></a></li>
									</ul>
								</div>
							</div> <!-- /#trademark-box -->
							<p class="cpr text-center">
								© Bản quyền thuộc về <a href="http://runtime.vn/" style="color: #0f9ed8"
									target="_blank">RUNTIME STORE</a> | <a target="_blank"
									href="https://www.runtime.vn">Powered by RUNTIME.VN</a>.
							</p>
						</div>
					</footer>

				</div>

			</div>


			<div style="display: none;" id="loading-mask">
				<div id="loading_mask_loader" class="loader">
					<img alt="Loading..." src="/Images/ajax-loader-main.gif">
					<div>
						Please wait...
					</div>
				</div>
			</div>
			<a href="#" class="scroll_top" title="Scroll to Top" style="display: none;">Scroll</a>





</body>

</html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
	<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
		<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

			<html style=""
				class="supports-js supports-no-touch supports-csstransforms supports-no-csstransforms3d supports-fontface">

			<head>
				<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
				<meta charset="UTF-8">
				<title>Stationery shop</title>
				<meta name="description">
				<meta name="keywords">
				<link href="img/logo2.png" rel="shortcut icon" type="image/x-icon">

				<meta name="viewport" content="width=device-width, initial-scale=1.0">
				<meta property="fb:app_id" content="227481454296289">

				<base href="${pageContext.servletContext.contextPath}" />
				<meta content="vi_VN" property="og:locale">
				<meta content="website" property="og:type">
				<meta content="CÔNG TY TNHH PHÁT TRIỂN CÔNG NGHỆ RUNTIME" property="og:title">
				<meta property="og:description">

				<meta content="kute-shop" property="og:site_name">
				<!--------------CSS----------->
				<link rel="stylesheet"
					href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
				<link href="${pageContext.servletContext.contextPath}/assets/css/bootstrap.min.css" rel="stylesheet">
				<link href="${pageContext.servletContext.contextPath}/assets/css/reset.css?v=42" rel="stylesheet"
					type="text/css" media="all">
				<link href="${pageContext.servletContext.contextPath}/assets/css/flexslider.css?v=42" rel="stylesheet"
					type="text/css" media="all">
				<link href="${pageContext.servletContext.contextPath}/assets/css/animate.css?v=42" rel="stylesheet"
					type="text/css" media="all">
				<link href="${pageContext.servletContext.contextPath}/assets/css/jquery.bxslider.css?v=42"
					rel="stylesheet" type="text/css" media="all">
				<link href="${pageContext.servletContext.contextPath}/assets/js/jquery.fancybox.css?v=42"
					rel="stylesheet" type="text/css" media="all">
				<link href="${pageContext.servletContext.contextPath}/assets/css/jquery-ui.css?v=42" rel="stylesheet"
					type="text/css" media="all">
				<link href="${pageContext.servletContext.contextPath}/assets/css/owl.carousel.css?v=42" rel="stylesheet"
					type="text/css" media="all">
				<link href="${pageContext.servletContext.contextPath}/assets/css/select2.min.css?v=42" rel="stylesheet"
					type="text/css" media="all">

				<link href="${pageContext.servletContext.contextPath}/assets/css/style.css" rel="stylesheet"
					type="text/css">
				<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
					<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
					<link href="${pageContext.servletContext.contextPath}/assets/css/responsive.css" rel="stylesheet"
						type="text/css">
			</head>

			<body ng-app="appMain" style="" class="home option2 ng-scope">
				<div id="fb-root" class=" fb_reset">
					<div style="position: absolute; top: -10000px; width: 0px; height: 0px;">
						<div></div>
					</div>
				</div>
				<div class="wrapper page-home">


					<jsp:include page="../layout/header-hide.jsp" flush="true" />

					<div class="main">
						<div class="container">
							<div class="row">
								<div class="col-md-3">

									<script src="/app/services/moduleServices.js"></script>
									<script src="/app/controllers/moduleController.js"></script>
									<!--Begin-->
									<div class="box-support-online ng-scope" ng-controller="moduleController"
										ng-init="initSupportOnlineController('Shop','SupportOnlines')">
										<h3><span>Hỗ trợ trực tuyến</span></h3>
										<div class="support-online-block">
											<div class="support-hotline">
												HOTLINE<br><span class="ng-binding">0349655474</span>
											</div>
											<!-- ngRepeat: item in SupportOnlines -->
										</div>
									</div>
									<!--End-->
									<script type="text/javascript">
										window.Shop = { "Name": "CÔNG TY TNHH PHÁT TRIỂN CÔNG NGHỆ RUNTIME", "Email": "thanhutt@utt.edu.vn", "Phone": "0349655474", "Logo": "/Uploads/shop2198/images/logo2.png", "Fax": "0349655474", "Website": "http://www.runtime.vn", "Hotline": "0349655474", "Address": "301 Nguyễn Văn Giáp - phường Cầu diễn-Hà nội", "Fanpage": null, "Google": null, "Facebook": null, "Youtube": null, "Twitter": null, "IsBanner": false, "IsFixed": false, "BannerImage": null };
										window.SupportOnlines = [];
									</script>
								</div>
								<div class="col-md-9">

									<div class="breadcrumb clearfix">
										<ul>
											<li itemtype="http://shema.org/Breadcrumb" itemscope="" class="home">
												<a title="Đến trang chủ" href="http://localhost:8099/advpro/view/home"
													itemprop="url"><span itemprop="title">Trang chủ</span></a>
											</li>
											<li class="icon-li"><strong>Liên hệ</strong> </li>
										</ul>
									</div>
									<script type="text/javascript">
										$(".link-site-more").hover(function () { $(this).find(".s-c-n").show(); }, function () { $(this).find(".s-c-n").hide(); });
									</script>

									<script
										src="http://maps.google.com/maps/api/js?key=AIzaSyBO93-_2pxx4UBTNduADxfoWpsFrHAFKsA&amp;sensor=true"
										type="text/javascript"></script>
									<script src="/app/services/contactServices.js"></script>
									<script src="/app/controllers/contactController.js"></script>
									<!--Begin-->
									<div class="contact-shop contact-content ng-scope" ng-controller="contactController"
										ng-init="initController('Shop','Maps')">
										<div id="layout-page">
											<div class="header-contact header-page clearfix">
												<h1>Liên hệ</h1>
											</div>
											<div class="content-contact content-page clearfix">
												<div class="map clearfix">
													<div style="width: 100%"><iframe width="100%" height="600"
															frameborder="0" scrolling="no" marginheight="0"
															marginwidth="0"
															src="https://maps.google.com/maps?width=100%25&amp;height=600&amp;hl=en&amp;q=301%20Nguy%E1%BB%85n%20V%C4%83n%20Gi%C3%A1p%20-%20ph%C6%B0%E1%BB%9Dng%20C%E1%BA%A7u%20di%E1%BB%85n-H%C3%A0%20n%E1%BB%99i+(Fasion%20shop)&amp;t=&amp;z=14&amp;ie=UTF8&amp;iwloc=B&amp;output=embed"></iframe><a
															href="https://www.maps.ie/route-planner.htm">Road Trip
															Planner</a></div>
												</div>

												<div class="col-md-5" id="col-right">
													<h3>Chúng tôi ở đây</h3>
													<hr class="line-right">
													<h3 class="name-company ng-binding">FASHION STORE</h3>
													<p class="ng-binding"></p>
													<ul class="info-address">
														<li>
															<span class="ng-binding">301 Nguyễn Văn Giáp - phường Cầu
																diễn-Hà nội</span>
														</li>
														<li>
															<i class="glyphicon glyphicon-envelope"></i>
															<span class="ng-binding">thanhutt@utt.edu.vn</span>
														</li>
														<li>
															<span class="ng-binding">0349655474</span>
														</li>
													</ul>
												</div>
											</div>
										</div>
									</div>

								</div>
							</div>
						</div>
						<jsp:include page="../layout/tin-tuc-noi-bat.jsp" flush="true" />


						<jsp:include page="../layout/footer.jsp" flush="true" />

						<div style="display: none;" id="loading-mask">
							<div id="loading_mask_loader" class="loader">
								<img alt="Loading..." src="/Images/ajax-loader-main.gif">
								<div>Please wait...</div>
							</div>
						</div>
						<a href="#" class="scroll_top" title="Scroll to Top" style="display: none;">Scroll</a>
			</body>

			</html>
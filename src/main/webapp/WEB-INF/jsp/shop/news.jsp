<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
	<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
		<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
			<html style=""
				class="supports-js supports-no-touch supports-csstransforms supports-no-csstransforms3d supports-fontface">

			<head>
				<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
					<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
					<meta charset="UTF-8">
					<title>Stationery shop</title>
					<meta name="description">
					<meta name="keywords">
					<link href="img/logo2.png" rel="shortcut icon" type="image/x-icon">

					<meta name="viewport" content="width=device-width, initial-scale=1.0">
					<meta property="fb:app_id" content="227481454296289">

					<meta content="vi_VN" property="og:locale">
					<meta content="website" property="og:type">
					<meta content="CÔNG TY TNHH PHÁT TRIỂN CÔNG NGHỆ RUNTIME" property="og:title">
					<meta property="og:description">

					<meta content="kute-shop" property="og:site_name">
					<!--------------CSS----------->
					<link rel="stylesheet"
						href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
					<link href="${pageContext.servletContext.contextPath}/assets/css/bootstrap.min.css"
						rel="stylesheet">
					<link href="${pageContext.servletContext.contextPath}/assets/css/reset.css?v=42" rel="stylesheet"
						type="text/css" media="all">
					<link href="${pageContext.servletContext.contextPath}/assets/css/flexslider.css?v=42"
						rel="stylesheet" type="text/css" media="all">
					<link href="${pageContext.servletContext.contextPath}/assets/css/animate.css?v=42" rel="stylesheet"
						type="text/css" media="all">
					<link href="${pageContext.servletContext.contextPath}/assets/css/jquery.bxslider.css?v=42"
						rel="stylesheet" type="text/css" media="all">
					<link href="${pageContext.servletContext.contextPath}/assets/js/jquery.fancybox.css?v=42"
						rel="stylesheet" type="text/css" media="all">
					<link href="${pageContext.servletContext.contextPath}/assets/css/jquery-ui.css?v=42"
						rel="stylesheet" type="text/css" media="all">
					<link href="${pageContext.servletContext.contextPath}/assets/css/owl.carousel.css?v=42"
						rel="stylesheet" type="text/css" media="all">
					<link href="${pageContext.servletContext.contextPath}/assets/css/select2.min.css?v=42"
						rel="stylesheet" type="text/css" media="all">

					<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
					<link href="${pageContext.servletContext.contextPath}/assets/css/style.css" rel="stylesheet"
						type="text/css">
					<link href="${pageContext.servletContext.contextPath}/assets/css/responsive.css" rel="stylesheet"
						type="text/css">
			</head>

			<body ng-app="appMain" style="" class="home option2 ng-scope">
				<div id="fb-root" class=" fb_reset">
					<div style="position: absolute; top: -10000px; width: 0px; height: 0px;">
						<div></div>
					</div>
				</div>
				<div class="wrapper page-home">

					<div id="header" class="header">

						<section class="top-link clearfix">
							<div class="container">
								<div class="row">
									<div class="col-md-12">
										<ul class="nav navbar-nav topmenu-contact pull-left">
											 
										</ul>
										<ul class="nav navbar-nav navbar-right topmenu  hidden-xs hidden-sm">



											<c:choose>
												<c:when test="${userAll != null}">
													<li class="order-check"><a
															href="http://localhost:8099/advpro/view/mycart"><i
																class="fa fa-pencil-square-o"></i> Kiểm tra đơn hàng</a>
													</li>
													<li class="order-cart"><i class="fa fa-shopping-cart"></i>
														Xin chào ${userAll.name}</a></li>
													<li class="order-check"><a
															href="http://localhost:8099/advpro/view/logout"><i
																class="fa fa-pencil-square-o"></i> Đăng xuất</a></li>
												</c:when>
												<c:otherwise>
													<li class="order-cart"><a
															href="http://localhost:8099/advpro/view/cart"><i
																class="fa fa-shopping-cart"></i> Giỏ hàng</a></li>
													<li class="account-login"><a
															href="http://localhost:8099/advpro/view/login"><i
																class="fa fa-sign-in"></i> Đăng nhập </a></li>
													<li class="account-register"><a
															href="http://localhost:8099/advpro/view/register"><i
																class="fa fa-key"></i> Đăng ký </a></li>
												</c:otherwise>
											</c:choose>
										</ul>
										<div class="show-mobile hidden-lg hidden-md">
											<div class="quick-user">
												<div class="quickaccess-toggle">
													<i class="fa fa-user"></i>
												</div>
												<div class="inner-toggle">
													<ul class="login links">
														<li>
															<a href="http://localhost:8099/advpro/view/register"><i
																	class="fa fa-sign-in"></i> Đăng ký</a>
														</li>
														<li>
															<a href="http://localhost:8099/advpro/view/login"><i
																	class="fa fa-key"></i> Đăng nhập</a>
														</li>
													</ul>
												</div>
											</div>
											<div class="quick-access">
												<div class="quickaccess-toggle">
													<i class="fa fa-list"></i>
												</div>
												<div class="inner-toggle">
													<ul class="links">
														<li><a id="mobile-wishlist-total"
																href="http://localhost:8099/advpro/view/donhang"
																class="wishlist"><i class="fa fa-pencil-square-o"></i>
																Kiểm tra đơn
																hàng</a></li>
														<li><a href="http://localhost:8099/advpro/view/cart"
																class="shoppingcart"><i class="fa fa-shopping-cart"></i>
																Giỏ hàng</a></li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</section>

						<!-- MAIN HEADER -->



						<div class="container main-header">
							<div class="row">
								<div class="col-xs-12 col-sm-3 logo">
									<a href="http://localhost:8099/advpro/view/home" class="logo"
										title="CÔNG TY TNHH PHÁT TRIỂN CÔNG NGHỆ RUNTIME">
										<img src="http://localhost:8099/advpro/img/logo.png""
											alt="CÔNG TY TNHH PHÁT TRIỂN CÔNG NGHỆ RUNTIME"
											title="CÔNG TY TNHH PHÁT TRIỂN CÔNG NGHỆ RUNTIME">
									</a>
									<h1 style="display: none;">
										CÔNG TY TNHH PHÁT TRIỂN CÔNG NGHỆ RUNTIME
									</h1>
								</div>
								<div class="col-xs-7 col-sm-7 header-search-box">
									<div class="search-box">

									</div>
									<form class="search form-inline ng-pristine ng-valid">
										<div class="form-group input-serach">
											<input type="text" name="search" class="search_box" id="txtsearch"
												onblur="if(this.value=='')this.value='Nhập từ khóa tìm kiếm...'"
												onfocus="if(this.value=='Nhập từ khóa tìm kiếm...')this.value=''"
												value="Nhập từ khóa tìm kiếm...">
										</div>
										<button id="btnsearch" class="pull-right btn-search">
											<span class="hidden-400">Tìm kiếm</span>
											<span class="show-400"><i class="fa fa-search"
													aria-hidden="true"></i></span>
										</button>
									</form>
								</div>
								<div class="col-xs-5 col-sm-2 group-button-header new-login">
									<div class="btn-cart" id="cart-block">
										<a title="My cart" href="http://localhost:8099/advpro/view/cart">Giỏ hàng</a>
										<span class="text-show">Giỏ hàng</span>
										<span class="notify notify-right">${totalQ}</span>
									</div>
								</div>
							</div>
						</div>
						<!-- END MANIN HEADER -->

						<!--Template--
	--End-->

						<div id="nav-top-menu" class="nav-top-menu">
							<div class="container">
								<div class="row">
									<div class="col-sm-3" id="box-vertical-megamenus">
										<div class="box-vertical-megamenus menu-quick-select">
											<h4 class="title click-menu">
												<span class="title-menu">Danh mục sản phẩm</span>
												<span class="btn-open-mobile pull-right home-page"><i
														class="fa fa-bars"></i></span>
											</h4>

										</div>
									</div>
									<div id="main-menu-new" class="col-sm-12 col-md-9 main-menu">
										<nav class="navbar navbar-default">
											<div class="container-fluid">
												<div class="navbar-header">
													<button type="button" class="navbar-toggle collapsed"
														data-toggle="collapse" data-target="#new-menu"
														aria-expanded="false" aria-controls="navbar">
														<i class="fa fa-bars"></i>
													</button>
													<a class="navbar-brand" href="#">MENU</a>
												</div>
												<div id="new-menu" class="navbar-collapse collapse">
													<ul class="menu t-menu nav">
														<li class="level0"><a class="" href="http://localhost:8099/advpro/view/home"><span>Trang
															chủ</span></a></li>
												<li class="level0"><a class=""
														href="http://localhost:8099/advpro/view/showProductWithCate"><span>Sản
															phẩm</span></a></li>
												<li class="level0"><a class="" href="http://localhost:8099/advpro/view/news"><span>Giới
															thiệu</span></a></li>
												<li class="level0"><a class=""
														href="http://localhost:8099/advpro/view/shownews"><span>Tin
															tức</span></a></li>
												<li class="level0"><a class=""
														href="http://localhost:8099/advpro/view/contact"><span>Liên hệ</span></a></li>
												 
													</ul>
												</div>
											</div>
										</nav>
									</div>
								</div>
								<!-- userinfo on top-->
								<div id="form-search-opntop">
								</div>
								<!-- userinfo on top-->

								<!-- CART ICON ON MMENU -->
								<div id="shopping-cart-box-ontop" style="display: none;">
									<a href="http://localhost:8099/advpro/view/cart"></a>
									<span class="icon-cart-ontop"></span>
									<span class="cart-items-count">0</span>
									<span class="text">Giỏ hàng</span>
									<div class="shopping-cart-box-ontop-content">
									</div>
								</div>
							</div>
						</div>
					</div>
					<div id="blog-template">
						<div class="main">
							<div class="container">
								<div class="row">
									<div class="col-md-9">

										<div class="breadcrumb clearfix">
											<ul>
												<li itemtype="http://shema.org/Breadcrumb" itemscope="" class="home">
													<a title="Đến trang chủ"
														href="http://localhost:8099/advpro/view/showProductWithCate"
														itemprop="url"><span itemprop="title">Trang
															chủ</span></a>
												</li>
												<li class="icon-li"><strong>Tin tức</strong> </li>
											</ul>
										</div>

										<div class="news-content">
											<h2 class="page-heading">
												<span class="page-heading-title2">Tất cả bài viết</span>
											</h2>
											<!-- Begin: Nội dung blog -->
											<ul class="blog-posts">

												<c:forEach var="pr" items="${listNews}">

													<li class="post-item">
														<article class="entry">
															<div class="row">
																<div class="col-sm-12">
																	<div class="entry-ci">
																		<h3 class="entry-title"><a
																				href="http://localhost:8099/advpro/view/new/${pr.id}">${pr.name}</a>
																		</h3>
																		<div class="entry-meta-data">
																			<span class="author">
																				<i class="fa fa-user"></i>
																				by: CÔNG TY TNHH PHÁT TRIỂN CÔNG NGHỆ
																				RUNTIME
																			</span>
																			<span class="comment-count">
																				<i class="fa fa-comment-o"></i> 0
																			</span>
																			<span class="date"><i
																					class="fa fa-calendar"></i>
																				${pr.createDateTime}</span>
																		</div>
																		<div class="entry-excerpt">
																			<p>${pr.title}</p>

																		</div>
																		<div class="entry-more">
																			<a
																				href="http://localhost:8099/advpro/view/new/${pr.id}">Xem
																				thêm</a>
																		</div>
																	</div>
																</div>
															</div>
														</article>
													</li>
												</c:forEach>
											</ul>
										</div>
									</div>
									<div class="col-md-3">

										<div id="left_column">
											<div class="block left-module">
												<p class="title_block">Danh mục tin tức</p>
												<div class="block_content">
													<!-- layered -->
													<div class="layered layered-category">
														<div class="layered-content">
															<ul class="tree-menu">
																<li><a href="/tin-tuc/kinh-doanh"><span
																			class="menu-icon"><i
																				class="fa fa-arrow-circle-o-right"></i></span>
																		<span class="menu-label">Kinh doanh</span></a>
																</li>
																<li><a href="/tin-tuc/thi-truong"><span
																			class="menu-icon"><i
																				class="fa fa-arrow-circle-o-right"></i></span>
																		<span class="menu-label">Thị trường</span></a>
																</li>
															</ul>
														</div>
													</div>
													<!-- ./layered -->
												</div>
											</div>
										</div>
										<div id="left_column">
											<div class="block left-module">
												<p class="title_block">Bài viết nổi bật</p>
												<div class="block_content">
													<!-- layered -->
													<div class="layered">
														<div class="layered-content">
															<ul class="blog-list-sidebar clearfix">
																<!--Begin: Bài viết mới nhất-->
																<c:forEach var="pr" items="${listNews}">

																	<li> 
																		<div class="post-info">
																			<h5 class="entry_title"><a
																					href="http://localhost:8099/advpro/view/new/${pr.id}">${pr.name}</a>
																			</h5>
																			<div class="post-meta">
																				<span class="date"><i
																						class="fa fa-calendar"></i>
																					${pr.createDateTime}</span>
																				<span class="comment-count">
																					<i class="fa fa-comment-o"></i> 0
																				</span>
																			</div>
																		</div>
																	</li>

																</c:forEach>
																<!--End: Bài viết mới nhất-->
															</ul>
														</div>
													</div>
													<!-- ./layered -->
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="main">
						<div class="container">

							<div class="footer">

								<footer id="footer">
									<div class="container">
										<!-- introduce-box -->
										<div id="introduce-box" class="row">
											<div class="col-md-3">
												<div id="address-box">
													<a href="/"><img
															src="http://localhost:8099/advpro/img/logo.png""
															alt="logo"></a>
													<div id="address-list">
														<div class="tit-name">Địa chỉ:</div>
														<div class="tit-contain">301 Nguyễn Văn Giáp - phường Cầu diễn-Hà nội</div>
														<div class="tit-name">Điện thoại:</div>
														<div class="tit-contain">0349655474</div>
														<div class="tit-name">Email:</div>
														<div class="tit-contain">thanhutt@utt.edu.vn</div>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="row">
													<div class="col-sm-4">
														<div class="introduce-title">Về chúng tôi</div>
														<ul class="introduce-list">
															<li class="item">
																<a
																	href="/http://localhost:8099/advpro/view/news-thieu.html">
																	Giới thiệu
																</a>
															</li>
															<li class="item">
																<a href="/content/giao-hang-doi-tra.html">
																	Giao hàng - Đổi trả
																</a>
															</li>
															<li class="item">
																<a href="/content/chinh-sach-bao-mat.html">
																	Chính sách bảo mật
																</a>
															</li>
															<li class="item">
																<a href="/lien-he.html">
																	Liên hệ
																</a>
															</li>
														</ul>
													</div>
													<div class="col-sm-4">
														<div class="introduce-title">Trợ giúp</div>
														<ul class="introduce-list">
															<li class="item">
																<a href="/content/huong-dan-mua-hang.html">
																	Hướng dẫn mua hàng
																</a>
															</li>
															<li class="item">
																<a href="/content/huong-dan-thanh-toan.html">
																	Hướng dẫn thanh toán
																</a>
															</li>
															<li class="item">
																<a href="/content/tai-khoan-giao-dich.html">
																	Tài khoản giao dịch
																</a>
															</li>
														</ul>
													</div>
												</div>
											</div>
											<div class="col-md-3">
												<div id="contact-box" ng-controller="moduleController"
													ng-init="initController()" class="ng-scope">
													<div class="introduce-title">Đăng ký nhận tin</div>
													<form ng-submit="registerNewsletter()"
														class="contact-form ng-pristine ng-valid-email ng-invalid ng-invalid-required">
														<div class="input-group" id="mail-box">
															<input ng-model="newsletter.Email" type="email"
																placeholder="Đăng ký email" required="required"
																class="ng-pristine ng-untouched ng-valid-email ng-invalid ng-invalid-required">
															<span class="input-group-btn">
																<button type="submit"
																	class="btn btn-default">Gửi</button>
															</span>
														</div><!-- /input-group -->
													</form>
													<div class="introduce-title">Liên kết</div>
													<div class="social-link">
														<a><i class="fa fa-facebook"></i></a>
														<a><i class="fa fa-youtube"></i></a>
														<a><i class="fa fa-twitter"></i></a>
														<a><i class="fa fa-google-plus"></i></a>
													</div>
												</div>
											</div>
										</div><!-- /#introduce-box -->
										<!-- #trademark-box -->
										<div id="trademark-box" class="row">
											<div class="col-sm-12">
												<ul id="trademark-list">
													<li id="payment-methods">Phương thức thanh toán</li>
													<li><a href="javascript:;"><img
																src="http://runecom02.runtime.vn/assets/100002/img/trademark_1.jpg?v=42"
																alt="Phương thức thanh toán 1"></a></li>
													<li><a href="javascript:;"><img
																src="http://runecom02.runtime.vn/assets/100002/img/trademark_2.jpg?v=42"
																alt="Phương thức thanh toán 2"></a></li>
													<li><a href="javascript:;"><img
																src="http://runecom02.runtime.vn/assets/100002/img/trademark_3.jpg?v=42"
																alt="Phương thức thanh toán 3"></a></li>
													<li><a href="javascript:;"><img
																src="http://runecom02.runtime.vn/assets/100002/img/trademark_4.jpg?v=42"
																alt="Phương thức thanh toán 4"></a></li>
													<li><a href="javascript:;"><img
																src="http://runecom02.runtime.vn/assets/100002/img/trademark_5.jpg?v=42"
																alt="Phương thức thanh toán 5"></a></li>
													<li><a href="javascript:;"><img
																src="http://runecom02.runtime.vn/assets/100002/img/trademark_6.jpg?v=42"
																alt="Phương thức thanh toán 6"></a></li>
													<li><a href="javascript:;"><img
																src="http://runecom02.runtime.vn/assets/100002/img/trademark_7.jpg?v=42"
																alt="Phương thức thanh toán 7"></a></li>
													<li><a href="javascript:;"><img
																src="http://runecom02.runtime.vn/assets/100002/img/trademark_8.jpg?v=42"
																alt="Phương thức thanh toán 8"></a></li>
													<li><a href="javascript:;"><img
																src="http://runecom02.runtime.vn/assets/100002/img/trademark_9.jpg?v=42"
																alt="Phương thức thanh toán 9"></a></li>
													<li><a href="javascript:;"><img
																src="http://runecom02.runtime.vn/assets/100002/img/trademark_10.jpg?v=42"
																alt="Phương thức thanh toán 10"></a></li>
												</ul>
											</div>
										</div> <!-- /#trademark-box -->
										<p class="cpr text-center">
											© Bản quyền thuộc về <a href="http://runtime.vn/" style="color: #0f9ed8"
												target="_blank">RUNTIME STORE</a> | <a target="_blank"
												href="https://www.runtime.vn">Powered by RUNTIME.VN</a>.
										</p>
									</div>
								</footer>

							</div>

						</div>


						<div style="display: none;" id="loading-mask">
							<div id="loading_mask_loader" class="loader">
								<img alt="Loading..." src="/Images/ajax-loader-main.gif">
								<div>
									Please wait...
								</div>
							</div>
						</div>
						<a href="#" class="scroll_top" title="Scroll to Top" style="display: none;">Scroll</a>





			</body>

			</html>
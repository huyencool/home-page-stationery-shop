 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html style=""
	class="supports-js supports-no-touch supports-csstransforms supports-no-csstransforms3d supports-fontface">

<head>
	<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
		<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
		<meta charset="UTF-8">
		<title>Stationery shop</title>
		<meta name="description">
		<meta name="keywords">
		<link href="img/logo2.png" rel="shortcut icon" type="image/x-icon">

		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta property="fb:app_id" content="227481454296289">

		<meta content="vi_VN" property="og:locale">
		<meta content="website" property="og:type">
		<meta content="CÔNG TY TNHH PHÁT TRIỂN CÔNG NGHỆ RUNTIME" property="og:title">
		<meta property="og:description">

		<base href="${pageContext.servletContext.contextPath}" />
		<meta content="kute-shop" property="og:site_name">
		<!--------------CSS----------->
		<link rel="stylesheet"
			href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<link href="${pageContext.servletContext.contextPath}/assets/css/bootstrap.min.css" rel="stylesheet">
		<link href="${pageContext.servletContext.contextPath}/assets/css/reset.css?v=42" rel="stylesheet"
			type="text/css" media="all">
		<link href="${pageContext.servletContext.contextPath}/assets/css/flexslider.css?v=42" rel="stylesheet"
			type="text/css" media="all">
		<link href="${pageContext.servletContext.contextPath}/assets/css/animate.css?v=42" rel="stylesheet"
			type="text/css" media="all">
		<link href="${pageContext.servletContext.contextPath}/assets/css/jquery.bxslider.css?v=42" rel="stylesheet"
			type="text/css" media="all">
		<link href="${pageContext.servletContext.contextPath}/assets/js/jquery.fancybox.css?v=42" rel="stylesheet"
			type="text/css" media="all">
		<link href="${pageContext.servletContext.contextPath}/assets/css/jquery-ui.css?v=42" rel="stylesheet"
			type="text/css" media="all">
		<link href="${pageContext.servletContext.contextPath}/assets/css/owl.carousel.css?v=42" rel="stylesheet"
			type="text/css" media="all">
		<link href="${pageContext.servletContext.contextPath}/assets/css/select2.min.css?v=42" rel="stylesheet"
			type="text/css" media="all">

		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="${pageContext.servletContext.contextPath}/assets/css/style.css" rel="stylesheet" type="text/css">
		<link href="${pageContext.servletContext.contextPath}/assets/css/responsive.css" rel="stylesheet"
			type="text/css">
</head>

<body ng-app="appMain" style="" class="home option2 ng-scope">
	<div id="fb-root" class=" fb_reset">
		<div style="position: absolute; top: -10000px; width: 0px; height: 0px;">
			<div></div>
		</div>
	</div>
	<div class="wrapper page-home">

		<jsp:include page="../layout/header.jsp" flush="true" />
 
		<jsp:include page="../layout/donhang.jsp" flush="true" />
		<div class="main">
			<div class="container">
				<div class="row">
					<div class="col-md-12">

						<jsp:include page="../layout/tin-tuc-noi-bat.jsp" flush="true" />


						<jsp:include page="../layout/footer.jsp" flush="true" />


					</div>

				</div>

				<div style="display: none;" id="loading-mask">
					<div id="loading_mask_loader" class="loader">
						<img alt="Loading..." src="/Images/ajax-loader-main.gif">
						<div>Please wait...</div>
					</div>
				</div>
				<a href="#" class="scroll_top" title="Scroll to Top" style="display: none;">Scroll</a>
</body>

</html>
package com.example.demo.rest;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.example.demo.domain.Color;
import com.example.demo.domain.Customer;
import com.example.demo.domain.EQAProgramAnnouncement;
import com.example.demo.domain.Event;
import com.example.demo.domain.Order;
import com.example.demo.domain.Product;
import com.example.demo.domain.ProductCategory;
import com.example.demo.domain.ProductColor;
import com.example.demo.domain.ProductOrder;
import com.example.demo.domain.ProductWarehouse;
import com.example.demo.domain.Role;
import com.example.demo.domain.User;
import com.example.demo.dto.Cart;
import com.example.demo.dto.EQAProgramAnnouncementDto;
import com.example.demo.dto.EventDto;
import com.example.demo.dto.OrderDto;
import com.example.demo.dto.ProductCategoryDto;
import com.example.demo.dto.ProductDto;
import com.example.demo.dto.SlideShowDto;
import com.example.demo.dto.UserDto;
import com.example.demo.repository.ColorRepository;
import com.example.demo.repository.CustomerRepository;
import com.example.demo.repository.EQAProgramAnnouncementRepository;
import com.example.demo.repository.EventRepository;
import com.example.demo.repository.OrderRepository;
import com.example.demo.repository.ProductCategoryRepository;
import com.example.demo.repository.ProductColorRepository;
import com.example.demo.repository.ProductOrderRepository;
import com.example.demo.repository.ProductRepository;
import com.example.demo.repository.ProductWarehouseRepository;
import com.example.demo.repository.RoleRepository;
import com.example.demo.repository.SlideShowRepository;
import com.example.demo.repository.UserRepository;

@Transactional
@Controller
@RequestMapping(path = "/view")
public class HomeController {

	@Autowired
	EntityManager manager;

	@Autowired
	ProductCategoryRepository productCategoryRepository;
	@Autowired
	ProductRepository productRepository;
	@Autowired
	ProductOrderRepository productOrderRepository;
	@Autowired
	OrderRepository orderRepository;
	@Autowired
	UserRepository userRepository;
	@Autowired
	RoleRepository roleRepository;
	@Autowired
	SlideShowRepository slideShowRepository;
	@Autowired
	CustomerRepository customerRepository;
	@Autowired
	OrderRepository orderPepository;
	@Autowired
	EventRepository eventPepository;
	@Autowired
	ProductColorRepository productColorRepository;

	@Autowired
	ColorRepository colorRepository;

	@Autowired
	ProductWarehouseRepository pwr;

	@Autowired
	EQAProgramAnnouncementRepository eqaProgramAnnouncementRepository;
	UserDto userAll = null;

	OrderDto od = new OrderDto();
	Integer co = 0;

//
	public SessionFactory getSessionFactory() {
		Session session = manager.unwrap(Session.class);
		return session.getSessionFactory();
	}

	@RequestMapping(value = { "/success" }, method = RequestMethod.GET)
	public String success(ModelMap model, HttpSession session) {
		HashMap<String, Cart> cartItems = (HashMap<String, Cart>) session.getAttribute("myCartItems");

		List<Cart> listAll = new ArrayList<Cart>();
		if (cartItems != null && cartItems.size() > 0) {
			for (Entry<String, Cart> cart : cartItems.entrySet()) {
				Cart cartD = cart.getValue();
				listAll.add(cartD);
			}
		}
		model.put("total", totalPrice(cartItems));
		model.put("cart", listAll);
		if (userAll != null) {
			model.put("userAll", userAll);
		}
		model.put("order", od);
		co = totalQuantity(cartItems);
		model.put("totalQ", co);
		cartItems = null;
		session.setAttribute("myCartItems", cartItems);
		return "shop/success";
	}

	@RequestMapping(value = { "/success/{id}" }, method = RequestMethod.GET)
	public String success(ModelMap model, @PathVariable("id") UUID id, HttpSession session) {
		HashMap<String, Cart> cartItems = (HashMap<String, Cart>) session.getAttribute("myCartItems");

		List<Cart> listAll = new ArrayList<Cart>();
		if (cartItems != null && cartItems.size() > 0) {
			for (Entry<String, Cart> cart : cartItems.entrySet()) {
				Cart cartD = cart.getValue();
				listAll.add(cartD);
			}
		}
		model.put("total", totalPrice(cartItems));
		model.put("cart", listAll);
		if (userAll != null) {
			model.put("userAll", userAll);
		}
		co = totalQuantity(cartItems);
		model.put("totalQ", co);
		cartItems = null;
		session.setAttribute("myCartItems", cartItems);
		return "shop/success";
	}

//
	@RequestMapping(value = { "/cart" }, method = RequestMethod.GET)
	public String index1(ModelMap model, HttpSession session) {
		HashMap<String, Cart> cartItems = (HashMap<String, Cart>) session.getAttribute("myCartItems");

		List<Cart> listAll = new ArrayList<Cart>();
		if (cartItems != null && cartItems.size() > 0) {
			for (Entry<String, Cart> cart : cartItems.entrySet()) {
				Cart cartD = cart.getValue();
				String str = String.format("%,f", cartD.getProduct().getCurrentSellingPrice() * cartD.getQuantity())
						.replace(".000000", ""); 
				cartD.setTotal(str);
				listAll.add(cartD);
			}
		}
		model.put("total", totalPrice(cartItems));
		model.put("cart", listAll);
		if (userAll != null) {
			model.put("userAll", userAll);
		}
		co = totalQuantity(cartItems);
		model.put("totalQ", co);

		List<EQAProgramAnnouncementDto> page = eqaProgramAnnouncementRepository.getAllNews();
		model.put("listNews", page);
		return "shop/cart";
	}

	@RequestMapping(value = { "/donhang" }, method = RequestMethod.GET)
	public String donhang(ModelMap model, HttpSession session) {
		if (userAll != null) {
			model.put("userAll", userAll);
		} else {
			UserDto dto = new UserDto();
			model.put("user", dto);
			model.addAttribute("mes", "Bạn chưa đăng nhập, Xin mời đăng nhập để đặt hàng !");
			if (userAll != null) {
				model.put("userAll", userAll);
			}
			model.put("totalQ", co);
			return "redirect:/view/login";
		}
		List<OrderDto> page = orderRepository.getListOrderById(userAll.getCustomerId());
		model.put("cart", page);
		List<EQAProgramAnnouncementDto> pagea = eqaProgramAnnouncementRepository.getAllNews();
		model.put("listNews", pagea);
		model.put("totalQ", co);
		return "shop/donhang";
	}

	@RequestMapping(value = { "/donhang/detail/{id}" }, method = RequestMethod.GET)
	public String donhang(ModelMap model, @PathVariable("id") UUID id, HttpSession session) {
		if (userAll != null) {
			model.put("userAll", userAll);
		} else {
			UserDto dto = new UserDto();
			model.put("user", dto);
			model.addAttribute("mes", "Bạn chưa đăng nhập, Xin mời đăng nhập để đặt hàng !");
			if (userAll != null) {
				model.put("userAll", userAll);
			}
			model.put("totalQ", co);
			return "redirect:/view/login";
		}
		List<ProductOrder> page = productOrderRepository.getListProductOrder(id);
		model.put("cart", page);
		List<EQAProgramAnnouncementDto> pagea = eqaProgramAnnouncementRepository.getAllNews();
		model.put("listNews", pagea);
		Double tong = 0.0;

		List<Cart> listAll = new ArrayList<Cart>();
		if (page != null && page.size() > 0) {
			for (ProductOrder cart : page) {
				Cart cartD = new Cart();
				cartD.setProduct(new ProductDto(cart.getProduct()));
				cartD.setQuantity(cart.getProductNumber());

				String strs = String.format("%,f", cart.getProduct().getCurrentSellingPrice() * cart.getProductNumber())
						.replace(".000000", "");
				cartD.setTotal(strs);
				tong += cart.getProduct().getCurrentSellingPrice() * cart.getProductNumber();
				listAll.add(cartD);
			}
		}
		Order order = orderRepository.findById(id).get();
		OrderDto d = new OrderDto(order);
		d.setDateBuy(new SimpleDateFormat("dd-MM-yyyy").format(order.getCreateDate()));
		model.put("order", d);
		int no = tong.intValue();
		String str = String.format("%,d", no);
		model.put("total", str);
		model.put("cart", listAll);
		if (userAll != null) {
			model.put("userAll", userAll);
		}
		model.put("totalQ", co);
		return "shop/success";
	}

	@RequestMapping(value = { "/contact" }, method = RequestMethod.GET)
	public String contact(ModelMap model, HttpSession session) {

		if (userAll != null) {
			model.put("userAll", userAll);
		}
		List<EQAProgramAnnouncementDto> page = eqaProgramAnnouncementRepository.getAllNews();
		model.put("listNews", page);
		model.put("totalQ", co);
		return "shop/contact";
	}

	@RequestMapping(value = { "/cart-fake" }, method = RequestMethod.GET)
	public String index1fake(ModelMap model, HttpSession session) {
		HashMap<String, Cart> cartItems = (HashMap<String, Cart>) session.getAttribute("myCartItems");

		List<Cart> listAll = new ArrayList<Cart>();
		if (cartItems != null && cartItems.size() > 0) {
			for (Entry<String, Cart> cart : cartItems.entrySet()) {
				Cart cartD = cart.getValue();
				listAll.add(cartD);
			}
		}
		UserDto user = new UserDto();
		model.put("user", user);
		model.put("total", totalPrice(cartItems));
		model.put("cart", listAll);
		if (userAll != null) {
			model.put("userAll", userAll);
		}
		co = totalQuantity(cartItems);
		model.put("totalQ", co);
		return "shop/cart-fake";
	}

//	@RequestMapping(value =  {"/cart/plus/{id} "}, method = RequestMethod.GET)
	@RequestMapping(value = { "/cart/plus/{id}/{idColor}" }, method = RequestMethod.GET)
	public String updateCartUp(ModelMap model, HttpSession session, @PathVariable("id") UUID id,
			@PathVariable("idColor") UUID idColor) {

		if (userAll != null) {
			model.put("userAll", userAll);
		}
		HashMap<String, Cart> cartItems = (HashMap<String, Cart>) session.getAttribute("myCartItems");
		if (cartItems == null) {
			cartItems = new HashMap<>();
		}
		Product product = productRepository.findById(id).get();
		String keyId = idColor + id.toString();
		if (product != null) {
			if (cartItems.containsKey(keyId)) {
				Cart item = cartItems.get(keyId);
				item.setProduct(new ProductDto(product));
				item.setQuantity(item.getQuantity() + 1);

				String strs = String.format("%,f", item.getProduct().getCurrentSellingPrice() * item.getQuantity())
						.replace(".000000", "");
				item.setTotal(strs);
				cartItems.put(keyId, item);
			}
		}
		co = totalQuantity(cartItems);
		model.put("totalQ", co);
		session.setAttribute("myCartItems", cartItems);
		session.setAttribute("myCartTotal", totalPrice(cartItems));
		session.setAttribute("myCartNum", cartItems.size());

		return "redirect:/view/cart/";
	}

	@RequestMapping(value = { "/cart/remove/{id}/{idColor}" }, method = RequestMethod.GET)
	public String remove(ModelMap model, HttpSession session, @PathVariable("id") UUID id,
			@PathVariable("idColor") UUID idColor) {

		if (userAll != null) {
			model.put("userAll", userAll);
		}
		HashMap<String, Cart> cartItems = (HashMap<String, Cart>) session.getAttribute("myCartItems");
		if (cartItems == null) {
			cartItems = new HashMap<>();
		}
		Product product = productRepository.findById(id).get();
		String keyId = idColor + id.toString();
		if (product != null) {
			if (cartItems.containsKey(keyId)) {
				Cart item = cartItems.get(keyId);
				cartItems.remove(keyId);
			}
		}
		co = totalQuantity(cartItems);
		model.put("totalQ", co);
		session.setAttribute("myCartItems", cartItems);
		session.setAttribute("myCartTotal", totalPrice(cartItems));
		session.setAttribute("myCartNum", cartItems.size());

		return "redirect:/view/cart/";
	}

	@RequestMapping(value = { "/cart/minus/{id}/{idColor}" }, method = RequestMethod.GET)
	public String updateCartUpminus(ModelMap model, HttpSession session, @PathVariable("id") UUID id,
			@PathVariable("idColor") UUID idColor) {

		if (userAll != null) {
			model.put("userAll", userAll);
		}
		HashMap<String, Cart> cartItems = (HashMap<String, Cart>) session.getAttribute("myCartItems");
		if (cartItems == null) {
			cartItems = new HashMap<>();
		}
		Product product = productRepository.findById(id).get();
		String keyId = idColor + id.toString();
		if (product != null) {
			if (cartItems.containsKey(keyId)) {
				Cart item = cartItems.get(keyId);
				item.setProduct(new ProductDto(product));
				item.setQuantity(item.getQuantity() - 1);
				if (item.getQuantity() <= 0) {
					item.setQuantity(0);
				}

				String strs = String.format("%,f", item.getProduct().getCurrentSellingPrice() * item.getQuantity())
						.replace(".000000", "");
				item.setTotal(strs);
				cartItems.put(keyId, item);
			}
		}
		co = totalQuantity(cartItems);
		model.put("totalQ", co);
		session.setAttribute("myCartItems", cartItems);
		session.setAttribute("myCartTotal", totalPrice(cartItems));
		session.setAttribute("myCartNum", cartItems.size());

		return "redirect:/view/cart/";
	}

//
	@RequestMapping(value = { "/home" }, method = RequestMethod.GET)
	public String home(ModelMap model) {
		List<ProductCategory> page = productCategoryRepository.findAll();
		model.put("listCategory", page);
		List<ProductCategoryDto> pageB = getCateLimit();
		List<ProductCategoryDto> pageBA = new ArrayList<>();
		for (int i = 0; i < pageB.size(); i++) {
			List<ProductDto> pagea = productRepository.getListProduct(pageB.get(i).getId());
			ProductCategory o = productCategoryRepository.findById(pageB.get(i).getId()).get();
			ProductCategoryDto pageProduct = new ProductCategoryDto(o, pagea);
			pageBA.add(pageProduct);

		}
		List<EQAProgramAnnouncementDto> cxzc = eqaProgramAnnouncementRepository.getAllNews();
		model.put("listNews", cxzc);
		model.put("listCategoryAndProduct", pageBA);
		List<SlideShowDto> ageProduct = slideShowRepository.getListSlide();
		model.put("slide", ageProduct);
		if (userAll != null) {
			model.put("userAll", userAll);
		}

		List<EventDto> eventList = eventPepository.getListPage(new Date());
		model.put("eventList", eventList);

		model.put("totalQ", co);
		return "shop/home";
	}

	public List<ProductCategoryDto> getCateLimit() {

		String sql = "select new com.example.demo.dto.ProductCategoryDto(ed) from   ProductCategory ed order by createDate desc ";
		Query q = (Query) manager.createQuery(sql, ProductCategoryDto.class);
		q.setMaxResults(5);
		List<ProductCategoryDto> entities = q.getResultList();
		return entities;

	}

//
	@RequestMapping(value = { "/logout" }, method = RequestMethod.GET)
	public String logout(ModelMap model) {
		List<ProductCategory> page = productCategoryRepository.findAll();
		model.put("listCategory", page);
		List<ProductCategoryDto> pageB = new ArrayList<>();
		for (ProductCategory productCategory : page) {
			List<ProductDto> pagea = productRepository.getListProduct(productCategory.getId());
			ProductCategoryDto pageProduct = new ProductCategoryDto(productCategory, pagea);
			pageB.add(pageProduct);

		}
		List<EQAProgramAnnouncementDto> cxzc = eqaProgramAnnouncementRepository.getAllNews();
		model.put("listNews", cxzc);
		model.put("listCategoryAndProduct", pageB);
		List<SlideShowDto> ageProduct = slideShowRepository.getListSlide();
		model.put("slide", ageProduct);
		userAll = null;
		model.put("totalQ", co);
		return "shop/home";
	}

	@RequestMapping(value = { "/showProductWithCate" }, method = RequestMethod.GET)
	public String productCategory(ModelMap model) {
		List<ProductDto> pagea = productRepository.getAllProduct();
		List<ProductDto> newP = getProductNew();
		List<ProductCategory> page = productCategoryRepository.findAll();
		model.put("listCategory", page);
		model.put("listProduct", pagea);
		model.put("listProductNew", newP);
		if (userAll != null) {
			model.put("userAll", userAll);
		}
		model.put("totalQ", co);
		return "shop/product";
	}

	public List<ProductDto> getProductNew() {

		String sql = "select new com.example.demo.dto.ProductDto(ed) from Product ed  order by createDate desc ";
		Query q = (Query) manager.createQuery(sql, ProductDto.class);
		q.setMaxResults(6);
		List<ProductDto> entities = q.getResultList();
		return entities;

	}

	public List<ProductDto> getProductName(String name) {

		String sql = "select new com.example.demo.dto.ProductDto(ed) from Product ed  ";
		sql += "WHERE ed.name like :text order by createDate desc ";
		Query q = (Query) manager.createQuery(sql, ProductDto.class);
		q.setParameter("text", "%" + name + "%");
		q.setMaxResults(6);
		List<ProductDto> entities = q.getResultList();
		return entities;

	}

	@RequestMapping(value = { "/showProductWithCate/{id}" }, method = RequestMethod.GET)
	public String productCategory(ModelMap model, @PathVariable("id") UUID id) {
		List<ProductDto> pagea = productRepository.getListProduct(id);
		List<ProductDto> newP = getProductNew();
		List<ProductCategory> page = productCategoryRepository.findAll();
		model.put("listCategory", page);
		model.put("listProduct", pagea);
		model.put("listProductNew", newP);
		if (userAll != null) {
			model.put("userAll", userAll);
		}
		model.put("totalQ", co);
		return "shop/product";
	}

	@RequestMapping(value = "/searchProduct", params = "insert", method = RequestMethod.POST)
	public String searchProduct(ModelMap model, @Valid ProductDto searchDto) {
//		ProductSearchDto dto = new ProductSearchDto("test");
//		Set<String> categories = new HashSet<String>();
//		dto.setCategories(categories);
//		dto.setKeyword(searchDto.getKeyword());
//		Page<ProductDto> page = productServiceImpl.searchByPage(dto);
//		model.put("listProduct", page.getContent());
//		if (userAll != null) {
//			model.put("userAll", userAll);
//		}
		List<ProductDto> pagea = getProductName(searchDto.getKeyword());
		List<ProductDto> newP = getProductNew();
		List<ProductCategory> page = productCategoryRepository.findAll();
		model.put("listCategory", page);
		model.put("listProduct", pagea);
		model.put("listProductNew", newP);
		if (userAll != null) {
			model.put("userAll", userAll);
		}
		model.put("totalQ", co);
		return "shop/product";
	}

	@RequestMapping(value = { "/news" }, method = RequestMethod.GET)
	public String news(ModelMap model) {
		if (userAll != null) {
			model.put("userAll", userAll);
		}
		model.put("totalQ", co);
		return "shop/about";
	}

	@RequestMapping(value = { "/product" }, method = RequestMethod.GET)
	public String product(ModelMap model) {
		if (userAll != null) {
			model.put("userAll", userAll);
		}
		model.put("totalQ", co);
		return "shop/product";
	}

	@RequestMapping(value = { "/shownews" }, method = RequestMethod.GET)
	public String shownews(ModelMap model) {
		if (userAll != null) {
			model.put("userAll", userAll);
		}

		List<EQAProgramAnnouncementDto> page = eqaProgramAnnouncementRepository.getAllNews();
		model.put("listNews", page);
		model.put("totalQ", co);

		return "shop/news";
	}

	@RequestMapping(value = { "/new/{id}" }, method = RequestMethod.GET)
	public String shownews(ModelMap model, @PathVariable("id") UUID idnew) {
		if (userAll != null) {
			model.put("userAll", userAll);
		}
		EQAProgramAnnouncement eqap = new EQAProgramAnnouncement();

		eqap = eqaProgramAnnouncementRepository.findById(idnew).get();
		EQAProgramAnnouncementDto announcementDto = new EQAProgramAnnouncementDto(eqap);

		List<EQAProgramAnnouncementDto> page = eqaProgramAnnouncementRepository.getAllNews();
		model.put("listNews", page);
		model.put("news", announcementDto);
		model.put("totalQ", co);

		return "shop/shownews";
	}

	@RequestMapping(value = { "/login" }, method = RequestMethod.GET)
	public String login(ModelMap model) {
		UserDto dto = new UserDto();
		model.put("user", dto);
		model.put("totalQ", co);
		return "shop/login";
	}

	@RequestMapping(value = { "/login2" }, method = RequestMethod.GET)
	public String login2(ModelMap model) {
		UserDto dto = new UserDto();
		model.put("user", dto);
		model.addAttribute("mes", "Bạn chưa đăng nhập, Xin mời đăng nhập để đặt hàng !");
		if (userAll != null) {
			model.put("userAll", userAll);
		}
		model.put("totalQ", co);
		return "shop/login";
	}

	@RequestMapping(value = "/login", params = "insert", method = RequestMethod.POST)
	public String login23(ModelMap model, @ModelAttribute("user") UserDto dp) {
		if (userAll != null) {
			model.put("userAll", userAll);
		}
		UserDto user = checkLogin1(dp);
		if (user != null) {
			Customer c = customerRepository.findByUsername(user.getUsername());
			user.setCustomerId(c.getId());
			user.setName(c.getName());
			user.setAddress(c.getAddress());
			user.setPhone(c.getPhoneNumber());
			user.setCustomerId(c.getId());
			userAll = user;
			model.put("userAll", userAll);
			model.put("totalQ", co);
			return "redirect:/view/home/";
		} else {
			model.put("mes", "Sai tài khoản hoặc mật khẩu");
			return "shop/login";
		}

	}

	public UserDto checkLogin1(UserDto dto) {
		User user = userRepository.findByUsername(dto.getUsername());
		if (user != null && user.getId() != null) {
			if (dto.getUsername() != null && StringUtils.hasText(dto.getUsername().toString())) {
				if (dto.getUsername().equals(user.getUsername())) {
					if (dto.getPassword().equals(user.getPassword())) {
						return new UserDto(user);
					}
				}
			}
		}
		return null;
	}

	@RequestMapping(value = "/login2", params = "insert", method = RequestMethod.POST)
	public String login21(ModelMap model) {
//		if (userAll != null) {
//			model.put("userAll", userAll);
//		}
//		UserDto user = userService.checkLogin(dto);
//		if (user != null) {
//			userAll = user;
//			return "redirect:/view/home/";
//		} else {
//			model.put("mes", "Sai tài khoản hoặc mật khẩu");
		return "shop/login";
//		}

	}

	@RequestMapping(value = { "/register" }, method = RequestMethod.GET)
	public String register(ModelMap model) {
		UserDto dto = new UserDto();
		model.put("user", dto);
		model.put("totalQ", co);
		return "shop/register";
	}

	String pattern = "^[0-9]{10}$";

	String emailPattern = "\\w+@\\w+[.]\\w+";

	@RequestMapping(value = "/register", params = "insert", method = RequestMethod.POST)
	public String insert(ModelMap model, @ModelAttribute("user") UserDto dp) {
		User user = userRepository.findByUsername(dp.getUsername());

		String fail = "";
		if (user != null) {
			fail += "Tài khoản đã tồn tại. ";
		}
		if (userRepository.findByemail(dp.getEmail()) != null) {
			fail += "Email đã tồn tại. ";
		}
		if (dp.getUsername().length() <= 6 || dp.getUsername().contains(" ")
				|| dp.getUsername().matches(".*[a-zA-Z0-9].*0") || dp.getPassword().length() <= 6
				|| dp.getPassword().contains(" ") || dp.getPassword().matches(".*[a-zA-Z0-9].*0")) {
			fail += "Tài khoản hoặc mật khẩu không chứa dấu cách,ký tự và phải lớn hơn 6 kí tự. ";
		}
		if (!dp.getPhone().matches(pattern)) {
			fail += "Không đúng định dạng số điện thoại, 10 số. ";
			model.put("user", dp);
		}
		if (!dp.getEmail().matches(emailPattern)) {
			fail += "Không đúng định dạng email";
		}

		if (fail.length() > 0) {
			model.put("mes", fail);
			return register(model);
		}
		Customer cus = new Customer();
		cus.setAddress(dp.getAddress());
		cus.setName(dp.getName());
		cus.setPhoneNumber(dp.getPhone());
		cus.setUsername(dp.getUsername());
		cus.setCode("WIS");
		cus.setId(UUID.randomUUID());
		cus.setBirthDate(new Date());
		cus.setCreateDate(new Date());
		cus.setCreatedBy("Wis");
		customerRepository.save(cus);

		Role role = roleRepository.findByUsername("ROLE_USER");
		User entity = new User();
		entity.setUsername(dp.getUsername());
		entity.setPassword(dp.getPassword());
		entity.setEmail(dp.getEmail());
		entity.setCreateDate(new Date());
		entity.setCreatedBy("Wis");

		userRepository.save(entity);

		String querry = "SET FOREIGN_KEY_CHECKS=0;";
		String q = "INSERT INTO tbl_user_role values (" + entity.getId() + "," + role.getId() + ");";
		Session session = getSessionFactory().openSession();
		Transaction tx = session.beginTransaction();
		Query query = session.createNativeQuery(querry);
		Query queryq = session.createNativeQuery(q);
		query.executeUpdate();
		queryq.executeUpdate();
		return "redirect:/view/login/";
	}

	public UserDto checkLogin(UserDto dto) {
		User user = userRepository.findByUsername(dto.getUsername());
		if (user != null && user.getId() != null) {
			if (dto.getUsername() != null) {
				if (dto.getUsername().equals(user.getUsername())) {
					if (dto.getPassword().equals(user.getPassword())) {
						return new UserDto(user);
					}
				}
			}
		}
		return null;
	}

	@RequestMapping(value = { "/showpro/{id}" }, method = RequestMethod.GET)
	public String showpro(ModelMap model, @PathVariable("id") UUID id) {
		Product dto = productRepository.findById(id).get();
		ProductDto abc = new ProductDto(dto);
		model.put("product", abc);
		Cart cart = new Cart();
		cart.setProductId(id);
		List<ProductDto> newP = getProductNew();
		List<ProductCategory> page = productCategoryRepository.findAll();
		model.put("listCategory", page);
		model.put("listProductNew", newP);
		model.put("cart", cart);
		if (userAll != null) {
			model.put("userAll", userAll);
		}
		model.put("totalQ", co);
		return "shop/showpro";
	}

	@RequestMapping(value = { "/showpro/{id}/{sale}" }, method = RequestMethod.GET)
	public String showpro(ModelMap model, @PathVariable("id") UUID id, @PathVariable("sale") Float sale) {
		Product dto = productRepository.findById(id).get();
		dto.setCurrentSellingPrice(Double.parseDouble(sale + ""));
		model.put("product", new ProductDto(dto));
		Cart cart = new Cart();
		cart.setProductId(id);
		List<ProductDto> newP = getProductNew();
		List<ProductCategory> page = productCategoryRepository.findAll();
		model.put("listCategory", page);
		model.put("listProductNew", newP);
		model.put("cart", cart);
		if (userAll != null) {
			model.put("userAll", userAll);
		}
		model.put("totalQ", co);
		return "shop/showpro";
	}

	@RequestMapping(value = "/showpro/{id}/{sale}", params = "insert", method = RequestMethod.POST)
	public String insert(ModelMap model, @ModelAttribute("cart") Cart dp, HttpSession session,
			@PathVariable("id") UUID id, @PathVariable("sale") Float sale) {

		if (userAll != null) {
			model.put("userAll", userAll);
		}
		if (dp.getQuantity() == 0) {
			model.put("mes", "Số lượng phải lớn hơn 0. ");
			model.put("ss", "123");
			return showpro(model, id);
		}
		if (dp.getColorUUID().equals(UUID.fromString("c7a60edd-a031-4c03-b069-6f567dad5d41"))) {
			model.put("mes", "Xin mời chọn màu. ");
			model.put("ss", "123");
			return showpro(model, id);
		}
		HashMap<String, Cart> cartItems = (HashMap<String, Cart>) session.getAttribute("myCartItems");
		if (cartItems == null) {
			cartItems = new HashMap<>();
		}
		Product product = productRepository.findById(id).get();

		String keyId = dp.getColorUUID() + id.toString();

		if (product != null) {
			if (cartItems.containsKey(keyId)) {
				Cart item = cartItems.get(keyId);
				product.setCurrentSellingPrice(Double.parseDouble(sale + ""));
				item.setProduct(new ProductDto(product));
				item.setColor(dp.getColor());
				item.setColorUUID(dp.getColorUUID());
				if (item.getQuantity() == null ) {
					item.setQuantity(0);
				}
				for (ProductColor iterable_element : product.getProductColors()) {
					if(iterable_element.getColor().getId().equals(dp.getColorUUID())) {
						for (ProductWarehouse aaa : iterable_element.getProductWarehouse()) {
							if(iterable_element.getProductWarehouse().size()==0) {
								session.setAttribute("myCartItems", cartItems);
								session.setAttribute("myCartTotal", totalPrice(cartItems));
								session.setAttribute("myCartNum", cartItems.size());

								Product dto = productRepository.findById(id).get();
								model.put("product", new ProductDto(dto));
								List<ProductDto> newP = getProductNew();
								List<ProductCategory> page = productCategoryRepository.findAll();
								model.put("listCategory", page);
								model.put("listProductNew", newP);
								co = totalQuantity(cartItems);
								model.put("totalQ", totalQuantity(cartItems));
								if (userAll != null) {
									model.put("userAll", userAll);
								}
								model.put("mes", "Số lượng sản phẩm " + product.getName()
										+ " đã hết, vui lòng chọn sản phẩm hoặc mẫu khác");
								model.put("ss", "123");
								return "shop/showpro";
							}
							if( aaa.getProductNumber()>(dp.getQuantity()+item.getQuantity())) {  
								session.setAttribute("myCartItems", cartItems);
								session.setAttribute("myCartTotal", totalPrice(cartItems));
								session.setAttribute("myCartNum", cartItems.size());

								Product dto = productRepository.findById(id).get();
								model.put("product", new ProductDto(dto));
								List<ProductDto> newP = getProductNew();
								List<ProductCategory> page = productCategoryRepository.findAll();
								model.put("listCategory", page);
								model.put("listProductNew", newP);
								co = totalQuantity(cartItems);
								model.put("totalQ", totalQuantity(cartItems));
								if (userAll != null) {
									model.put("userAll", userAll);
								}
								model.put("mes", "Số lượng sản phẩm " + product.getName()
										+ " đã hết, vui lòng chọn sản phẩm hoặc mẫu khác");
								model.put("ss", "123");
								return "shop/showpro";
							}
						}
					}
				}

				String str = String.format("%,f", item.getProduct().getCurrentSellingPrice() * item.getQuantity())
						.replace(".000000", ""); 
				item.setQuantity(item.getQuantity() + dp.getQuantity());
				if (item.getQuantity() <= 0) {
					item.setQuantity(0);
				}
				item.setTotal(str);
				cartItems.put(keyId, item);
			} else {
				Cart item = new Cart();
				item.setProduct(new ProductDto(product));
				item.setColor(dp.getColor());
				item.setColorUUID(dp.getColorUUID()); 
				if (item.getQuantity() == null ) {
					item.setQuantity(0);
				}
				
				for (ProductColor iterable_element : product.getProductColors()) {
					if(iterable_element.getColor().getId().equals(dp.getColorUUID())) {
						if(iterable_element.getProductWarehouse().size()==0) {
							session.setAttribute("myCartItems", cartItems);
							session.setAttribute("myCartTotal", totalPrice(cartItems));
							session.setAttribute("myCartNum", cartItems.size());

							Product dto = productRepository.findById(id).get();
							model.put("product", new ProductDto(dto));
							List<ProductDto> newP = getProductNew();
							List<ProductCategory> page = productCategoryRepository.findAll();
							model.put("listCategory", page);
							model.put("listProductNew", newP);
							co = totalQuantity(cartItems);
							model.put("totalQ", totalQuantity(cartItems));
							if (userAll != null) {
								model.put("userAll", userAll);
							}
							model.put("mes", "Số lượng sản phẩm " + product.getName()
							+ " đã hết, vui lòng chọn sản phẩm hoặc mẫu khác");
							model.put("ss", "123");
							return "shop/showpro";
						}
						for (ProductWarehouse aaa : iterable_element.getProductWarehouse()) {
							if( aaa.getProductNumber() <(dp.getQuantity()+item.getQuantity())) {
								session.setAttribute("myCartItems", cartItems);
								session.setAttribute("myCartTotal", totalPrice(cartItems));
								session.setAttribute("myCartNum", cartItems.size());

								Product dto = productRepository.findById(id).get();
								model.put("product", new ProductDto(dto));
								List<ProductDto> newP = getProductNew();
								List<ProductCategory> page = productCategoryRepository.findAll();
								model.put("listCategory", page);
								model.put("listProductNew", newP);
								co = totalQuantity(cartItems);
								model.put("totalQ", totalQuantity(cartItems));
								if (userAll != null) {
									model.put("userAll", userAll);
								}
								model.put("mes", "Số lượng sản phẩm " + product.getName()
								+ " đã hết, vui lòng chọn sản phẩm hoặc mẫu khác");
								model.put("ss", "123");
								return "shop/showpro";
							}
						}
					}
				}

				String str = String.format("%,f", item.getProduct().getCurrentSellingPrice() * item.getQuantity())
						.replace(".000000", "");
				item.setQuantity(dp.getQuantity());
				if (item.getQuantity() <= 0) {
					item.setQuantity(0);
				}
				item.setTotal(str);
				cartItems.put(keyId, item);

			}
		}
		dp.setProductId(id);
		session.setAttribute("myCartItems", cartItems);
		session.setAttribute("myCartTotal", totalPrice(cartItems));
		session.setAttribute("myCartNum", cartItems.size());

		Product dto = productRepository.findById(id).get();
		model.put("product", new ProductDto(dto));
		List<ProductDto> newP = getProductNew();
		List<ProductCategory> page = productCategoryRepository.findAll();
		model.put("listCategory", page);
		model.put("listProductNew", newP);
		co = totalQuantity(cartItems);
		model.put("totalQ", totalQuantity(cartItems));
		if (userAll != null) {
			model.put("userAll", userAll);
		}

		model.put("mes", "Bạn đã thêm sản phẩm " + dto.getName() + ", số lượng " + dp.getQuantity()
				+ " vào giỏ hàng thành công!.");
		model.put("ss", "123");
		return "shop/showpro";
	}

	@RequestMapping(value = "/showpro/{id}", params = "insert", method = RequestMethod.POST)
	public String insert(ModelMap model, @ModelAttribute("cart") Cart dp, HttpSession session,
			@PathVariable("id") UUID id) {

		if (userAll != null) {
			model.put("userAll", userAll);
		}
		if (dp.getQuantity() == 0) {
			model.put("mes", "Số lượng phải lớn hơn 0. ");
			model.put("ss", "123");
			return showpro(model, id);
		}
		if (dp.getColorUUID().equals(UUID.fromString("c7a60edd-a031-4c03-b069-6f567dad5d41"))) {
			model.put("mes", "Xin mời chọn màu. ");
			model.put("ss", "123");
			return showpro(model, id);
		}
		HashMap<String, Cart> cartItems = (HashMap<String, Cart>) session.getAttribute("myCartItems");
		if (cartItems == null) {
			cartItems = new HashMap<>();
		}
		Product product = productRepository.findById(id).get();

		String keyId = dp.getColorUUID() + id.toString();

		if (product != null) {
			if (cartItems.containsKey(keyId)) {
				Cart item = cartItems.get(keyId);
				item.setProduct(new ProductDto(product));
				item.setColor(dp.getColor());
				item.setColorUUID(dp.getColorUUID());
				if (item.getQuantity() == null ) {
					item.setQuantity(0);
				}
				
				for (ProductColor iterable_element : product.getProductColors()) {
					if(iterable_element.getColor().getId().equals(dp.getColorUUID())) {
						if(iterable_element.getProductWarehouse().size() ==0) {
							session.setAttribute("myCartItems", cartItems);
							session.setAttribute("myCartTotal", totalPrice(cartItems));
							session.setAttribute("myCartNum", cartItems.size());

							Product dto = productRepository.findById(id).get();
							model.put("product", new ProductDto(dto));
							List<ProductDto> newP = getProductNew();
							List<ProductCategory> page = productCategoryRepository.findAll();
							model.put("listCategory", page);
							model.put("listProductNew", newP);
							co = totalQuantity(cartItems);
							model.put("totalQ", totalQuantity(cartItems));
							if (userAll != null) {
								model.put("userAll", userAll);
							}
							model.put("mes", "Số lượng sản phẩm " + product.getName()
									+ " đã hết, vui lòng chọn sản phẩm hoặc mẫu khác");
							model.put("ss", "123");
							return "shop/showpro";
						}
						for (ProductWarehouse aaa : iterable_element.getProductWarehouse()) {
							if(  aaa.getProductNumber()<(dp.getQuantity()+item.getQuantity())) {
								session.setAttribute("myCartItems", cartItems);
								session.setAttribute("myCartTotal", totalPrice(cartItems));
								session.setAttribute("myCartNum", cartItems.size());

								Product dto = productRepository.findById(id).get();
								model.put("product", new ProductDto(dto));
								List<ProductDto> newP = getProductNew();
								List<ProductCategory> page = productCategoryRepository.findAll();
								model.put("listCategory", page);
								model.put("listProductNew", newP);
								co = totalQuantity(cartItems);
								model.put("totalQ", totalQuantity(cartItems));
								if (userAll != null) {
									model.put("userAll", userAll);
								}
								model.put("mes", "Số lượng sản phẩm " + product.getName()
								+ " đã hết, vui lòng chọn sản phẩm hoặc mẫu khác");
								model.put("ss", "123");
								return "shop/showpro";
							}
						}
					}
				}
				String str = String.format("%,f", item.getProduct().getCurrentSellingPrice() * item.getQuantity())
						.replace(".000000", "");
				item.setTotal(str);
				item.setQuantity(dp.getQuantity());
				if (item.getQuantity() <= 0) {
					item.setQuantity(0);
				}
				item.setQuantity(item.getQuantity() + dp.getQuantity());
				cartItems.put(keyId, item);
			} else {
				Cart item = new Cart();
				item.setProduct(new ProductDto(product));
				item.setColor(dp.getColor());
				item.setColorUUID(dp.getColorUUID());
				if (item.getQuantity() == null ) {
					item.setQuantity(0);
				}
				
				for (ProductColor iterable_element : product.getProductColors()) {
					if(iterable_element.getColor().getId().equals(dp.getColorUUID())) {
						if(iterable_element.getProductWarehouse().size()==0) {
							session.setAttribute("myCartItems", cartItems);
							session.setAttribute("myCartTotal", totalPrice(cartItems));
							session.setAttribute("myCartNum", cartItems.size());

							Product dto = productRepository.findById(id).get();
							model.put("product", new ProductDto(dto));
							List<ProductDto> newP = getProductNew();
							List<ProductCategory> page = productCategoryRepository.findAll();
							model.put("listCategory", page);
							model.put("listProductNew", newP);
							co = totalQuantity(cartItems);
							model.put("totalQ", totalQuantity(cartItems));
							if (userAll != null) {
								model.put("userAll", userAll);
							}
							model.put("mes", "Số lượng sản phẩm " + product.getName()
									+ " đã hết, vui lòng chọn sản phẩm hoặc mẫu khác");
							model.put("ss", "123");
							return "shop/showpro"; 
						}
						for (ProductWarehouse aaa : iterable_element.getProductWarehouse()) {
							if( aaa.getProductNumber()!= null && ( aaa.getProductNumber() <(dp.getQuantity()+item.getQuantity()))) {
								session.setAttribute("myCartItems", cartItems);
								session.setAttribute("myCartTotal", totalPrice(cartItems));
								session.setAttribute("myCartNum", cartItems.size());

								Product dto = productRepository.findById(id).get();
								model.put("product", new ProductDto(dto));
								List<ProductDto> newP = getProductNew();
								List<ProductCategory> page = productCategoryRepository.findAll();
								model.put("listCategory", page);
								model.put("listProductNew", newP);
								co = totalQuantity(cartItems);
								model.put("totalQ", totalQuantity(cartItems));
								if (userAll != null) {
									model.put("userAll", userAll);
								}
								model.put("mes", "Số lượng sản phẩm " +product.getName()
										+ " không đủ,số lượng còn lại:"+aaa.getProductNumber());
								model.put("ss", "123");
								return "shop/showpro";
							}
						}
						
					}
				}

				String str = String.format("%,f", item.getProduct().getCurrentSellingPrice() * item.getQuantity())
						.replace(".000000", "");
				item.setQuantity(dp.getQuantity());
				if (item.getQuantity() <= 0) {
					item.setQuantity(0);
				}
				item.setTotal(str);
				cartItems.put(keyId, item);

			}
		}
		dp.setProductId(id);
		session.setAttribute("myCartItems", cartItems);
		session.setAttribute("myCartTotal", totalPrice(cartItems));
		session.setAttribute("myCartNum", cartItems.size());

		Product dto = productRepository.findById(id).get();
		model.put("product", new ProductDto(dto));
		List<ProductDto> newP = getProductNew();
		List<ProductCategory> page = productCategoryRepository.findAll();
		model.put("listCategory", page);
		model.put("listProductNew", newP);
		co = totalQuantity(cartItems);
		model.put("totalQ", totalQuantity(cartItems));
		if (userAll != null) {
			model.put("userAll", userAll);
		}

		model.put("mes", "Bạn đã thêm sản phẩm " + dto.getName() + ", số lượng " + dp.getQuantity()
				+ " vào giỏ hàng thành công!.");
		model.put("ss", "123");
		return "shop/showpro";
	}

	@RequestMapping(value = "/cart", params = "insert", method = RequestMethod.POST)
	public String redicret2(ModelMap model, HttpSession session) {
		if (userAll != null) {
			model.put("userAll", userAll);
		}
		HashMap<String, Cart> cartItems = (HashMap<String, Cart>) session.getAttribute("myCartItems");
		co = totalQuantity(cartItems);
		if (co == 0) {
			model.put("mes", "Xin mời thêm sản phẩm vào giỏ hàng");
			model.put("ss", "123");
			return "shop/cart";
		}

		if (userAll == null) {

			model.put("user", new UserDto());
			return "redirect:/view/cart-fake";
		} else {
			model.put("user", userAll);
			return "redirect:/view/cart-real-step2";
		}
	}

	@RequestMapping(value = "/cart-real-step2", method = RequestMethod.GET)
	public String redicret12(ModelMap model, HttpSession session) {
		if (userAll != null) {
			model.put("userAll", userAll);

		}
		HashMap<String, Cart> cartItems = (HashMap<String, Cart>) session.getAttribute("myCartItems");
		List<Cart> listAll = new ArrayList<Cart>();
		if (cartItems != null && cartItems.size() > 0) {
			for (Entry<String, Cart> cart : cartItems.entrySet()) {
				Cart cartD = cart.getValue();
				listAll.add(cartD);
			}
		}
		model.put("total", totalPrice(cartItems));
		model.put("cart", listAll);
		model.put("user", userAll);
		model.put("totalQ", co);
		return "shop/cart-real-step2";
	}

	@RequestMapping(value = "/cart-real-step2", params = "insert", method = RequestMethod.POST)
	public String redicret(ModelMap model, HttpSession session) {

		if (userAll != null) {
			model.put("userAll", userAll);

		}
		HashMap<String, Cart> cartItems = (HashMap<String, Cart>) session.getAttribute("myCartItems");

		List<Cart> listAll = new ArrayList<Cart>();
		if (cartItems != null && cartItems.size() > 0) {
			for (Entry<String, Cart> cart : cartItems.entrySet()) {
				Cart cartD = cart.getValue();
				listAll.add(cartD);
			}
		}
		try {
			Order dto = new Order();
			String prefix = "ORDER";
			String prefixAB = "URT";
			String formatCode = "%s%s";
			String formatCode2 = "%s%s%s";
			java.text.SimpleDateFormat format = new java.text.SimpleDateFormat("yyyyMMddHHmmss");
			java.text.SimpleDateFormat formata = new java.text.SimpleDateFormat("HHmmss");
			String dateString = format.format(new Date());
			String dateString2 = formata.format(new Date());
			String code = String.format(formatCode, prefix, dateString);
			String code2 = String.format(formatCode2, dateString, prefixAB, dateString2);
			dto.setCode(code2);
			dto.setName(code);
			Double total = 0.00;
			Customer c = customerRepository.findByUsername(userAll.getUsername());

			dto.setCustomer(c);
			dto.setId(UUID.randomUUID());

			dto.setCreateDate(new Date());
			dto.setCreatedBy("Wis");

			dto = orderRepository.save(dto);
			for (Cart cart : listAll) {
				ProductOrder dtDto = new ProductOrder();
				dtDto.setOrder(dto);
				Product product = productRepository.findById(cart.getProduct().getId()).get();
				dtDto.setProduct(product);
				dtDto.setProductNumber(cart.getQuantity());
				dtDto.setId(UUID.randomUUID());
				dtDto.setCreateDate(new Date());
				dtDto.setCreatedBy("Wis"); 
				Color color = null;
				color = colorRepository.findById(cart.getColorUUID()).get();
				total += product.getCurrentSellingPrice() * cart.getQuantity();
				dtDto.setIntoMoney(total);
				dtDto.setDiscount(product.getCurrentSellingPrice());
				dtDto.setUnitPrice(product.getPrice());

				List<ProductColor>	productColor = productColorRepository.findByColor(color.getId(),product.getId());
				dtDto.setProductColor(productColor.get(0));
				productOrderRepository.save(dtDto); 
			}

			dto.setTotalPrice(total);
			dto = orderRepository.save(dto);

			od = new OrderDto(dto);
			od.setDateBuy(new SimpleDateFormat("dd-MM-yyyy").format(new Date()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		co = (int) totalQuantity(cartItems);
		model.put("totalQ", totalQuantity(cartItems));

		model.put("ss", "123");
		session.setAttribute("myCartItems", cartItems);
		return "redirect:/view/success";
	}

	public String totalPrice(HashMap<String, Cart> cartItems) {
		Double count = (double) 0;
		if (cartItems != null) {
			for (Map.Entry<String, Cart> list : cartItems.entrySet()) {
				count += list.getValue().getProduct().getCurrentSellingPrice() * list.getValue().getQuantity();
			}
		}
		String strs = "0";
		if (count > 0) {
			strs = String.format("%,f", count).replace(".000000", "");
		}

		return strs;
	}

	public Integer totalQuantity(HashMap<String, Cart> cartItems) {
		Integer count = 0;
		if (cartItems != null) {
			for (Map.Entry<String, Cart> list : cartItems.entrySet()) {
				count++;
			}
		}
		return count;
	}

	@RequestMapping(value = "/cart-fake", params = "insert", method = RequestMethod.POST)
	public String redicreta(ModelMap model, @ModelAttribute("user") UserDto dp, HttpSession session) {
		if (userAll != null) {
			model.put("userAll", userAll);
		}
		String fail = "";

		if (dp.getPhone() == null || dp.getEmail() == null || dp.getName() == null || dp.getAddress() == null) {
			fail += "Xin mời nhập đủ dữ liệu";
			model.put("mes", fail);
			model.put("user", dp);
			return index1fake(model, session);
		}

		if (dp.getPhone() != null && !dp.getPhone().matches(pattern)) {
			fail += "Không đúng định dạng số điện thoại, 10 số. ";
			model.put("user", dp);
		}

		if (!dp.getEmail().matches(emailPattern)) {
			fail += "Không đúng định dạng email";
		}

		if (fail.length() > 0) {
			model.put("mes", fail);
			return index1fake(model, session);
		}

		HashMap<String, Cart> cartItems = (HashMap<String, Cart>) session.getAttribute("myCartItems");

		List<Cart> listAll = new ArrayList<Cart>();
		if (cartItems != null && cartItems.size() > 0) {
			for (Entry<String, Cart> cart : cartItems.entrySet()) {
				Cart cartD = cart.getValue();
				listAll.add(cartD);
			}
		}
		try {
			Order dto = new Order();
			String prefix = "ORDER";
			String prefixAB = "URT";
			String formatCode = "%s%s";
			String formatCode2 = "%s%s%s";
			java.text.SimpleDateFormat format = new java.text.SimpleDateFormat("yyyyMMddHHmmss");
			java.text.SimpleDateFormat formata = new java.text.SimpleDateFormat("HHmmss");
			String dateString = format.format(new Date());
			String dateString2 = formata.format(new Date());
			String code = String.format(formatCode, prefix, dateString);
			String code2 = String.format(formatCode2, dateString, prefixAB, dateString2);
			dto.setCode(code2);
			dto.setName(code);
			Double total = 0.00;
			Customer c = new Customer();

			c = new Customer();
			c.setAddress(dp.getAddress());
			c.setName(dp.getName());
			c.setPhoneNumber(dp.getPhone());
			c.setUsername(dp.getUsername());
			c.setCode("WIS");
			c.setId(UUID.randomUUID());
			c.setBirthDate(new Date());
			c.setCreateDate(new Date());
			c.setCreatedBy("Wis");
			c = customerRepository.save(c);

			dto.setCustomer(c);
			dto.setId(UUID.randomUUID());

			dto.setCreateDate(new Date());
			dto.setCreatedBy("Wis");

			dto = orderRepository.save(dto);
			for (Cart cart : listAll) {
				ProductOrder dtDto = new ProductOrder();
				dtDto.setOrder(dto);
				Product product = productRepository.findById(cart.getProduct().getId()).get();
				dtDto.setProduct(product);
				dtDto.setProductNumber(cart.getQuantity());
				dtDto.setId(UUID.randomUUID());
				dtDto.setCreateDate(new Date());
				dtDto.setCreatedBy("Wis"); 
				Color color = null;
				color = colorRepository.findById(cart.getColorUUID()).get();
				
				
				total += product.getCurrentSellingPrice() * cart.getQuantity();
				dtDto.setIntoMoney(total);
				dtDto.setDiscount(product.getCurrentSellingPrice());
				dtDto.setUnitPrice(product.getPrice());
				List<ProductColor>	productColor = productColorRepository.findByColor(color.getId(),product.getId());
				dtDto.setProductColor(productColor.get(0));
				productOrderRepository.save(dtDto);
				//discount giam gia, unit don gia, intomoney thanh tien
				
			}

			dto.setTotalPrice(total);
			dto = orderRepository.save(dto);
			od = new OrderDto(dto);

			od.setDateBuy(new SimpleDateFormat("dd-MM-yyyy").format(new Date()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		co = (int) totalQuantity(cartItems);
		model.put("totalQ", totalQuantity(cartItems));

		model.put("ss", "123");
		session.setAttribute("myCartItems", cartItems);
		return "redirect:/view/success";
	}
}

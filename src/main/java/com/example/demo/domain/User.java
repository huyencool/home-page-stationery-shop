package com.example.demo.domain;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name = "tbl_user")
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(name = "create_date", nullable = false)
	private Date createDate;
	@Column(name = "created_by", length = 100, nullable = false)
	private String createdBy;
	@Column(name = "username", length = 100, nullable = false, unique = true)

	private String username;
	@Column(name = "password", nullable = false)
	private String password;
	@Column(name = "just_created", nullable = false)
	private Boolean justCreated = true;
	@Column(name = "last_login_time", nullable = true)
	private Date lastLoginTime;
	@Column(name = "total_login_failures", nullable = true)
	private Long totalLoginFailures;
	@Column(name = "last_login_failures", nullable = true)
	private Long lastLoginFailures;
	@Column(name = "email", length = 150, nullable = true, unique = false)
	private String email;

	@Transient
	private Boolean changePassword = false;
	@Transient
	private String confirmPassword;
	@Column(name = "active", nullable = false)
	private Boolean active = true;
	@Column(name = "account_non_expired", nullable = true)
	private Boolean accountNonExpired = true;
	@Column(name = "account_non_locked", nullable = true)
	private Boolean accountNonLocked = true;
	@Column(name = "credentials_non_expired", nullable = true)
	private Boolean credentialsNonExpired = true;

	@ManyToMany(fetch = FetchType.EAGER)
	@Fetch(FetchMode.SELECT)
	@JoinTable(name = "tbl_user_role", joinColumns = { @JoinColumn(name = "user_id") }, inverseJoinColumns = {
			@JoinColumn(name = "role_id") })
	private Set<Role> roles = new HashSet();

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Boolean getJustCreated() {
		return this.justCreated;
	}

	public void setJustCreated(Boolean justCreated) {
		this.justCreated = justCreated;
	}

	public Date getLastLoginTime() {
		return this.lastLoginTime;
	}

	public void setLastLoginTime(Date lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}

	public Long getTotalLoginFailures() {
		return this.totalLoginFailures;
	}

	public void setTotalLoginFailures(Long totalLoginFailures) {
		this.totalLoginFailures = totalLoginFailures;
	}

	public Long getLastLoginFailures() {
		return this.lastLoginFailures;
	}

	public void setLastLoginFailures(Long lastLoginFailures) {
		this.lastLoginFailures = lastLoginFailures;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Boolean getActive() {
		return this.active;
	}

	public Boolean getAccountNonExpired() {
		return this.accountNonExpired;
	}

	public Boolean getAccountNonLocked() {
		return this.accountNonLocked;
	}

	public Boolean getCredentialsNonExpired() {
		return this.credentialsNonExpired;
	}

	public void setAccountNonExpired(Boolean accountNonExpired) {
		this.accountNonExpired = accountNonExpired;
	}

	public void setAccountNonLocked(Boolean accountNonLocked) {
		this.accountNonLocked = accountNonLocked;
	}

	public void setCredentialsNonExpired(Boolean credentialsNonExpired) {
		this.credentialsNonExpired = credentialsNonExpired;
	}

	public boolean isAccountNonExpired() {
		return this.accountNonExpired;
	}

	public boolean isAccountNonLocked() {
		return this.accountNonLocked;
	}

	public boolean isCredentialsNonExpired() {
		return this.credentialsNonExpired;
	}

	public boolean isEnabled() {
		return this.active;
	}

	public Boolean getChangePassword() {
		return this.changePassword;
	}

	public void setChangePassword(Boolean changePassword) {
		this.changePassword = changePassword;
	}

	public String getConfirmPassword() {
		return this.confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public User() {
	}


    public User(Long id, String username, String email, Boolean accountNonLocked) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.accountNonLocked = accountNonLocked;
    }

    public User(Long id, String username, String email, Boolean accountNonLocked, Boolean accountNonExpired) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.accountNonLocked = accountNonLocked;
        this.accountNonExpired = accountNonExpired;
    }

    public User(User user, boolean isSetPerson) {
        this.accountNonExpired = user.isAccountNonExpired();
        this.accountNonLocked = user.isAccountNonLocked();
        this.active = user.getActive();
        this.changePassword = user.changePassword;
        this.confirmPassword = user.confirmPassword;
        this.credentialsNonExpired = user.isCredentialsNonExpired();
        this.email = user.getEmail();
        this.justCreated = user.getJustCreated();
        this.lastLoginFailures = user.getLastLoginFailures();
        this.lastLoginTime = user.getLastLoginTime();
        this.password = user.getPassword();
        this.changePassword = user.getChangePassword();
        this.confirmPassword = user.getConfirmPassword();
        this.username = user.getUsername();
        this.setId(user.getId());
        this.setCreateDate(user.getCreateDate());
        this.setCreatedBy(user.getCreatedBy());
        this.roles = user.getRoles();
         

    }
}

package com.example.demo.dto;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.example.demo.domain.Product;
import com.example.demo.domain.ProductColor;
import com.example.demo.domain.ProductImage;
import com.example.demo.domain.ProductWarehouse; 

public class ProductDto extends BaseObjectDto{
	private String name;
	private String code;
	private Double currentSellingPrice; 
	private Integer soLuongDangCo; 
	private ProductCategoryDto productCategory; 
	private String priceFm;
	private String imageUrl;//Đường dẫn đến File ảnh tiêu đề bài báo (nếu có)
	private List<ImageDto> images;
	private String posts;
	private Set<ColorDto> productColors = new HashSet<ColorDto>();
	private Double price;
	private String  description;
	private String  priceMM;
	private String  keyword;
	
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public String getPriceMM() {
		return priceMM;
	}
	public void setPriceMM(String priceMM) {
		this.priceMM = priceMM;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getPriceFm() {
		return priceFm;
	}
	public void setPriceFm(String priceFm) {
		this.priceFm = priceFm;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public Double getCurrentSellingPrice() {
		return currentSellingPrice;
	}
	public void setCurrentSellingPrice(Double currentSellingPrice) {
		this.currentSellingPrice = currentSellingPrice;
	}
 
	public Integer getSoLuongDangCo() {
		return soLuongDangCo;
	}
	public void setSoLuongDangCo(Integer soLuongDangCo) {
		this.soLuongDangCo = soLuongDangCo;
	}
	public ProductCategoryDto getProductCategory() {
		return productCategory;
	}
	public void setProductCategory(ProductCategoryDto productCategory) {
		this.productCategory = productCategory;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public List<ImageDto> getImages() {
		return images;
	}
	public void setImages(List<ImageDto> images) {
		this.images = images;
	}
	public String getPosts() {
		return posts;
	}
	public void setPosts(String posts) {
		this.posts = posts;
	}
	public Set<ColorDto> getProductColors() {
		return productColors;
	}
	public void setProductColors(Set<ColorDto> productColors) {
		this.productColors = productColors;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public ProductDto() {
		super();
	}
	public ProductDto(Product e) {
		this.setId(e.getId());
		this.code = e.getCode();
		this.currentSellingPrice = e.getCurrentSellingPrice();
		this.name = e.getName();
		this.imageUrl = e.getImageUrl().replace("http://localhost:8085/da/api/upload/getImage/", "http://localhost:8085/da/public/getImage/").replace(".", "/");;
		this.description = e.getPosts();
		this.price = e.getPrice();
		
		int no1 = e.getPrice().intValue();
		String strs = String.format("%,d", no1);
		this.priceMM = strs;
		
		int no = e.getCurrentSellingPrice().intValue();
		String str = String.format("%,d", no);
		this.priceFm = str;
		this.images = new ArrayList<ImageDto>();
		if (e.getProductImage() != null && e.getProductImage().size() > 0) {
			for (ProductImage productCategory : e.getProductImage()) {
				ImageDto dto = new ImageDto(productCategory.getImage());
				this.images.add(dto);
			}
		}
		if(e.getProductColors()!=null && e.getProductColors().size()>0){
			for (ProductColor item : e.getProductColors()) {
				ColorDto dto = new ColorDto(item.getColor());
				for (ProductWarehouse dtoA : item.getProductWarehouse()) {
					if(dtoA.getProductColor().getColor().getId().equals(dto.getId())) {
						dto.setQuantity(dtoA.getProductNumber());
					}
				}
				this.productColors.add(dto);				
			}
		}
	}
	public ProductDto(Product e,Boolean simple) {
		this.setId(e.getId());
		this.code = e.getCode();
		this.currentSellingPrice = e.getCurrentSellingPrice();
		this.price = e.getPrice();
		this.name = e.getName();
		this.imageUrl = e.getImageUrl();
		this.posts = e.getPosts();
		 
		if(e.getProductCategory()!= null) {
			this.productCategory = new ProductCategoryDto(e.getProductCategory());
		}
		
	}
}

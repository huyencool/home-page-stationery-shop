package com.example.demo.dto;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.persistence.Column;

import com.example.demo.domain.EQAProgramAnnouncement;
import com.example.demo.domain.EQAProgramAnnouncementFileAttachment;

public class EQAProgramAnnouncementDto extends BaseObjectDto{
	private String name;
	private String content;
	private String title;
	private String messageContent;
	private Boolean active;
	private String code;
	private String createDateTime;
	private String imageUrl;//Đường dẫn đến File ảnh tiêu đề bài báo (nếu có)
	private Set<EQAProgramAnnouncementFileAttachmentDto> documents;
	
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getContent() {
		return content;
	}
	public String getMessageContent() {
		return messageContent;
	}
	public void setMessageContent(String messageContent) {
		this.messageContent = messageContent;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}
	
	public Set<EQAProgramAnnouncementFileAttachmentDto> getDocuments() {
		return documents;
	}
 
	public String getCreateDateTime() {
		return createDateTime;
	}
	public void setCreateDateTime(String createDateTime) {
		this.createDateTime = createDateTime;
	}
	public void setDocuments(Set<EQAProgramAnnouncementFileAttachmentDto> documents) {
		this.documents = documents;
	}
	public EQAProgramAnnouncementDto() {
		
	}
	public EQAProgramAnnouncementDto(EQAProgramAnnouncement entity) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
		this.id = entity.getId();
		this.content = entity.getContent();
		String[] words=entity.getContent().split(" ");
		String getTitle ="";
		for (int i = 0; i < 80; i++) {
			getTitle += words[i]+" ";
		}
		getTitle+="...";
		this.title=getTitle;
		this.messageContent = entity.getMessageContent();
		this.name = entity.getName();
		this.code = entity.getCode();
		this.active = entity.getActive();
		this.imageUrl = entity.getImageUrl()!= null? entity.getImageUrl().replace("http://localhost:8085/da/api/upload/getImage/", "http://localhost:8085/da/public/getImage/").replace(".", "/"):"";
		
		this.createDateTime = sdf.format(entity.getCreateDate());
		if(entity.getDocuments() != null  && entity.getDocuments().size() > 0) {
			this.documents = new HashSet<EQAProgramAnnouncementFileAttachmentDto>();
			for(EQAProgramAnnouncementFileAttachment document : entity.getDocuments()) {
				this.documents.add(new EQAProgramAnnouncementFileAttachmentDto(document));
			}
		}
	}
	
	public EQAProgramAnnouncementDto(EQAProgramAnnouncement entity, Boolean Sample) {
		this.id = entity.getId();
		this.content = entity.getContent();
		this.messageContent = entity.getMessageContent();
		this.name = entity.getName();
		this.code = entity.getCode();
		this.active = entity.getActive();
	}

}

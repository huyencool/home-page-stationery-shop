package com.example.demo.dto;

import java.util.Date;
import java.util.UUID;

public class BaseObjectDto {
	protected UUID id;

	 
	protected Date createDate;
	 
	protected Date modifyDate;
	
	
	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}
	
	
}

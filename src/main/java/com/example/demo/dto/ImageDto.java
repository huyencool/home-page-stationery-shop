package com.example.demo.dto;

import com.example.demo.domain.Image;

public class ImageDto extends BaseObjectDto {

	private String url;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public ImageDto(Image entity) {
		if (entity != null) {
			this.url = entity.getUrl().replace("http://localhost:8085/da/api/upload/getImage/", "http://localhost:8085/da/public/getImage/").replace(".", "/");

		}

	}

	public ImageDto() {
		// TODO Auto-generated constructor stub
	}
}

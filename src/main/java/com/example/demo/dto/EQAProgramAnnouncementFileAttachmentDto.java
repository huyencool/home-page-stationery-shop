package com.example.demo.dto;

import com.example.demo.domain.EQAProgramAnnouncementFileAttachment;

public class EQAProgramAnnouncementFileAttachmentDto extends BaseObjectDto {
	private String name;
	private String description;
	private EQAProgramAnnouncementDto eQAProgramAnnouncementDto;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public EQAProgramAnnouncementDto geteQAProgramAnnouncementDto() {
		return eQAProgramAnnouncementDto;
	}
	public void seteQAProgramAnnouncementDto(EQAProgramAnnouncementDto eQAProgramAnnouncementDto) {
		this.eQAProgramAnnouncementDto = eQAProgramAnnouncementDto;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public EQAProgramAnnouncementFileAttachmentDto() {
		
	}
	
	public EQAProgramAnnouncementFileAttachmentDto(EQAProgramAnnouncementFileAttachment entity) {
		if(entity != null) {
			this.name = entity.getName();
			this.description = entity.getDescription();
			if(entity.geteQAProgramAnnouncement() != null) {
				this.eQAProgramAnnouncementDto = new EQAProgramAnnouncementDto(entity.geteQAProgramAnnouncement(), true);
			}
		}
	}
}

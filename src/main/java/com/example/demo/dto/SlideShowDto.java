package com.example.demo.dto;

import com.example.demo.domain.Slideshow;

public class SlideShowDto extends BaseObjectDto {
	private String url; 
	private String name;
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	

	public String getUrl() { return url; }

	public void setUrl(String url) { this.url = url; }
public SlideShowDto() {
	// TODO Auto-generated constructor stub
}
	public SlideShowDto(Slideshow entity) {
		this.setId(entity.getId());
		url = entity.getUrl() != null ? entity.getUrl().replace("http://localhost:8085/da/api/upload/getImage/", "http://localhost:8085/da/public/getImage/").replace(".", "/"):"";
		name= entity.getName();
	}
}

package com.example.demo.dto;

import java.util.UUID;

public class Cart {
	private UUID productId;
	private Integer quantity;
	private ProductDto product;
	private String total;
	private String color;
	private UUID colorUUID;
	
	

	public UUID getColorUUID() {
		return colorUUID;
	}

	public void setColorUUID(UUID colorUUID) {
		this.colorUUID = colorUUID;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public UUID getProductId() {
		return productId;
	}

	public void setProductId(UUID productId) {
		this.productId = productId;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	 

	public ProductDto getProduct() {
		return product;
	}

	public void setProduct(ProductDto product) {
		this.product = product;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String d) {
		this.total = d;
	}

}

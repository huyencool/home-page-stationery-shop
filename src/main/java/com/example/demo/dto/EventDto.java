package com.example.demo.dto;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.example.demo.domain.Event;
import com.example.demo.domain.ProductEvent;

public class EventDto extends BaseObjectDto{
	private String name;
	private String code;
	private String description;//mô tả
	private String startDate;//Ngày bắt đầu
	private String endDate;//Ngày kết thúc
	private Boolean isActivate;//Sự kiện còn hoạt động không
	private Set<ProductEventDto> productEvent;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public Boolean getIsActivate() {
		return isActivate;
	}
	public void setIsActivate(Boolean isActivate) {
		this.isActivate = isActivate;
	}
	public Set<ProductEventDto> getProductEvent() {
		return productEvent;
	}
	public void setProductEvent(Set<ProductEventDto> productEvent) {
		this.productEvent = productEvent;
	}
	public EventDto() {
		super();
	}
	public EventDto(Event e) {
		if(e != null) {
			this.setId(e.getId());
			this.code = e.getCode();
			this.name = e.getName();
			this.description = e.getDescription();
			SimpleDateFormat ff = new SimpleDateFormat("dd-MM-yyyy");
		 
				this.startDate =ff.format(e.getStartDate());
				this.endDate =ff.format(e.getEndDate());
			 
			
			this.isActivate = e.getIsActivate();
			if (e.getProductEvent()!= null && e.getProductEvent().size() >0) {
				this.productEvent = new HashSet<ProductEventDto>();
				for (ProductEvent sanPhamPhieuXuatDto :e.getProductEvent()) {
					this.productEvent.add(new ProductEventDto(sanPhamPhieuXuatDto));
				}
			}
		}
		
	}
	public EventDto(Event e, Boolean simple) {
		if(e != null) {
			this.setId(e.getId());
			this.code = e.getCode();
			this.name = e.getName();
			this.description = e.getDescription();
			SimpleDateFormat ff = new SimpleDateFormat("dd-MM-yyyy");
			 
			this.startDate =ff.format(e.getStartDate());
			this.endDate =ff.format(e.getEndDate());
	this.isActivate = e.getIsActivate();
			
		}
		
	}
	
}

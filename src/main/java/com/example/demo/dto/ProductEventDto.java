package com.example.demo.dto;

import com.example.demo.domain.ProductEvent;

public class ProductEventDto extends BaseObjectDto {
	private EventDto event;
	private ProductDto product;
	private Float discountPercent;
	private String priceFm;
	private Integer percentSale;

	public Integer getPercentSale() {
		return percentSale;
	}

	public void setPercentSale(Integer percentSale) {
		this.percentSale = percentSale;
	}

	public String getPriceFm() {
		return priceFm;
	}

	public void setPriceFm(String priceFm) {
		this.priceFm = priceFm;
	}

	public EventDto getEvent() {
		return event;
	}

	public void setEvent(EventDto event) {
		this.event = event;
	}

	public ProductDto getProduct() {
		return product;
	}

	public void setProduct(ProductDto product) {
		this.product = product;
	}

	public Float getDiscountPercent() {
		return discountPercent;
	}

	public void setDiscountPercent(Float discountPercent) {
		this.discountPercent = discountPercent;
	}

	public ProductEventDto() {
		super();
	}

	public ProductEventDto(ProductEvent p) {
		if (p != null) {
			this.setId(p.getId());
			if (p.getProduct() != null) {
				this.product = new ProductDto(p.getProduct());
			}
			if (p.getEvent() != null) {
				this.event = new EventDto(p.getEvent(), false);
			}
			this.percentSale = (int) Math.round((p.getProduct().getPrice()-p.getDiscountPercent()) / p.getProduct().getPrice() * 100);
			this.discountPercent = p.getDiscountPercent();
			int no1 = p.getDiscountPercent().intValue();
			String strs = String.format("%,d", no1);
			this.priceFm = strs;
		}
	}

}

package com.example.demo.dto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import com.example.demo.domain.ProductCategory;

public class ProductCategoryDto extends BaseObjectDto {
	private String name;
	private String code;
	private List<ProductDto> products;
	private Set<ProductCategoryDto> productCategory;
	HashMap<UUID, ProductDto> listProduct;
	private List<ProductCategoryDto> childrent;
	private ProductCategoryDto parent;

	public HashMap<UUID, ProductDto> getListProduct() {
		return listProduct;
	}

	public void setListProduct(HashMap<UUID, ProductDto> listProduct) {
		this.listProduct = listProduct;
	}

	public Set<ProductCategoryDto> getProductCategory() {
		return productCategory;
	}

	public List<ProductDto> getProducts() {
		return products;
	}

	public void setProducts(List<ProductDto> products) {
		this.products = products;
	}

	public void setProductCategory(Set<ProductCategoryDto> productCategory) {
		this.productCategory = productCategory;
	}

	public List<ProductCategoryDto> getChildrent() {
		return childrent;
	}

	public void setChildrent(List<ProductCategoryDto> childrent) {
		this.childrent = childrent;
	}

	public ProductCategoryDto getParent() {
		return parent;
	}

	public void setParent(ProductCategoryDto parent) {
		this.parent = parent;
	}

	/* Getters and Setters */
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public ProductCategoryDto() {
		super();
	}

	public ProductCategoryDto(ProductCategory entity) {
		super();
		if (entity != null) {
			this.id = entity.getId();
			this.name = entity.getName();
			this.code = entity.getCode();
			if (entity != null && entity.getChildrent() != null) {
				List<ProductCategoryDto> listChild = new ArrayList<ProductCategoryDto>();
				for (ProductCategory cat : entity.getChildrent()) {
					ProductCategoryDto catDto = new ProductCategoryDto(cat, false, false);
					listChild.add(catDto);
				}
				this.setChildrent(listChild);
			}

		}

	}

	public ProductCategoryDto(ProductCategory entity, Boolean parent, Boolean childrent) {
		super();
		this.id = null;
		if (entity != null) {
			this.id = entity.getId();
			this.name = entity.getName();
			this.code = entity.getCode();
		}
		this.childrent = null;
		if (childrent) {
			if (entity != null && entity.getChildrent() != null) {
				List<ProductCategoryDto> listChild = new ArrayList<ProductCategoryDto>();
				for (ProductCategory cat : entity.getChildrent()) {
					ProductCategoryDto catDto = new ProductCategoryDto(cat, false, false);
					listChild.add(catDto);
				}
				this.setChildrent(listChild);
			}
		}
		this.parent = null;
		if (parent) {
			this.parent = new ProductCategoryDto(entity.getParent(), false, false);
		}
	}

	public ProductCategoryDto(ProductCategory entity, boolean simple) {
		if (entity != null) {
			this.id = entity.getId();
			this.name = entity.getName();
			this.code = entity.getCode();
		}
	}

	public ProductCategoryDto(ProductCategory entity, List<ProductDto> listProductGet) {
		super();
		if (entity != null) {
			this.id = entity.getId();
			this.name = entity.getName();
			this.code = entity.getCode();
			this.products = null;
			this.childrent = new ArrayList<ProductCategoryDto>();
			this.products = new ArrayList<ProductDto>();
			this.listProduct = new HashMap<UUID, ProductDto>();

			if (listProductGet != null && listProductGet.size() > 0) {
				for (ProductDto productCategory : listProductGet) {
					products.add( productCategory);
				}
			}

		}
	}

}

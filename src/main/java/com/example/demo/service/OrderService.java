package com.example.demo.service;

import java.util.UUID;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.jaxb.SpringDataJaxb.OrderDto;
import org.springframework.stereotype.Service;

import com.example.demo.dto.seachdto.SearchDto;
@Service
public interface OrderService  {
	public Page<OrderDto> getPage(int pageSize, int pageIndex);
	public OrderDto saveOrUpdate(UUID id,OrderDto dto);
	public Boolean deleteDonHang(UUID id);
	public OrderDto getCertificate(UUID id);
	Page<OrderDto> searchByPage(SearchDto dto);
	Boolean checkCode (UUID id,String code);
	public Boolean deleteCheckById(UUID id);
}

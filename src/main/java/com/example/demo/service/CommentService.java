package com.example.demo.service;

import java.util.List;
import java.util.UUID;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.example.demo.dto.CommentDto;
import com.example.demo.dto.seachdto.SearchDto;
@Service
public interface CommentService {
	public List<CommentDto> getListByProduct(UUID productId);
	public CommentDto saveOrUpdate(UUID id,CommentDto dto);
	public Boolean deleteComment(UUID id);
	public CommentDto getCertificate(UUID id);
	Page<CommentDto> searchByPage(SearchDto dto);
	public CommentDto hideComment(UUID id);
}

package com.example.demo.service;

import java.util.List;
import java.util.UUID;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.example.demo.dto.ProductCategoryDto;
import com.example.demo.dto.seachdto.SearchDto;
@Service
public interface ProductCategoryService {
	public Page<ProductCategoryDto> getPage(int pageSize, int pageIndex);
	public List<ProductCategoryDto> getAllCategory();
	public ProductCategoryDto saveOrUpdate(UUID id,ProductCategoryDto dto);
	public Boolean deleteKho(UUID id);
	public ProductCategoryDto getCertificate(UUID id);
	Page<ProductCategoryDto> searchByPage(SearchDto dto);
	Boolean checkCode (UUID id,String code);
	public Boolean deleteCheckById(UUID id);
}

package com.example.demo.service;

import java.util.List;
import java.util.UUID;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.example.demo.dto.ProductDto;
import com.example.demo.dto.seachdto.SearchDto;

@Service
public interface ProductService{
	public Page<ProductDto> getPage(int pageSize, int pageIndex);
	public ProductDto saveOrUpdate(UUID id,ProductDto dto);
	public Boolean deleteKho(UUID id);
	public ProductDto getCertificate(UUID id);
	Page<ProductDto> searchByPage(SearchDto dto);
	Boolean checkCode (UUID id,String code);
	public Boolean deleteCheckById(UUID id);
	ProductDto updateImage(List<UUID> imageDtos, UUID id);
}

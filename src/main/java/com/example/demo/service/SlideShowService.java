package com.example.demo.service;

import java.util.UUID;

import com.example.demo.dto.SlideShowDto;

public interface SlideShowService {

	public SlideShowDto getCertificate(UUID id);
}

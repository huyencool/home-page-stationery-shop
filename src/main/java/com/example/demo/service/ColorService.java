package com.example.demo.service;

import java.util.UUID;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.example.demo.dto.ColorDto;
import com.example.demo.dto.seachdto.SearchDto;
@Service
public interface ColorService {
	public Page<ColorDto> getPage(int pageSize, int pageIndex);
	public ColorDto saveOrUpdate(UUID id,ColorDto dto);
	public Boolean deleteKho(UUID id);
	public ColorDto getCertificate(UUID id);
	Page<ColorDto> searchByPage(SearchDto dto);
	Boolean checkCode (UUID id,String code);
	public Boolean deleteCheckById(UUID id);
}

package com.example.demo.service;

import java.util.List;
import java.util.UUID;

import org.springframework.data.domain.Page;

import com.example.demo.dto.CustomerDto;
import com.example.demo.dto.seachdto.SearchDto;


public interface CustomerService {
	CustomerDto getCustomerDto(UUID id);
	CustomerDto saveCustomer(CustomerDto dto);
	Page<CustomerDto> searchCustomer(SearchDto dto);
	Page<CustomerDto> getListPage(int pageIndex, int pageSize);
	boolean deleteMultiple(CustomerDto[] dtos);
	Boolean deleteCustomer(UUID id);
	List<CustomerDto> getAllCustomers();
	Boolean checkCode (UUID id,String code);
}

package com.example.demo.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.demo.domain.Customer;
import com.example.demo.domain.User;
@Repository
public interface CustomerRepository extends JpaRepository<Customer, UUID>{ 


	@Query("FROM Customer c where c.username = ?1 ")
	Customer findByUsername(String username);
	 
}

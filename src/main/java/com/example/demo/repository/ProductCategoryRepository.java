package com.example.demo.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.demo.domain.Product;
import com.example.demo.domain.ProductCategory;
import com.example.demo.dto.ProductCategoryDto; 

@Repository
public interface ProductCategoryRepository extends JpaRepository<ProductCategory, UUID>{ 
	@Query("select new com.example.demo.dto.ProductCategoryDto(ed) from ProductCategory ed    ")
	List<ProductCategoryDto> getListProductCategory( );
	
	
}

package com.example.demo.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.demo.domain.Order;
import com.example.demo.domain.ProductOrder;
import com.example.demo.dto.OrderDto;


@Repository
public interface OrderRepository extends JpaRepository<Order, UUID>{
	@Query("select new com.example.demo.dto.OrderDto(ed) from  Order ed where ed.customer.id = ?1  ")
	List<OrderDto> getListOrderById(UUID order );
}

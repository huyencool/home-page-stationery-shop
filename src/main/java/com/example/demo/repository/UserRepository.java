package com.example.demo.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.demo.domain.User;
import com.example.demo.dto.ProductCategoryDto;
import com.example.demo.dto.UserDto;
@Repository
public interface UserRepository extends JpaRepository<User, Long>{ 
	@Query("select new com.example.demo.dto.UserDto(ed) from User ed where ed.username=?1 and ed.password=?2   ")
	List<UserDto> getUser(String username,String password );
	

	@Query("FROM User c where c.username = ?1 ")
	User findByUsername(String username);
	 
	
	@Query("select c FROM User c where c.email = ?1 ")
	User findByemail(String username);
}

package com.example.demo.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.demo.domain.ProductOrder;
import com.example.demo.dto.ProductCategoryDto;
@Repository
public interface ProductOrderRepository extends JpaRepository<ProductOrder, UUID>{
	
	@Query("select ed from ProductOrder ed where ed.order.id = ?1  ")
	List<ProductOrder> getListProductOrder(UUID order );
}

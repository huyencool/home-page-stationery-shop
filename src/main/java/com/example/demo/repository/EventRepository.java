package com.example.demo.repository;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.demo.domain.Event;
import com.example.demo.dto.EventDto;
@Repository
public interface EventRepository extends JpaRepository<Event, UUID>{
	 
	@Query("select new com.example.demo.dto.EventDto(ed) from Event ed where ?1 BETWEEN ed.startDate and ed.endDate ")
	List<EventDto> getListPage(Date date  );

	 
	@Query("select new com.example.demo.dto.EventDto(ed) from Event ed  ")
	List<EventDto> getListPageAll( );
}

package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.demo.domain.Role;
import com.example.demo.domain.User;
import com.example.demo.dto.UserDto;
@Repository
public interface RoleRepository extends JpaRepository<Role, Long>{ 
 

	@Query("select c FROM Role c where c.name = ?1 ")
	Role findByUsername(String username);
	 
}

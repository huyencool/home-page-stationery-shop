package com.example.demo.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.demo.domain.Product;
import com.example.demo.dto.ProductDto; 
@Repository
public interface ProductRepository extends JpaRepository<Product, UUID>{
	 
	@Query("select new com.example.demo.dto.ProductDto(ed) from Product ed    where ed.productCategory.id=?1")
	List<ProductDto> getListProduct(UUID pageable);
	
	@Query("select new com.example.demo.dto.ProductDto(ed) from Product ed    ")
	List<ProductDto> getAllProduct();
	
	@Query("select new com.example.demo.dto.ProductDto(ed) from Product ed  where ed.name like ?1  ")
	List<ProductDto> getAllProductByName(String name);
	
	@Query(value="select new com.example.demo.dto.ProductDto(ed) from Product ed  order by createDate desc   " )
	List<ProductDto> getAllProductNew( );
}

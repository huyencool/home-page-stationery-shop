package com.example.demo.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.demo.domain.ProductColor;
@Repository
public interface ProductColorRepository extends JpaRepository<ProductColor, UUID>{
	@Query("FROM ProductColor c where c.color.id = ?1 and c.product.id=?2 ")
	List<ProductColor> findByColor(UUID username,UUID productId);
	 
}

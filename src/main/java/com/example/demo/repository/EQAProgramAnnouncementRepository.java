package com.example.demo.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.demo.domain.EQAProgramAnnouncement;
import com.example.demo.dto.EQAProgramAnnouncementDto;
import com.example.demo.dto.EQAProgramIntroductionDto;

@Repository
public interface EQAProgramAnnouncementRepository extends JpaRepository<EQAProgramAnnouncement, UUID>{
	
	@Query("select entity FROM EQAProgramAnnouncement entity where entity.code =?1 ")
	EQAProgramAnnouncement getByCode(String code);
	
	@Query("select new com.example.demo.dto.EQAProgramAnnouncementDto(ed) from EQAProgramAnnouncement ed")
	List<EQAProgramAnnouncementDto> getAllNews();
}

package com.example.demo.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.demo.domain.Slideshow;
import com.example.demo.dto.ProductCategoryDto;
import com.example.demo.dto.SlideShowDto;

@Repository
public interface SlideShowRepository extends JpaRepository<Slideshow, UUID> {
	@Query("select new com.example.demo.dto.SlideShowDto(ed) from Slideshow ed    ")
	List<SlideShowDto> getListSlide( );
}

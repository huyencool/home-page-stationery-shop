package com.example.demo.repository;
import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.demo.domain.EQAProgramIntroduction;
import com.example.demo.dto.EQAProgramIntroductionDto;

@Repository
public interface EQAProgramIntroductionRepository extends JpaRepository<EQAProgramIntroduction, UUID>{
	@Query("select entity FROM EQAProgramIntroduction entity where entity.code =?1 ")
	EQAProgramIntroduction getByCode(String code);
	
	@Query("select new com.example.demo.dto.EQAProgramIntroductionDto(ed) from EQAProgramIntroduction ed")
	List<EQAProgramIntroductionDto> getAllNews();
}
